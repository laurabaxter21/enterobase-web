#!/bin/bash

# py.test -n2 --cov . --ignore=entero/tests/test_selenium.py entero/tests
# Decided HTML output is not useful at present - it may be somewhat human readable in a browser
# but it seems to be a lot slower to generate and it will be less easy to write scripts to deal with
# it.  HTML option outputs multiple files to a named directory; XML option ouputs to a single
# named file apparently.
# py.test -n2 --cov . --ignore=entero/tests/test_selenium.py --cov-report=html:tests_res_html entero/tests
# Decided I don't like XML either.
py.test -n2 --cov . --ignore=entero/tests/test_client_admin_pages.py entero/tests
