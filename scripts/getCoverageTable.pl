#!/usr/bin/env perl

use strict;
use warnings;

use Getopt::Long;

# I think the values which are usually likely to be helpful are 1 and 2 (and the default 0)
# to filter 0 line __init__.py files and 1 line files in the unidecode directory, respectively.
# TO DO
# Sorting on number of lines and/ or grouping according to packages may be useful.
my $minLinesInFile = 0;

my $optionsOK = GetOptions("min-length=i" => \$minLinesInFile);

if(!$optionsOK or @ARGV != 1){
  die "Usage: $0 [ --min-length ] <in-file>\n";
}

if($minLinesInFile > 0){
  print "Skipping lines for code files with less than $minLinesInFile lines of code\n";
}

my $inFileName = $ARGV[0];
open(my $INFILE, "< $inFileName") or die "Error: failed to open $inFileName\n";

my $sepLine = "-----------------------------------------------------------------------";
my $status = "seekingTable";
while(<$INFILE>){
  if($status eq "seekingTable"){
    if(/^Name\s+Stmts\s+Miss\s+Cover/){
      $status = "seekingHeader";
      print;
    }
  }
  elsif($status eq "seekingHeader"){
    print;
    $status = "readingTable";
  }
  elsif($status eq "readingTable"){
    my @fields = split(/\s+/);
    if(defined($fields[1]) and $fields[1] >= $minLinesInFile){
      print;
    }
    elsif(/$sepLine/){
      print;
      $status = "seekingTotal";
    }
  }
  elsif($status eq "seekingTotal"){
    print;
    $status = "done";
  }
  elsif($status eq "done"){
    next;
  }
  else{
    die "Error: got to unknown status $status\n";
  }
}
