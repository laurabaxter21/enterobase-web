from flask_restful import Resource, reqparse, fields, inputs, marshal
from flask import abort, request, current_app
from flask_login import current_user
from entero import dbhandle, db,app, get_database, rollback_close_system_db_session
from entero.databases.system.models import UserInputFields,User,UserBuddies,UserUploads,UserJobs
from sqlalchemy.sql import or_,func
from flask_httpauth import HTTPBasicAuth
from entero.databases.system.models import check_permission
from entero.ExtraFuncs.workspace import get_access_permission
from ..databases.system.models import User, BuddyPermissionTags


auth = HTTPBasicAuth()

import json, requests, datetime



#from entero.load_celery.tasks import assemble_genome
from entero.cell_app.tasks import import_sra
from entero.jobs.jobs import AssemblyJob
from entero.cell_app.tasks import copy_s3_to_share_space
import os


def dictget(dic, fields):
    try:
        if len(fields) == 0:
            return None
        elif len(fields) == 1:
            name = fields.pop(0)
            return dic[name]
        else:
            name = fields.pop(0)
            return dictget(dic[name], fields)
    except Exception:
        app.logger.error('Could not format %s' %fields)
        return None

@auth.verify_password
def verify_password(username_or_token, password):
    user = User.verify_auth_token(username_or_token)
    #if not user:
    #    user = User.query.filter_by(username = username_or_tokent).first()
    if not user or not user.verify_password(password):
        return False
    return True



def check_ubr_sub_strains(dbase, Strains, editStrain, new_ubser_strain):
    '''
    #K.M 02/06/2021
    check if the editStrain is uberstrain for other strains
    if so, it will chenge these strains uberstarin to be the uberstrains for the edit strains
    :param Strains: model for strains table
    :param editStrain:
    :param new_ubser_strain:
    :return:
    '''
    sub_strains = dbase.session.query(Strains).filter(Strains.uberstrain == editStrain.id).filter(Strains.uberstrain != Strains.id).all()
    for strain in sub_strains:
        strain.uberstrain=new_ubser_strain.id
        dbase.update_record(0, strain, 'strains', False)


class S3UploadAPI(Resource):
        
    @auth.login_required    
    def post(self):
        # Need to return: available databases, 
        parser = reqparse.RequestParser() 
        parser.add_argument('username', required=True,help='username')
        args = parser.parse_args()
        dat = {}
        try:
            dat['user_id'] = db.session.query(User).filter(User.username == args['username']).one().id
            access_key =  "IBU04651A1ZK483GOF21"
            secret_key = 'vBnNtSUtHkvXay79yuD3GygGLOha4xxGMffG2a45'
            dat['access_key'] = access_key
            dat['secret_key'] = secret_key
            dat['ACTIVE_DATABASES'] = app.config['ACTIVE_DATABASES'].keys()
        except Exception as e:
            rollback_close_system_db_session()
            app.logger.exception("Error while retrieve user, error messge: %s"%e.message)
        return dat 

    @auth.login_required
    def put(self):
        # Add file to db after upload 
        parser = reqparse.RequestParser()
        parser.add_argument('username', required=True, help='username')
        parser.add_argument('userid', required=True, help='internal user id')
        parser.add_argument('database', required=True,help='database name')
        parser.add_argument('keys', required=True, action='append', help='filelist')
        args = parser.parse_args()
        copy_s3_to_share_space(args['username'], args['userid'], args['database'], args['keys'])
        return 200
        #copy_s3_to_share_space.apply_async(args =[args['username'], args['userid'], args['database'], args['keys']],queue='file')


class UploadFieldsAPI(Resource):

    def get(self):
        data = []
        try:
            results = db.session.query(UserInputFields)\
                .order_by(UserInputFields.species, \
                          UserInputFields.display_order, \
                          UserInputFields.nested_order).all()
            nested = []
            for res in results:
                if int(res.nested_order) > 0:
                    nested.append( res.as_dict() )
                else:
                    if len(nested) > 0:
                        data.append( nested )
                        nested = []
                    else:
                        data.append( res.as_dict() )
            if len(nested) > 0:
                data.append( nested )
        except Exception as e:
            rollback_close_system_db_session()
            app.logger.exception("Error in UploadFieldsAPI, API 1, error messge: %s" % e.message)

        return data

class GetUploadFieldsAPI(Resource):

    def get(self, dbname):
        data = []
        dbase = get_database(dbname)
        if not dbase:
            app.logger.info("%s database is not found inside GetUploadFieldsAPI 'get'"%dbname)
            return data
            #return ['No such database %s' % dbname, 404]


        param = dbase.models.DataParam
        try:
            results = dbase.session.query(param).filter(param.tabname.in_(['strains','traces']))\
                .order_by(param.display_order, \
                          param.nested_order).all()
            nested = []
            prevNestedOrder=0
            for res in results:
                no = int(res.nested_order)
                if no:
                    if no> prevNestedOrder:
                        nested.append( res.as_dict() )
                    else:
                        data.append( nested )
                        nested = []
                        nested.append(res.as_dict())
                else:
                    if len(nested) > 0:
                        data.append( nested )
                        nested=[]
                    data.append(res.as_dict())
                prevNestedOrder=no
            if len(nested) > 0:
                data.append( nested )
            dbase.session.close()

        except Exception as e:
            dbase.rollback_close_session()
            app.logger.exception("Error in GetUploadFieldsAPI 'get' for %s database, error message: %s"%(dbname, e.message))
            return []
        return data

class StrainMetaParser(object):
    #extra_args - just a list of strings which correlate
    # to extra arguments which may be required by the client
    def __init__(self, dbname,extra_args=None):
        # Create Parser object for input
        curr_db = get_database(dbname)
        if not curr_db:
            app.logger.info("%s database is not found in StrainMetaParser" % dbname)
            return

        self.marshal = {}
        self.parser = reqparse.RequestParser()

        try:
            results = curr_db.session.query(curr_db.models.DataParam).filter_by(tabname='strains').all()
            for res in results:
                restype = str
                require = False

                #if res.required == 1:
                 #   require = True
                if res.datatype == 'date':
                    self.marshal[res.name] = fields.DateTime(dt_format='iso8601')
                    restype = inputs.date
                elif res.datatype == 'integer':

                    self.marshal[res.name] = fields.Integer(default=None)
                    restype = int
                else:
                    self.marshal[res.name] = fields.String
                    restype = unicode
                self.parser.add_argument(res.name, type=restype, required=require, \
                                         help='Invalid %s' %res.label, \
                                         default=res.default_value)
            if extra_args:
                for arg in extra_args:
                    if extra_args[arg] == bool:
                        self.marshal[arg] = fields.Boolean
                        self.parser.add_argument(arg, type=extra_args[arg])

                    else:
                        self.marshal[arg] = fields.String
                        self.parser.add_argument(arg, type=extra_args[arg])

        except Exception as e:
            curr_db.rollback_close_session()
            app.logger.exception("Error in StrainMetaParser for %s database, error message: %s"%(dbname, e.message))


#To add strains to the database associated with reads not in the SRA

class StrainMetaAPI(Resource):

    def __init__(self):
        self.getparser = reqparse.RequestParser()
        self.getparser.add_argument('accession', type=str, required=True, \
                                    action='append', help='Invalid accession number')

    def post(self, dbname):
        curr_db = get_database(dbname)
        if not curr_db:
            app.logger.info("%s database is not found in StrainMetaParser" % dbname)
            return marshal([], {}), 400

        args = self.getparser.parse_args()
        mfields = StrainMetaParser(dbname).marshal
        dat = []
        accList = []

        for acc in args['accession']:
            accList += acc.split(',')
        #check in current database
        existingdat, missingAcc = curr_db.select_abstract_strain('accession', accList)
        if len(existingdat)==0:
            return marshal([], {}), 503
        dat += existingdat
        if len(missingAcc) > 0:
            #import from Zhemin
            imported = import_sra(dbname, accession=missingAcc, tout=1)
            if len(imported) > 0:
                addeddat, missingId = curr_db.select_abstract_strain('id', imported)
                for acc in addeddat:
                    if acc in missingAcc: missingAcc.remove(acc['accession'])
                dat += addeddat
            for stillMissing in missingAcc:
                blank = {'accession' : stillMissing}
                dat.append(blank)

        return marshal(dat,  mfields), 200
    '''
    Either uploads in the form  of
    accession readname1,readname2,readname4
    seq_platform ILLUMINA,ILLUMINA,PACBIO
    seq_library Paired,Paired,Single
    read_length 500,500,

    '''

    def put(self, dbname):
        dbase=get_database(dbname)
        if not dbase:
            return "%s database is not found" % dbname, 400

        putargs = StrainMetaParser(dbname,extra_args={'id':int})
        #multiple reads i.e. R1 and R2 - single insert size
        
        user_upload =  False
        user_input = request.form
        if request.form.get('user_upload'):
        #if 'user_upload' in request.form:
            user_upload = True        
        args = putargs.parser.parse_args()
        #print "keys", args.keys()
        mod = __import__('entero.databases.%s.models' % dbname, fromlist=[dbname])
        try:                
            Strains = mod.Strains
            if not current_user or current_user.is_anonymous():
                return "Anonymous user does not have permission to upload or edit strains metadata", 400
            #set currentuser to the logged in user id or -1
                        
            #if current_user and not current_user.is_anonymous():
            currentuser= current_user.id
            #else:
            #    currentuser=-1

            record_id = args.get('id')
            editStrain= None
            if not user_upload:    
                #return "Editing metadata is not available now, please try later", 400             
                editStrain = dbase.session.query(Strains)\
                .filter(Strains.id==args['id']).first()
                if editStrain == None:
                    return 'No record associated with id',400                
                #check edit permission    
                #check permission has been edited to use the same centeral method for permission
                permissions=get_access_permission(dbname, current_user.id, strains=[editStrain.__dict__],return_all_strains_permissions=True)
                if permissions.get(editStrain.id)!=3:
                    return "You do not have permission to edit this strain's metadata.",400
                '''
                if not current_user.administrator and not editStrain.owner ==current_user.id:                    
                    #check if the user given edit permission                    
                    if not check_permission(current_user.id, 'edit_strain_metadata', dbname):              
                        #finally check if the user given edit permission from the owner
                        edit_perm = db.session.query(BuddyPermissionTags).filter_by(user_id=editStrain.owner,buddy_id=current_user.id,field='edit_strain_metadata',species=dbname).first()
                        if not edit_perm or edit_perm.value=='False':                            
                            return "You do not have permission to edit this strain's metadata.",400
                '''
                #check barcode as it is not editable field           
                front_end_names={'study_accession':'Bio Project ID', 'secondary_study_accession': 'Project ID'}
                not_allowed_to_edit_list=['barcode','sample_accession','study_accession','secondary_study_accession','secondary_sample_accession']
                
                for not_editable_item in not_allowed_to_edit_list:                    
                    if not_editable_item in args.keys():
                        table_value=getattr(editStrain,not_editable_item)
                        if not table_value and not args.get(not_editable_item):
                            continue
                        if table_value!=args.get(not_editable_item):
                            if front_end_names.get(not_editable_item):
                                _not_editable_item=front_end_names.get(not_editable_item)
                            else:
                                _not_editable_item=not_editable_item
                            message="Strain's %s is not editable, attempting to change strain's %s %s with a new value %s"%(_not_editable_item, _not_editable_item,table_value, args.get(not_editable_item))
                            app.logger.warning(message)
                            return message, 400   
                      
                #Add a check to ubstrain so tp prevent having 
                #-null value
                #-non digit number
                #-number which is not acutual strain id
                if 'uberstrain' in args.keys():                    
                    if not args['uberstrain'] or  not isinstance (args['uberstrain'], int):
                        return 'uberstrain is null or do not have proper format, attemping to change ubserstarin value from %s to %s'%(editStrain.uberstrain, args['uberstrain']), 400                                                                      
                   
                    
                    if editStrain.uberstrain !=args['uberstrain']:                        
                        print "changing uberstrain ........", editStrain.uberstrain ,args['uberstrain']
                        new_ubser_strain=dbase.session.query(Strains).filter(Strains.id==args['uberstrain']).first()
                        if not new_ubser_strain:
                            return "uberstrain %s is not valid"%args['uberstrain'], 400
                        #check if the sub strain is an upperstarin for others
                        #if so it will make them sub strain for the uberstrain
                        #K.M 02/06/2012
                        check_ubr_sub_strains(dbase, Strains, editStrain, new_ubser_strain)
            else:

                editStrain = Strains()
                editStrain.owner = current_user.id
                editStrain.email = current_user.email
                dbase.session.add(editStrain)
                dbase.session.flush()
                #need edit strain id

                read_names = args.get('accession').split(",")
                platforms = args.get('seq_platform').split(",")
                libraries = args.get('seq_library').split(',')
                paired_sample =False;
                pos=0;
                trace_ids=[]
                prev_upload_rec=None
                prev_read_name=""

                for pos in range(0,len(read_names)):

                    platform = platforms[pos]
                    library = libraries[pos]
                    read_name = read_names[pos]
                    rec = UserUploads.query.filter(UserUploads.file_name==read_name,UserUploads.species==dbname,UserUploads.user_id==current_user.id).first()
                    read_name=os.path.join(rec.file_location,read_name)
                    rec.status='queued'
                    db.session.add(rec)
                    if library == 'Paired':
                        if paired_sample:
                            read_name = prev_read_name +","+read_name
                            paired_sample=False
                        else:
                            paired_sample=True
                            prev_upload_rec=rec
                            prev_read_name = read_name
                            continue

                    newTrace = mod.Traces(strain_id=editStrain.id, status='Uploaded',
                                               seq_platform=platform, seq_version=1,read_location=read_name,
                                               seq_library=library,experiment_accession= 'NA',lastmodifiedby=currentuser
                                               )
                    dbase.session.add(newTrace)

                    dbase.session.flush()
                    newTrace.barcode = dbase.encode(newTrace.id, dbname, 'traces')
                    rec.genome_id = newTrace.id
                    if prev_upload_rec:
                        prev_upload_rec.genome_id = newTrace.id
                    trace_ids.append(newTrace.id)
                    prev_upload_rec=None




                dbase.session.commit()
                db.session.commit()

                job=AssemblyJob(trace_ids=trace_ids,
                                             database=dbname,
                                             user_id=current_user.id,
                                             priority=0,
                                             workgroup='user_upload')

            #alter all fields contained in the args
            del args['id']  #don't want to replace id!!!!!!!            
            allowed = dbase.models.metadata.tables['strains'].c
            #print "Allowed: ", allowed
            for key in args:
                if not key in allowed:
                    continue
                #could be "" if the user has deleted the entry still needs to be overwritten
                if not args[key]:
                    args[key]=None

                setattr(editStrain, key, args[key])
            editStrain.verified=1
            if user_upload:
                editStrain.barcode = dbase.encode(editStrain.id, dbname, 'strains')
                editStrain.created= datetime.datetime.now()
                editStrain.release_date = editStrain.created + datetime.timedelta(days=182)
            result =dbase.update_record(currentuser, editStrain, 'strains', user_upload)
            if not result:
                return  "Error",400
            return 'Created', 201
        except Exception as e:
            dbase.rollback_close_session()
            rollback_close_system_db_session()
            app.logger.exception("Error in %s database, error message: %s"%(dbname, e.message))
            return "Error", 503

class SchemesAPI(Resource):
    def get(self,database,scheme):
        dbase=get_database(database)
        if not dbase:
            return ""
        loci,labels = dbase.get_scheme_loci(scheme)
        descriptor = []
        
        descriptor.append({
            'name':'ST',
            'label':'ST',
            'datatype':'integer',
            'display_order':1,
            'editable':False      
        })
        order= 2
        for label in labels:
            datatype = "text"
            if label == 'st_complex':
                datatype = 'integer'
            descriptor.append({
                'name':label,
                'label':labels[label],
                'datatype':datatype,
                'display_order':order,
                'editable':False      
            })
            order+=1
        
        
        for locus in loci:
            descriptor.append({
                'name':locus,
                'label':locus,
                'datatype':'integer',
                'display_order':order,
                'editable':False      
            })
            order+=1
       
        
        return descriptor

