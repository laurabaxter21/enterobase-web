from flask import render_template,session, make_response, redirect
from .. import db
from. import upload
from ..databases.system.models import UserUploads, User, check_permission,UserPreferences
from ..decorators import auth_login_required
from flask_login import login_user, logout_user, login_required, \
    current_user,request
from werkzeug import secure_filename
import json
import ujson
from entero.ExtraFuncs.workspace import get_analysis_object, get_access_permission
import datetime, re
from flask import current_app,send_file,request,abort,copy_current_request_context, flash, redirect, url_for
from ftplib import FTP
from entero.cell_app.tasks import copy_file_from_dropbox,copy_file_to_dropbox, copy_to_s3,assemble_user_uploads
import os,threading
from threading import Thread
from entero.databases.system.models import UserJobs
from entero import dbhandle,app, get_database, rollback_close_system_db_session, get_download_scheme_folder
from sqlalchemy.orm.exc import  MultipleResultsFound, NoResultFound
import subprocess
import os
import gzip,StringIO
from shutil import copyfile
from sqlalchemy.orm import load_only
import hashlib
from entero.oauth.oauth_utils import check_client
import traceback
import sys


def _get_md5_file(filename):
    md5_hash = hashlib.md5()
    with open(filename, "rb") as f:
        for byte_block in iter(lambda: f.read(8192), b""):
            md5_hash.update(byte_block)
        return(md5_hash.hexdigest())

@upload.route('/test_upload_assembly_file',methods=['GET', 'POST'])
def test_upload_assembly_file_from_client():
    '''
    test recieving files from a client
    it recieved the file in adition  to its md5
    after dnloading the file, it calculate the md5 and return true if calculated md 5 equal to te md5 sent from the client
    otherwise, it will return false
    a token needs to be sent with the request otherwise it will not unauthorize the request
    :return:
    '''
    token=request.form.get('token')
    file1 = request.files.get('files')
    if not token or not check_client(token):
        return json.dumps({"Error": "Unauthorized request"})
    r_md5 =(request.form.get("md5"))
    filename = secure_filename(file1.filename)
    temp_folder=os.path.join(app.config.get('FILE_UPLOAD_DIR'),"temp")
    try:
        if not os.path.exists(temp_folder):
            os.makedirs(temp_folder)
    except Exception as e:
        app.logger.exception("Error in test_upload_assembly_file_from_client, error message: %s" % (e.message))
        return "false"
    file_path=os.path.join(temp_folder,filename)
    file1.save(file_path)
    calc_md5=_get_md5_file(file_path)
    os.remove(file_path)
    if r_md5==calc_md5:
        return "true"
    else:
        return "false"


@upload.route('/upload_analysis_file',methods=['GET', 'POST'])
def upload_snp_file():
    file1 = request.files['file']
    project_id =int(request.args.get("project_id"))
    file_type = request.args.get("type")
    filename = secure_filename(file1.filename)
    snp = get_analysis_object(project_id)
    if snp.user_id <> current_user.id:
        return False
    file_path=os.path.join(snp.get_folder(),filename)
    file1.save(file_path)
    snp.data[file_type]=file_path
    snp.update()
    return True


@upload.route('/download_scheme',methods=['GET', 'POST'])
def download_scheme():
    print ("From download scheme ..")
    scheme = request.args.get("pipeline_scheme")
    database = request.args.get('database') 
    data = request.args.get('datafile') 
    if database == None:
        return 'Please specify a database'
    if scheme == None :
        return 'Please specify a scheme'
    if data == None :
        return 'Please specify a datafile'
    location = os.path.join('/share_space/interact/NServ_dump/', '%s.%s' %(database, scheme) )
    if not os.path.exists(location):
        return 'Invalid database or scheme'
    #K.M. 12/09/2018
    #added to get the poper link with the new improved ones
    #get the folder using the database and scheme
    folder=get_download_scheme_folder(database, scheme)
    #if it is none, it will use the ones in the database as they have the a proper format
    if not folder:
        if data == 'profiles':
            return redirect('http://enterobase.warwick.ac.uk/schemes/%s.%s/profiles.list.gz' %(database, scheme), code=302)
        else:
            return redirect('http://enterobase.warwick.ac.uk/schemes/%s.%s/%s.fasta.gz' %(database, scheme, data), code=302)
    else:
        if data == 'profiles':
            return redirect('http://enterobase.warwick.ac.uk/schemes/%s/profiles.list.gz' %folder, code=302)
        else:
            return redirect('http://enterobase.warwick.ac.uk/schemes/%s/%s.fasta.gz' %(folder, data), code=302)
    
#actually allows download of any file  connected with an assembly
#If location is specified than that file will be downloaded 
#genome_id is still needed to check if the file can be downloaded
@upload.route('/download',methods=['GET', 'POST'])
def download_assemblies():
    #is this needed?
    user_id=-1
    acc = request.args.get("accession")
    database = request.args.get('database')
    location =  request.args.get("location")
    aid = request.args.get("assembly_id")  
    abar = request.args.get("assembly_barcode")
    file_format =   request.args.get("file_format", 'fasta')  
    if file_format not in ['fasta', 'fastq']:
        return 'Please specify fasta or fastq as a file_format'
    if database == None:
        return 'Please specify a database'
    if aid == None and abar == None:
        return 'Please specify an assembly_id or assembly_barcode'
    dbase = dbhandle[database]
    active_user=None
    try:
        if request.headers.has_key('Authorization'):
            active_user = User.verify_auth_token(request.authorization.username)
        else:
            if current_user.is_authenticated():
                active_user = current_user
        if not active_user:
            #app.logger.exception("1_Error in upload/download, not active_user")
            return make_response("No login credential (or token) supplied or are invalid\n", 401)        
    except Exception as e:
        app.logger.exception("1_Error in upload/download with error message: %s"%(e.message))
        return make_response("No login credential (or token) supplied or are invalid\n", 401)

    if abar:
        try:
            #if request.headers.has_key('Authorization'):
            #    active_user = User.verify_auth_token(request.authorization.username)
            #else:
            #    if current_user.is_authenticated():
            #        active_user = current_user            
            allowed_ids=set()            
            permission = None
            #if active_user:
            #allowed_ids, permission = active_user.get_edit_meta_permissions(database)
            ass = dbase.session.query(dbase.models.Assemblies).options(load_only('file_pointer','barcode')).filter_by(barcode=abar).first()
            aid_change = dbase.decode(abar)
            strain=dbase.session.query(dbase.models.Strains).options(load_only('owner','release_date','strain','id')).filter_by(best_assembly=aid_change).first()
            if not ass or not strain:
                return make_response("No Assembly or strain record found" ,404)
            #add check using the central check method
            permission=False                
            permission=get_access_permission(database, active_user.id,strains=[strain.__dict__])
            if not permission:
                return make_response("Not permitted", 403)  
            '''
            allowed = False
            if permission or strain.owner in allowed_ids:
                allowed = True
            if strain.release_date:
                if strain.release_date > datetime.datetime.now() and not allowed:
                    return make_response("Not permitted", 403)
            '''                
            if not ass.file_pointer:
                return make_response('No assembly file found', 404)

            if location:
                file_name = os.path.split(location)[1]
                file_name=file_name.replace(ass.barcode,strain.strain)
            else:
                location = ass.file_pointer
                if file_format == 'fasta':
                    # check if the file location is in azure
                    #change the name to fasta.gz instead of fastq.gz
                    # K.M 12/11/2020
                    if location.startswith('https'):
                        location = location.replace('fastq.gz','fasta.gz')
                    else:
                        location =location[:-1]+"a"

                #if file_format == 'fasta':
                #    location =location[:-1]+"a"
                # I have added this because sometimes stain.stain is None and throw error errors
                # I think there is a need to prevent submit assembly job when this field is empty string
                # i.e  unsupported operand type(s) for +: 'NoneType' and 'str'
                # when trying to get file name
                try:
                    file_name = strain.strain+ "."+ file_format
                except Exception as e:
                    file_name=os.path.basename(location)
                    app.logger.warning("getting file name using stain.strain and file_format is failed for assembly barcode %s, error message %s"%(abar, e.message))

            #else:
            #    return make_response("No login credential (or token) supplied or are invalid\n", 401)
            #acc = ass.traces[0].accession
            #dbase.session.close()
        except Exception as e:
            app.logger.exception("1_Error in download_assemblies with barcode %s, error message: %s"%(abar, e.message))
            dbase.session.rollback()
            dbase.session.close()
            return make_response("query database error", 503)
    elif aid:
        try:
            #if request.headers.has_key('Authorization'):
            #    active_user = User.verify_auth_token(request.authorization.username)
            #else:
            #    if current_user.is_authenticated():
            #        active_user = current_user
            #if not active_user:
            #    return make_response("No login credential (or token) supplied or are invalid\n", 401)
            
            #allowed_ids=set()
            #permission = None
            #if active_user:
            #    allowed_ids, permission = active_user.get_edit_meta_permissions(database)
            #else:
            #    permission = False
            #    allowed_ids=[]
            ass = dbase.session.query(dbase.models.Assemblies).options(load_only('file_pointer','barcode')).filter_by(id=aid).first()
            strain=dbase.session.query(dbase.models.Strains).options(load_only('owner','release_date','strain','id','created')).filter_by(best_assembly=aid).first()
            if not ass or not strain:
                return make_response("No Assembly or strain record found",404)
            permission=False
            if active_user:
                permission=get_access_permission(database, active_user.id,strains=[strain.__dict__])
            if not permission:
                return make_response("Not permitted", 403)                        
            
            #allowed = False
            #if permission or strain.owner in allowed_ids:
            #    allowed = True
            #if strain.release_date:
            #    if strain.release_date > datetime.datetime.now() and not allowed:
            #        return make_response("Not permitted", 403)
            if not ass.file_pointer:
                return make_response('No assembly file found',404)
            if location:
                file_name = os.path.split(location)[1]
                try:
                    file_name=file_name.replace(ass.barcode,strain.strain)
                except:
                    pass
            else:
                location = ass.file_pointer
                if file_format == 'fasta':
                    # check if the file location is in azure
                    # change the name to fasta.gz instead of fastq.gz
                    # K.M 12/11/2020
                    if location.startswith('https'):
                        location = location.replace('fastq.gz','fasta.gz')
                    else:
                        location =location[:-1]+"a"
                # I have added this because sometimes stain.stain is None and throw error errors
                #i.e  unsupported operand type(s) for +: 'NoneType' and 'str'
                # I think there is a need to prevent submit assembly job when this field is empty string
                #when trying to get file name
                try:
                    file_name = strain.strain+ "."+ file_format
                except Exception as e:
                    file_name = os.path.basename(location)
                    app.logger.warning(
                        "getting file name using stain.strain and file_format is failed for assembly barcode %s, error message %s" %(abar, e.message))
            #acc = ass.traces[0].accession
            #dbase.session.close()
        except Exception as e:
            app.logger.exception("2_Error in download_assemblies with assembly id %s, error message: %s"%(aid,e.message))
            dbase.session.rollback()
            dbase.session.close()
            return make_response("query database error", 503)
            #return make_response("query database error", 503)
    elif  acc:
        jobs = db.session.query(UserJobs).filter_by(accession=acc).all()
        for job in jobs:
            if job.output_location.startswith("/share_space"):
                location = job.output_location 
                file_name= acc+ "."+ file_format
    if not location:
        return make_response("not found", 404)
    
    #return send_file(location,attachment_filename=file_name,as_attachment=True)
    #check if the file is in azure
    #K.M 12/11/2020
    if location.startswith('https'):
        redir=location
    elif location.startswith('/share_space/interact/outputs'):
        #change http to https as it causes
        #When using https and try to download the file over http, it throw this error:
        # This request has been blocked; the content must be served over HTTPS.
        #K.M. 25/03/2021
        redir = location.replace('/share_space/interact/outputs', 'https://enterobase.warwick.ac.uk/as')
    elif location.startswith('/share_space/assembly'):
        redir = location.replace('/share_space/assembly', 'https://enterobase.warwick.ac.uk/old_as')
    else:
        if os.path.exists(location) :
            return send_file(location,attachment_filename=file_name,as_attachment=True)
        else:
            return make_response("%s is not found"%(location), 404)
    app.logger.info('Download from %s of %s %s' %(active_user, file_name, redir) )
    return redirect(redir, 301)


#download nwk file with the identifier being specified in the filed argument
@upload.route('/download_nwk_file',methods=['GET', 'POST'])
def download_nwk_file():
    field= request.args.get("field")
    #tree_id = request.args.get("tree_id")
    # I have commented the above as it was noy used anywhere inside the method
    # I am not sure if this metod is used or not as I did not find a pointer to it
    # I have added the following two lines as snp_project attributes was not defined
    snp_id = request.args.get("snp_id")
    snp_project = get_analysis_object(snp_id)
    
    if snp_project.has_permission:
        nwk = snp_project.get_nwk_tree()
        #replace the identifier if required
        if field <> 'default':
            mapping = snp_project.get_barcode_to_field(field)
            for barcode in mapping:
                nwk = nwk.replace(barcode,mapping[barcode])
        strIO = StringIO.StringIO()
        strIO.write(nwk)
        strIO.seek(0)
        return send_file(strIO,
                         attachment_filename=snp_project.name+".nwk",
                         as_attachment=True)        

@upload.route('/download_snp_file',methods=['GET', 'POST'])
@auth_login_required
def download_snp():
    try:
        snp_id = request.args.get("snp_id")
        snp_project = get_analysis_object(snp_id)
        file_type = request.args.get("type")
        if not snp_project.has_permission(current_user):
            return {"status":"failed, has no permission"}

        if file_type=='nwk':
            field= request.args.get("field")
            wk = snp_project.get_nwk_tree()
            #replace the identifier if required
            nwk = snp_project.get_nwk_tree()
            if field <> 'default':
                mapping = snp_project.get_barcode_to_field(field)
                for barcode in mapping:
                    nwk = nwk.replace(barcode,mapping[barcode])
            strIO = StringIO.StringIO()
            strIO.write(nwk)
            strIO.seek(0)
            return send_file(strIO,
                             attachment_filename=snp_project.name+".nwk",
                             as_attachment=True)
        elif file_type=='mapping':
            mapping =snp_project.get_barcode_to_field("strain")
            arr=[]
            for barcode in mapping:
                arr.append(barcode+"\t"+mapping[barcode])
            strIO = StringIO.StringIO()
            strIO.write("\n".join(arr))
            strIO.seek(0)
            return send_file(strIO,
                                    attachment_filename=snp_project.name+".txt",
                                    as_attachment=True)
        elif file_type=='vcf':
            if not snp_project.data.get("vcf_file"):
                snp_project.create_vcf_file()
            file_location = snp_project.data["vcf_file"]
            file_name =snp_project.name+".vcf"
            return send_file(file_location,attachment_filename=file_name,as_attachment=True)


        else:

            file_location = snp_project.data[file_type]
            file_name =os.path.split(file_location)[1]
            return send_file(file_location,attachment_filename=file_name,as_attachment=True)
    except Exception as e:
        #error_trace = traceback.format_exc(sys.exc_info())
        app.logger.error("download_snp failed, error message: %s"%(e.message))
        return json.dumps({"status":"failed"})


def file_upload_success(user_id,filename,directory,species):
    try:
        upload = db.session.query(UserUploads).filter(UserUploads.user_id==user_id,
                                                      UserUploads.file_name==filename,
                                                      UserUploads.species == species,
                                                      UserUploads.status <> 'Assembled').all()
        #I have changed the query to use all instead of first
        #as it will thrown an error if the table has more than row staisfied the conditions or no row found
        #as first ignore multiple rows and returns only the first row
                                                      #UserUploads.species==species,UserUploads.status <> 'Assembled').first()

        if len(upload)==0:
            raise Exception("No record found for file name: %s"%filename)
        elif len(upload)>1:
            raise Exception("file %s has more than record (%s) for database %s"%(filename, len(upload), species))
        else:
            upload=upload[0]
        #if upload:
            upload.file_location = directory
            data =  ujson.loads(upload.data)
            reads =  data['accession'].split(",")
            upload.status = 'Uploaded'
            #db.session.add(upload)
            db.session.commit()            
            other_reads = db.session.query(UserUploads).with_for_update().filter(UserUploads.type=="reads").filter(UserUploads.user_id==user_id).filter(UserUploads.file_name.in_(reads)).filter(UserUploads.species==species).all()
            #get all reads associated with this to check if assembly job can be sent
            if len(other_reads)==0:
                send = False      
            else:
                send=True          
            #other_reads= []
            # use md5 library to get a hash code for data['accession']
            # db.session.execute('SELECT pg_try_advisory_lock(hash)')
            # if return 1
            for other_read in other_reads: 
                print ("%s read file's status is: %s "%(other_read.file_name, other_read.status))
                if other_read.status!='Uploaded':
                    send=False
                    break
            if send:
                for read in other_reads:
                    read.status = 'Processing'
                db.session.commit()
                db.session.close() 
                user = db.session.query(User).filter_by(id=user_id).first()
                app.logger.info("Sending reads %s from user %s to be assembled" % (data['accession'],user.username))
                if app.config.get('USE_CELERY'):
                    assemble_user_uploads.apply_async(args=[user_id,species,data],kwargs={"user_email":user.email,"username":user.username},queue='job')
                else:
                    assemble_user_uploads(user_id,species,data,user_email=user.email,username=user.username) 
            else:
                db.session.close()    
               
            '''
            for readName in reads:
                #ignore this file
                if readName ==filename:
                    continue  
                other_read = db.session.query(UserUploads).filter_by(type="reads",status="Uploaded",user_id=user_id,file_name=readName,species=species).first()
                #Not all the reads are uploaded 
                if not other_read:
                    send = False
                    break
                other_reads.append(other_read)
            if send:
                #make sure that the status is processing 
                upload.status='Processing'
        
                for read in other_reads:
                    read.status = 'Processing'
                db.session.commit()
                user = db.session.query(User).filter_by(id=user_id).first()
                app.logger.info("Sending reads %s from user %s to be assembled" % (data['accession'],user.username))
                if app.config.get('USE_CELERY'):
                    assemble_user_uploads.apply_async(args=[user_id,species,data],kwargs={"user_email":user.email,"username":user.username},queue='job')
                else:
                    assemble_user_uploads(user_id,species,data,user_email=user.email,username=user.username)
            '''
        return True
    
    except Exception as e:
        db.session.rollback()
        db.session.close()
        app.logger.warning("Error in uploading file, error message: %s" % e.message)
        return False
    

@upload.route('/upload_reads',methods=['GET', 'POST'])
@auth_login_required
def file_upload():
    try:
        Uploaded_status=True
        species = request.args['species']
        user_id = current_user.id
        user_name = current_user.username        
        chunk_size = 4069
        filename = None
        chunk_line = request.stream.read(chunk_size).split('\r\n')
        '''
        Fix issue of adding additional line (boundary tag, e.g. ------WebKitFormBoundary) at the end of the uploaded files
        K.M. 13/01/2021 by removing the 
        
        '''
        boundary=chunk_line[0]+'--'

        for line in chunk_line[0:3]:
            regex = re.search("filename=\"(.+)\"", line)
            if regex:
                filename =  regex.group(1)
        if filename:
            filename = secure_filename(filename)
            folder = app.config['LOCAL_UPLOAD_DIR']
            directory = os.path.join(folder,str(user_id),species)
            if not os.path.exists(directory):
                os.makedirs(directory)
            with open(os.path.join(directory,filename), 'wb') as f: 
                chunk_size = 4069
                if boundary in chunk_line:
                    chunk_line.remove(boundary)
                f.write('\r\n'.join(chunk_line[4:]))
                while True:
                    chunk = request.stream.read(chunk_size)
                    if len(chunk) == 0:
                        break
                    #check if the boundary tag is present
                    #if so, it removes it.
                    if boundary in chunk:
                        chunk=chunk.replace(boundary,'')
                        lines=chunk.split('\r\n')
                        while not lines[-1]:
                            lines.remove(lines[-1])
                        f.write('\r\n'.join(lines))
                    else:
                        f.write(chunk)
                app.logger.info("File %s by user %s has been uploaded" % (filename,user_name))
                Uploaded_status=file_upload_success(user_id,filename,directory,species)
            
        else: 
            app.logger.exception("Error Uploading File, no file name specified")
            return  json.dumps({"error": "true"})                    
        
    except Exception as e:
        app.logger.exception("Error Uploading File, error message: %s"%e.message)
        return  json.dumps({"error": "true"})        

    if not Uploaded_status:
        return json.dumps({"error": "true"})
    result = uploadfile(name= filename, type=None, size=10000)
    return json.dumps({"files": [result.get_file()]})

                   
def load_reads_from_folder(user_id,user_name,folder,database,ftp_settings=None,min_file_size=0):
    ftp=None

    try:
        #check which files need uploading
        uploads = UserUploads.query.filter_by(species=database,
                                              user_id=user_id,
                                              status='Awaiting Upload'
                                             ).all()
        upload_dict = {}
        for upload in uploads:
            upload_dict[upload.file_name] = True
        if ftp_settings:
            arr = [app.config['EBI_FTP'] , app.config['EBI_FTP_USER'] , app.config['EBI_FTP_PASSWORD']]
            #arr = ftp_settings.split(",")
            ftp = FTP(arr[0],arr[1],arr[2])
            files = ftp.nlst(folder)
        else:
            files =os.listdir(folder)
            temp_files=[]
            for filename in files:
                temp_files.append(os.path.join(folder,filename))
            files =temp_files



        #check to see any files in the specified folder in dropbox
        for fi in files:
            name =  os.path.split(fi)[1]
            upload = upload_dict.get(name)
            if upload:
                directory = os.path.join(current_app.config['FILE_UPLOAD_DIR'],str(user_id),database)

                if not os.path.exists(directory):
                    os.makedirs(directory)
                local_filename = os.path.join(directory,name)
                if ftp_settings:
                    size =ftp.size(fi)
                    if not size<25000000:
                        local_file = open(local_filename, 'wb')
                        ftp.retrbinary('RETR '+ fi, local_file.write)
                        local_file.close()
                    else:
                        continue

                else:
                    copyfile(fi, local_filename)

                file_upload_success(user_id, name, directory, database)

        if ftp_settings:
            ftp.close()
    except Exception as e:
        app.logger.exception("load_reads_from_folder failed, error message: %s"%e.message)
        if ftp:
            try:
                ftp.close()
            except:
                pass

            
def upload_user_reads_to_ebi(database,ws_id, ftp_settings = "webin.ebi.ac.uk,Webin-37410,Gogvi5aK"):
    from entero.ExtraFuncs.workspace import get_analysis_object
    ws = get_analysis_object(ws_id)
    sids = map(str,ws.get_strain_ids())
    sids=",".join(sids)
    dbase = dbhandle[database]
    arr = ftp_settings.split(",")
    ftp = FTP(arr[0],arr[1],arr[2])
    tax_id='57046'#'119912'#'57046'
    model = "Illumina MiSeq"
    scientific_name = "Salmonella enterica subsp. enterica serovar Paratyphi C"#'Salmonella enterica subsp. enterica serovar Choleraesuis'#
    sql = "SELECT strain,source_niche,source_type,source_details,collection_year,collection_month,collection_date,"+\
        "continent,country,admin1,admin2,city,serotype,traces.read_location as files FROM strains "+\
              "INNER JOIN trace_assembly ON trace_assembly.assembly_id = strains.best_assembly "+\
              "INNER JOIN traces ON traces.id = trace_assembly.trace_id WHERE strains.id IN (%s) AND strains.strain= '20157827' AND traces.read_location IS NOT NULL" % sids
    records = dbase.execute_query(sql)
    meta_file_name = "/share_space/user_reads/ebi_upload/parac/meta_cs.tsv"
    read_file_name = "/share_space/user_reads/ebi_upload/parac/reads_cs.tsv"
    out_meta = open(meta_file_name,"w")
    out_read=open(read_file_name,"w")
    out_meta.write("#checklist_accession ERC00028\n")
    out_meta.write("#unique_name_prefix\n")
    out_meta.write("sample_alias\ttax_id\tscientific_name\tcommon_name\tsample_title\tsample_description\tisolation_source\tlat_lon\tcollection date\tgeographic location (country and/or sea)\tgeographic location (region and locality)\thost health state\thost scientific name\tisolate\tserovar\n")
    out_read.write("sample_alias\tinstrument_model\tlibrary_name\tlibrary_source\tlibrary_selection\tlibrary_strategy\tdesign_description\tlibrary_construction_protocol\tlibrary_layout\tinsert_size\tforward_file_name\tforward_file_md5\treverse_file_name\treverse_file_md5\n")
    ftp_files = ftp.nlst()
    for rec in records:
        if rec['strain']=='Ragna':
            continue
        out_meta.write("\t".join([rec['strain'],tax_id,scientific_name]))
        out_meta.write("\t\t\t")
       
        source = rec['source_details']
        if not source:
            source = rec['source_type']
            if not source:
                source=rec['source_niche']
                if not source:
                    source='Unknown'
        out_meta.write("\t"+source+"\t")
        #lat long here
        date=rec['collection_year']
        if rec['collection_month']:
            date=str(date)+"-"+str(rec['collection_month'])
            if rec['collection_date']:
                date=date+"-"+str(rec['collection_date'])
        out_meta.write("\t"+str(date))
        loc=""
        if not rec['country']:
            out_meta.write("\tnot provided")
            if  rec['continent']:
                loc=rec['continent']
        else:
            out_meta.write("\t"+rec['country'])
        if not loc:
            loc = rec['city']
        if not loc:
            loc = rec['admin2']
            if not loc:
                loc=rec['admin1']
                if not loc:
                    loc=""
        out_meta.write("\t"+loc)
        out_meta.write("\tnot applicable\tnot applicable")
        out_meta.write("\t"+rec['strain'])
        sero = rec['serotype']
        if not sero:
            sero=""
        out_meta.write("\t"+sero+"\n")
        
        files =  rec['files'].split(",")
        f_names=[]
        
        for file_name in files:
            f_names.append(os.path.split(file_name)[1])
        
        out_read.write(rec['strain']+"\t"+model+"\t\t"+"GENOMIC\tRANDOM\tWGS\t\t\t\t500\t"+f_names[0]+"\t\t"+f_names[1]+"\t\t\n")
        
        
        for file_name in files:
            if file_name.startswith("s3"):
                name = os.path.split(file_name)[1]
                file_name = "/share_space/user_reads/martin/senterica/"+name
            
            md5 = file_name+".md5"
            #command = "md5sum %s > %s" % (file_name,md5)
            proc =  subprocess.Popen(["md5sum",  file_name],stdout=subprocess.PIPE)
            (md5_info, err) = proc.communicate()
            md5_info=md5_info.replace(file_name,os.path.split(file_name)[1])
            md5_out=open(md5,"w")
            md5_out.write(md5_info)
            md5_out.close()
            try:
                if os.path.split(file_name)[1] not  in ftp_files:
                    print (file_name)
                    upload_file = open(file_name,'rb')
                    ftp.storbinary('STOR %s' % os.path.split(file_name)[1], upload_file)   
                    upload_file.close()
                upload_file = open(md5,'rb')
                ftp.storbinary('STOR %s' % os.path.split(md5)[1],upload_file)   
                upload_file.close()
            except:
                print (file_name+" not found")
     
    out_meta.close()
    out_read.close()

#get all the information for the current user in the species database
@upload.route('/get_upload_reads_info/<species>',methods=['GET', 'POST'])
@auth_login_required
def get_upload_reads_info(species):
  
    
    uploads = UserUploads.query.filter_by(species=species,user_id=current_user.id).all()
    list=[]
    for record in uploads:
        list.append(record.file_name)
        
    ftp = FTP("webin.ebi.ac.uk", user='Webin-37410', passwd='Gogvi5aK')
    
    base = current_user.username+"/"+species
    files = ftp.nlst(base)
    #ftp.quit()
    #check to see any files in dropbox and add to database if not already
    try:
        for fi in files:
            name =  fi.split("/")[2]
            if name not in list:
                local_location= os.path.join(current_user.username+"/"+species)
                directory = os.path.join(current_app.config['FILE_UPLOAD_DIR'],local_location)
                rec = UserUploads(species=species,user_id=current_user.id,file_name=name,status="uploaded")
                if not os.path.exists(directory):
                    os.makedirs(directory)
                local_filename = os.path.join(directory,name)
                ftpFile =  "/"+fi
                copy_file_from_dropbox.apply_async(args=[ftpFile,local_filename],queue="file")
                rec.file_location= directory
                db.session.add(rec)
                list.append(name)

        ftp.close()
        db.session.commit()
        uploads = UserUploads.query.filter_by(species=species,user_id=current_user.id,status="uploaded").all()
        list=[]
        for record in uploads:
            list.append(record.file_name)
        return json.dumps(list)
    except Exception as e:
        app.logger.exception("get_upload_reads_info failed, error message: %s"%e.message)
        rollback_close_system_db_session()
        return json.dumps([])

class uploadfile():
    def __init__(self, name, type=None, size=None, not_allowed_msg=''):
        self.name = name
        self.type = type
        self.size = size
        self.not_allowed_msg = not_allowed_msg
        self.url = "data/%s" % name
        self.thumbnail_url = "thumbnail/%s" % name
        self.delete_url = "delete/%s" % name
        self.delete_type = "DELETE"


    def is_image(self):
        fileName, fileExtension = os.path.splitext(self.name.lower())

        if fileExtension in ['.jpg', '.png', '.jpeg', '.bmp']:
            return True

        return False


    def get_file(self):
        if self.type != None:
            # POST an image
            if self.type.startswith('image'):
                return {"name": self.name,
                        "type": self.type,
                        "size": self.size,
                        "url": self.url,
                        "thumbnailUrl": self.thumbnail_url,
                        "deleteUrl": self.delete_url,
                        "deleteType": self.delete_type,}

            # POST an normal file
            elif self.not_allowed_msg == '':
                return {"name": self.name,
                        "type": self.type,
                        "size": self.size,
                        "url": self.url,
                        "deleteUrl": self.delete_url,
                        "deleteType": self.delete_type,}

            # File type is not allowed
            else:
                return {"error": self.not_allowed_msg,
                        "name": self.name,
                        "type": self.type,
                        "size": self.size,}

        # GET image from disk
        elif self.is_image():
            return {"name": self.name,
                    "size": self.size,
                    "url": self.url,
                    "thumbnailUrl": self.thumbnail_url,
                    "deleteUrl": self.delete_url,
                    "deleteType": self.delete_type,}

        # GET normal file from disk
        else:
            return {"name": self.name,
                    "size": self.size,
                    "url": self.url,
                    "deleteUrl": self.delete_url,
                    "deleteType": self.delete_type,}



