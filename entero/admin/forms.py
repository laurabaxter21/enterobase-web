from flask_wtf import Form
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from entero.databases.system.models import User, UserPermissionTags, BuddyPermissionTags
from entero import app, db
from wtforms import StringField, PasswordField, BooleanField, SubmitField, HiddenField, TextAreaField, SelectMultipleField, SelectField, validators, TextField
from wtforms.widgets import TextArea, ListWidget, CheckboxInput

def valid_databases():
    values = [] 
    for key in app.config['ACTIVE_DATABASES'].keys():
        values.append((key, app.config['ACTIVE_DATABASES'][key][0]))
    return values
    
def enabled_fields():
    with app.app_context():
        return db.session.query(UserPermissionTags.field, UserPermissionTags.field).distinct()

def user_list():
    with app.app_context():
        return db.session.query(User.id, User.username).filter(User.id >= 1).all()

def values():
    with app.app_context():
        return db.session.query(UserPermissionTags.value, UserPermissionTags.value).distinct()

def enabled_buddy_fields():
    with app.app_context():
        return db.session.query(BuddyPermissionTags.field, BuddyPermissionTags.field).distinct()


class EditUserPermissionsForm(Form):
    user_id = SelectField('Username', coerce=int, choices=user_list())
    species =  SelectField('Database', choices=valid_databases())
    field = SelectField('Field', choices=enabled_fields())
    value =  SelectField('Value', choices=values())


class EditBuddyPermissionsForm(Form):
    user_id = SelectField('Username', coerce=int, choices=user_list())
    species =  SelectField('Database', choices=valid_databases())
    field = SelectField('Field', choices=enabled_buddy_fields())
    value =  SelectField('Value', choices=values())

class CKTextAreaWidget(TextArea):
    def call(self, field, *kwargs):
        kwargs.setdefault('class_', 'ckeditor')
        return super(CKTextAreaWidget, self).call(field, *kwargs)

class CKTextAreaField(TextAreaField):
    widget = CKTextAreaWidget()
    
    
class MultiCheckboxField(SelectMultipleField):
    widget = ListWidget(prefix_label=False)
    option_widget = CheckboxInput()

class EmailUserForm(Form):
    dbs = [ ('salmonella', 'Salmonella'), ('ecoli', 'Escherichia'), ('yersinia', 'Yersinia'), ('mcatarrhalis', 'Moraxella')]
    usergrps = [ ('legacy_mlst', 'Legacy MLST users'), ('view_species', 'Users with view permissions'), ('upload_reads', 'Users who can upload reads'), ('edit_strain_metadata', 'Curators'), ('api_access', 'Enterobase users with API access'), ]
    usergroups = MultiCheckboxField('User groups', choices = usergrps )
    database = MultiCheckboxField('Databases', choices = dbs )
    extra_recip = TextAreaField('Extra email recipients')
    blacklist = TextAreaField('ignore list emails')    
    subject = TextField('Subject')
    body = CKTextAreaField('Message body')
    dryrun = BooleanField('Do NOT send email to recipients (testing purposes)')
    submit = SubmitField('Send Email')
    
    def validate_extra_recip(form, field):
        field.data.split(',')
        for e in  field.data.split(','):
            if not re.match(r"[^@]+@[^@]+\.[^@]+", e):
                raise ValidationError('Invalid email %s' %e )