from celery import current_task
from entero.databases.system.models import UserJobs,UserUploads,UserPreferences,User
import  ujson,json
import requests
from entero import app, db, dbhandle, celery, create_app, config, get_database
from flask import current_app, url_for
from sqlalchemy import create_engine,func, or_
from ftplib import FTP
import psycopg2
from psycopg2.extras import RealDictCursor
from collections import OrderedDict
from sqlalchemy import select,and_
import traceback
import os,re
import gzip
from werkzeug.utils  import secure_filename
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
from shutil import copyfile
from entero.entero_email import send_email
from datetime import datetime, timedelta


@celery.task
def process_snp_job(snp_id):
   from entero.jobs.jobs import get_crobot_job
   from entero.ExtraFuncs.workspace import get_analysis_object
   from entero.jbrowse.utilities import create_snp_annotations
   snp_project=get_analysis_object(snp_id)
   try:
      snp_project.create_stats()
      if snp_project.data.get("create_annotation"):
         make_jbrowse_from_genome(snp_project.ref_barcode, snp_project.database)
         snp_project.create_vcf_file()
         create_snp_annotations(snp_project)
   
   except Exception as e:
      app.logger.exception("Probem annotating/calling stats on snp job %i" % int(snp_id))
   if snp_project.data.get("send_email"):
      try:
         snp_project.send_job_complete_email()
      except Exception as e:
         app.logger.exception("could not send email for snp project %i complete " % int(snp_id))
   if snp_project.data.get("failed"):
      del snp_project.data['failed']
   snp_project.data['complete']="true"
   snp_project.update()
      

   
@celery.task
def make_jbrowse_from_genome(barcode,database,force=False):
   from entero.jbrowse.utilities import build_annotation_file,build_sequence_index,\
        build_quality_file,addGCRatioTrack
   dbase = get_database(database)
   if not database:
        app.logger.warning("make_jbrowse_from_genome falied, could not find %s database" % database)
        return False
   Assemblies = dbase.models.Assemblies
   try:
      ass = dbase.session.query(Assemblies).filter_by(barcode=barcode).first()
      try:
         data = json.loads(ass.other_data)
      except:
         data={}
      if  data.get("can_view") and  not force:
         return False      
      build_sequence_index(ass.file_pointer[0:-1]+"a",barcode,overwrite=True)
      fasta_directory = os.path.split(ass.file_pointer)[0]
      trace = ass.traces[0]
      accession = trace.accession
      data={}
    
      if trace.status in ['Complete Genome','Contig','Scafffold',"Chromosome"]: 
         file_name =  build_annotation_file(barcode,accession,fasta_directory)
         if file_name:
            data['annotation']= file_name
      else:
         build_quality_file(ass.file_pointer,barcode)
      make_jbrowse_schemes(barcode,database)
      make_jbrowse_ref_masker(barcode, database)
      addGCRatioTrack(barcode)
      data['can_view']="true"
      ass.other_data=json.dumps(data)
      dbase.session.commit()
      dbase.session.close()
      app.logger.info("Formatted %s for JBrowse" % barcode)
      return True

   except Exception as e:
       app.logger.exception("make_jbrowse_from_genome falied, error message: %s"%e.message)
       dbase.rollback_close_session()
       return False

@celery.task
def make_jbrowse_ref_masker(barcode,database):
   dbase = get_database(database)
   if not dbase:
      app.logger.warning("make_jbrowse_ref_masker failed, could not find % database"%database)
      return False
   from entero.jbrowse.utilities import build_gff_files,alter_track
   sql = "SELECT assembly_lookup.other_data->>'file_location' AS file_location FROM assembly_lookup INNER JOIN schemes ON schemes.id =scheme_id  AND schemes.description= 'ref_masker' INNER JOIN assemblies ON assemblies.id = assembly_id AND assemblies.barcode = '%s'" % barcode
   results =dbase.execute_query(sql)
   if len (results)  == 0:
      return False
   else:

      file_location =results[0]['file_location']
      info = [file_location,"Repeat/Masked Regions"]
      gff_files={}
      gff_files['ref_masker']=info
      build_gff_files(barcode,gff_files)
      track = {}
      track['label']="ref_masker"
      track['displayMode']="collapsed"
      track['style']={
         "strandArrow":None,
         "color":"gray"
      }
      alter_track(barcode,track)
      
      

@celery.task
def make_jbrowse_schemes(barcode,database):
   
   from entero.jbrowse.utilities import build_gff_files,build_names
   dbase = get_database(database)
   if not dbase:
      app.logger.warning("make_jbrowse_schemes failed, could not find % database"%database)
      return
   try:
      assembly =  dbase.session.query(dbase.models.Assemblies).filter_by(barcode=barcode).first()
      barcode = assembly.barcode
      path = os.path.split(assembly.file_pointer)[0]
      schemes =dbase.session.query(dbase.models.Schemes).all()
      id_to_name ={}
      for scheme in schemes:
         id_to_name[scheme.id]=[scheme.description,scheme.name]

      lookups = dbase.session.query(dbase.models.AssemblyLookup).filter_by(assembly_id=assembly.id).all()
      gff_files = {}

      for lookup in lookups:
         if lookup.annotation:
            if len(lookup.annotation) <100:
               continue
            scheme_name = id_to_name[lookup.scheme_id]
            if scheme_name[0]  == 'CRISPOL' or scheme_name[0] == 'CRISPR':
               continue
            file_name = scheme_name[0]+".gff"
            file_path = os.path.join(path,file_name)
            handle = open(file_path,"w")
            handle.write(lookup.annotation)
            #name to [file location,label]
            gff_files[scheme_name[0]]=[file_path,scheme_name[1]]
            handle.close()
         #add unzipped prokka file if any
         if lookup.other_data and type(lookup.other_data) is dict:
            results = lookup.other_data.get('results')
            if results:
               gff_file_name= results.get('gff_file')
               if gff_file_name:
                  dest_name=os.path.join(path,"prokka.gff")
                  if not os.path.exists(gff_file_name):
                     app.logger.error("make_jbrowse_schemes failed, file %s is not exisit"%gff_file_name)
                     return
                  with gzip.open(gff_file_name, 'rb') as infile:
                     with open(dest_name, 'w') as outfile:
                        for line in infile:
                           outfile.write(line)
                  gff_files["prokka_gff"]=[dest_name,"Prokka Annotation"]



      build_gff_files(barcode,gff_files)
      #remove all files
      for file_name in gff_files:
         if os.path.exists(file_name):
            os.remove(gff_files[file_name][0])

      build_names(barcode)
   except Exception as e:
      app.logger.exception("make_jbrowse_schemes failed, error message: %s"%e.message)
      dbase.rollback_close_session()
    

def annotate_snp_gff(ref_barcode,barcodes,database,snp_scheme,project_id,good_positions):
   from entero.jbrowse.utilities import build_gff_files,build_snp_gff_files
   
   dbase =get_database(database)
   if not dbase:
      app.logger.warning("annotate_snp_gff failed, could not find %s database"%database)
      return
   Assemblies = dbase.models.Assemblies
   Lookup = dbase.models.AssemblyLookup
   Strains = dbase.models.Strains
   try:

      aids = []
      aid_to_barcode = {}
      ref_assembly = dbase.session.query(Assemblies).filter(Assemblies.barcode == ref_barcode).first()
      can_view=False
      ref_ass_barcode=ref_assembly.barcode
      ref_ass_id=ref_assembly.id
      #has the reference been annotated
      if ref_assembly.other_data:
         other_data = None
         try:
            other_data= json.loads(ref_assembly.other_data)
         except:
            pass
         if other_data:
            can_view =  other_data.get("can_view")
      #annotate the reference      
      if not can_view:        
         make_jbrowse_from_genome(ref_barcode, database)
      make_jbrowse_ref_masker(ref_barcode, database)       
      assemblies = dbase.session.query(Assemblies).filter(Assemblies.barcode.in_(barcodes)).all()
      
      for ass in assemblies:
         aids.append(ass.id)
         aid_to_barcode[ass.id] =  ass.barcode
                                                                                               
      
      
      lookups = dbase.session.query(Lookup).filter(Lookup.assembly_id.in_(aids),
                                                   Lookup.scheme_id==snp_scheme,
                                                   Lookup.st_barcode == ref_barcode
                                                   ).all()     
            
      aid_to_gff={}
      for query in lookups:
         q_data= ujson.loads(query.other_data)
         if type(q_data) == unicode:
            q_data= ujson.loads(q_data)
         gff = q_data.get('file_pointer')
         aid_to_gff[query.assembly_id] = gff          
      aid_to_name = {}
      gff_to_annotate={}
      aid_to_snpnum={}
      for strain in Strains:
         gff = aid_to_gff[strain.best_assembly]
         #create new GFF containing only snp /aligned calls
         #named after barcode_project-id_snps.gff
         gff_directory,gff_name = os.path.split(gff)
         gff_name= gff_name[0:-8].split(".")[-1]+"_"+str(project_id)
         base = os.path.join(gff_directory,gff_name)
         snp_gff = base+"_snps.gff"
         aligned_gff = base+"_aligned.gff"
         in_handle = open(gff)
         out_handle_snp= open(snp_gff,"w")
         out_handle_aligned = open(aligned_gff,"w")
         data = in_handle.readlines()
         snpnum=0;
         for line in data:
            arr = line.split("\t")
            if arr[2] == 'variation':
               position = arr[0]+arr[3]
               if position in good_positions:
                  out_handle_snp.write(line)
                  snpnum=snpnum+1
            if arr[2] == 'misc_feature':
               line=line.strip()      
               node = line.split("%20")[-1]
               arr =node.split(":")
               extra = 'name=%s;section=%s' % (arr[0],arr[1][:-1])
               arr = line.split("\t")
               arr = arr[0:-1]
               arr[2]='Aligned Contig'
               arr.append(extra)
               out_handle_snp.write("\t".join(arr)+"\n")
         in_handle.close()
         out_handle_snp.close()
         out_handle_aligned.close()
         aid_to_snpnum[strain.best_assembly]=snpnum
         #file name to label
         #gff_to_annotate[aligned_gff]=strain.strain+" Aligned"
         strain_name = strain.strain
         if  not strain_name:
            strain_name = 'Unknown'
            
         gff_to_annotate[snp_gff]=strain_name+" SNPs (" +str(snpnum)+")"
      dbase.session.close()
      build_snp_gff_files(ref_barcode,gff_to_annotate,project_id)
      aid_to_snpnum[ref_ass_id]=ref_ass_barcode
      return aid_to_snpnum
   except:
      app.logger.exception("Cannot send SNP scheme to jbrowse")
      dbase.rollback_close_session()

@celery.task
def set_strain_ownership(user_id,user_email):
   for key in app.config['ACTIVE_DATABASES']:
      dbase=get_database(key)
      try:
         Strains = dbase.models.Strains
         my_strains =dbase.session.query(Strains).filter(func.lower(Strains.email)==func.lower(user_email),Strains.owner == None).all()
         for strain in my_strains:
            strain.owner = user_id
            dbase.session.commit()
      
      except Exception as e:
         app.logger.exception("Problem with database:-%s" % (key))
         dbase.session.rollback()
         dbase.session.close()
      dbase.session.close() 
   
@celery.task(bind=True)
def error_handler(self, uuid):
   result = self.app.AsyncResult(uuid)
   print('Task {0} raised exception: {1!r}\n{2!r}'.format(
      uuid, result.result, result.traceback))

@celery.task
def add (x, y ):
   return x + y  


@celery.task
def copy_to_s3(username, userid, database, files):
   import boto
   import boto.s3.connection
   access_key =  "IBU04651A1ZK483GOF21"
   secret_key = 'vBnNtSUtHkvXay79yuD3GygGLOha4xxGMffG2a45'
   conn = boto.connect_s3(
      aws_access_key_id = access_key,
      aws_secret_access_key = secret_key,
      host = 's3.climb.ac.uk',
      is_secure=False,               # uncomment if you are not using ssl
      calling_format = boto.s3.connection.OrdinaryCallingFormat(),
   )
   user_bucket = conn.create_bucket('%s.%s' %(username,database))
   for file in files:

      user_reads = user_bucket.new_key(secure_filename(file.filename))
      
      #output_file = open(os.path.join(loc.encode('ascii','ignore'),key.encode('ascii','ignore')),'wb')
      user_reads.set_contents_from_string(file.read())
   #if not os.path.exists(os.path.join('/share_space/user_reads',username)):
      #os.mkdir(os.path.join('/share_space/user_reads',username))
   #if not os.path.exists(os.path.join('/share_space/user_reads',username,database)):
      #os.mkdir(os.path.join('/share_space/user_reads',username,database))   

@celery.task
def copy_s3_to_share_space(username, userid, database, keys):
   #import boto
   #import boto.s3.connection
   #access_key =  "IBU04651A1ZK483GOF21"
   #secret_key = 'vBnNtSUtHkvXay79yuD3GygGLOha4xxGMffG2a45'
   #conn = boto.connect_s3(
      #aws_access_key_id = access_key,
      #aws_secret_access_key = secret_key,
      #host = 'climb-radosgw01.bham.ac.uk',
      #is_secure=False,               # uncomment if you are not using ssl
      #calling_format = boto.s3.connection.OrdinaryCallingFormat(),
   #)
   #user_bucket = conn.get_bucket('%s.%s' %(username,database))
   #loc = os.path.join('/share_space/user_reads',username,database)
   #if not os.path.exists(os.path.join('/share_space/user_reads',username)):
      #os.mkdir(os.path.join('/share_space/user_reads',username))
   #if not os.path.exists(os.path.join('/share_space/user_reads',username,database)):
      #os.mkdir(os.path.join('/share_space/user_reads',username,database))
   try:
      for key in keys:
         #user_reads = user_bucket.get_key(key)
         #output_file = open(os.path.join(loc.encode('ascii','ignore'),key.encode('ascii','ignore')),'wb')
         #user_reads.get_contents_to_file(output_file)

         rec = UserUploads(status='uploaded',file_name=key,species=database,user_id=userid, file_location='s3://%s.%s/%s' %(username,database,key))
         db.session.add(rec)
      db.session.commit()
   except Exception as e:
      app.logger.exception("copy_s3_to_share_space failed, error message: %s"%e.message)
      db.session.rollback()

@celery.task
def copy_file_to_dropbox(ftp_directory,ftp_filename,local_file):
   ftp = FTP("webin.ebi.ac.uk", user='Webin-37410', passwd='Gogvi5aK')

   #success = ftp.mkd(ftp_directory)
   ftp_file = ftp_directory+"/"+ftp_filename
   try:
      file = open(local_file,"rb")
      ftp.storbinary('STOR '+ ftp_file, file)
      file.close()
      ftp.close()
   except Exception as e:
      app.logger.exception("copy_file_to_dropbox failed, error message: %s"%e.message)

@celery.task
def copy_file_from_dropbox(ftpFile, local_filename):
   try:
      ftp = FTP("webin.ebi.ac.uk", user='Webin-37410', passwd='Gogvi5aK')
      file = open(local_filename, 'wb')
      ftp.retrbinary('RETR '+ ftpFile, file.write)
      file.close()
      ftp.close()
   except Exception as e:
      app.logger.exception("copy_file_from_dropbox failed, error message: %s" % e.message)


@celery.task
def process_job(job_no):
   from entero.jobs.jobs import get_crobot_job
   data=[]
   resp =None
   try:
      URI = app.config['CROBOT_URI']+"/head/show_jobs"
      params = "tag IN (%s)" % str(job_no)
      resp = requests.post(URI,data={"FILTER":params})
      if resp.status_code != 502:
         data = ujson.loads(resp.text)
   except Exception as ex:
      response = "No response"
      if resp:
         response= resp.reason
      app.logger.exception("Error obtaining information for job: %s\n Response text: %s" % (URI,response))
   for datum in data:
      job = get_crobot_job(data=datum)
      job.update_job()



#checks all records that are queued, running or waiting resource and updates
@celery.task
def update_user_jobs(user_id=None,database=None):
   current =["RUNNING","QUEUE","WAIT RESOURCE","SUBMIT"]
   if not user_id:
      jobs =UserJobs.query.filter(UserJobs.status.in_(current)).order_by(UserJobs.id).all()
   else:  
      jobs =UserJobs.query.filter(UserJobs.user_id==user_id,UserJobs.database==database,UserJobs.status.in_(current)).order_by(UserJobs.id).all()
   id_list = []
   sub_list = []
   jobid_to_job={}
   for job in jobs:
      sub_list.append(str(job.id))
      jobid_to_job[job.id]=job
      if len(sub_list)==200:
         id_list.append(list(sub_list))
         sub_list = []

   try:
      for sub_list in id_list:
         to_send=','.join(sub_list)
         try:
            URI = app.config['CROBOT_URI']+"/head/show_job/"+to_send
            resp = requests.get(URI)
            data = ujson.loads(resp.text)
            for rec in data:
               job = jobid_to_job.get(rec['tag'])
               job.status = rec['status']
               db.session.add(job)
         except Exception as ex:
            app.logger.exception("Error in updating user jobs")
      db.session.commit()
   except Exception as e:
      app.logger.exception("update_user_jobs failed, error message: %s"%e.message)
      db.session.rollback()


def _set_priority(jobid, priority):
   URI = app.config['CROBOT_URI']+"/head/set_priority"
   URI = URI + "?job_tag=%i&priority=%i"%(jobid, priority)
   resp = requests.get(url = URI)
   app.logger.info("Altering priority:"+URI);

   
def _kill_active_jobs(lookup,assembly_barcode,pipeline):
   try:
      jobs = db.session.query(UserJobs).filter(UserJobs.accession == assembly_barcode)\
         .filter(UserJobs.pipeline==pipeline)\
         .filter(UserJobs.version == lookup.version)\
         .order_by(UserJobs.date_sent.desc()).all()
      for job in jobs:
         URI = app.config['CROBOT_URI']+"/head/kill_job/"+str(job.id)
         resp = requests.get(URI)
         reply = json.loads(resp.text)
   except Exception as e:
      app.logger.exception("_kill_active_jobs failed, error message: %s"%e.message)
      db.session.rollback()

@celery.task()
def assembly_result(msg):
   print (msg)



@celery.task
def check_queued(dbName = 'senterica'):
   dbase  = get_database(dbName)
   if not dbase:
      app.logger.warning("check_queued failed, could not fin %s database"%dbName)
      return
   Ass = dbase.models.Assemblies
   try:
      assemblies = dbase.session.query(Ass).filter(Ass.status == 'Queued').filter(Ass.job_id >= fromNumber).all()
      nums = []
      for ass in (assemblies):
         nums.append(ass.job_id)
      for num in sorted(nums):
         process_job(num)
   except Exception as e:
      app.logger.exception("check_queued failed, error message: %s"%e.message)
      db.session.rollback()
      
@celery.task

def import_assemblies(dbname,term,only_complete=True):
   all_data = []
   dbase = get_database(dbname)
   if not dbase:
      app.logger.warning("Error in import_assemblies, could find %s database"%dbname)
      return all_data

   genus_name = app.config['ACTIVE_DATABASES'][dbname][3]
   genus_arr = genus_name
   search_term = term
   if not search_term:
   
      search_term = ' OR '.join(genus_name)
   
   data = []
   for batch_id in range(0,9000000, 3000) :
      URI = current_app.config['DOWNLOAD_URI'] + 'assembly?num=3000&start={0}&term={1}'.format(batch_id, search_term)
      try :
         resp = requests.get(url = URI)
         d = ujson.loads(resp.text)
         if isinstance(d, list) and len(d) :
            app.logger.warning("{0} fetched {1} genomes.".format(dbname, len(d)))
            data.extend(d)
         else :
            break
      except Exception as e :
         break
   app.logger.warning("Got a total of {0} genomes for {1}.".format(len(data), dbname))
   Traces= dbase.models.Traces
   
   try:
      #get assemblies that have already loaded
      already_loaded = []
      traces =dbase.session.query(Traces).filter(Traces.status.in_(['Complete Genome','Scaffold','Contig',"Chromosome"])).all()
      for trace in traces:
         already_loaded.append(trace.accession)
      
      summary_data = {}
      for item in data:
         record = {}
         genus =  item['Sample']['Organism']['Name'].split(" ")[0]
         if genus not in genus_arr and genus_arr[0] != 'Herpesviridae':
            continue
         seq = item.get('Sequencing')
         record['status']= 'Unknown'
         if seq:
            record['status'] = seq.get('Status')
         #print genus +":"+record['status']
         complete = record['status'].startswith("Complete") or record['status'].startswith("Chromosome")
         if only_complete and not complete:
            continue
         record['accession']= item.get('Assembly')
         if record['accession'] in already_loaded:
            continue
         
         record['created'] = item.get('Creation Time')
         metadata = item['Sample'].get("Metadata")
         record['strain'] = 'Unknown'
        
         if metadata:
            record['strain'] = metadata.get('Strain')
         source = item['Sample'].get('Source')
         
         record['contact'] = 'Unknown'
         if source:
            record['contact']=source.get('Center')
         
         summary_data[record['accession']]=record
         all_data.append(item)
      #return json.dumps(summary_data)
   except Exception as e:
      dbase.rollback_close_session()
      info = 'No Response'
      if resp:
         info = resp.text          
         app.logger.exception("Call to get assembly metadata failed, error message: %s"%e.message)
   if len(all_data ) >0:
      load_sra_data(dbname, all_data, assembly=True)
 
@celery.task
def assemblies_loaded(database,data):
   #get the assembly barcodes


   dbase = get_database(database)
   if not dbase:
      app.logger.warning("Error in assemblies_loaded, Could not find %s database"%database)
      return
   Assemblies = dbase.models.Assemblies
   Strains = dbase.models.Strains
   Traces =dbase.models.Traces
   try:
      bar_to_pointer={}
     
      bar_to_acc= data['params']['acc'];
      for entry in data['outputs']['genome']:
         if entry.startswith("*"):
            continue

         try:
            name = os.path.split(entry)[1]
            barcode = name.split("_gen")[0]
            bar_to_pointer[barcode]=entry
         except:
            app.logger.info("Unable to process:" % entry)
     
      bar_list=[]
      for barcode in bar_to_acc:
         bar_list.append(barcode)
      assemblies = dbase.session.query(Assemblies).filter(Assemblies.barcode.in_(bar_list)).all()
      for ass in assemblies:
         info = ujson.loads(ass.other_data)
         strain = dbase.session.query(Strains).filter_by(id=int(info['strain_id'])).first()
         trace = dbase.session.query(Traces).filter_by(accession=bar_to_acc[ass.barcode]).first()
         if  not trace:    
            trace = Traces(strain_id=strain.id,status=info['status'],seq_platform=info['status'],accession=bar_to_acc[ass.barcode])
            ass.traces.append(trace)
            dbase.session.add(trace)            
         ass_id = ass.id
         strain.best_assembly = ass_id
        
        
         dbase.session.commit()
       
         trace.barcode = dbase.encode(trace.id, database, 'traces')
         file_pointer = bar_to_pointer.get(ass.barcode)
         if file_pointer:
               ass.status='Assembled'
               ass.file_pointer = file_pointer
        
         dbase.session.commit()
      dbase.session.close()
         
   except Exception as e:
      dbase.rollback_close_session()
      app.logger.exception('Error in prossing job %i in database '% int(data['tag']),database)
      
      
      
  
@celery.task
def import_sra(dbname, organism_name=None, accession=None, file_loc=None, backdate=100, tout=3600, numcount = 3000, sample=None,project=None):
   import ujson, requests, collections
   results = []
   if tout == None: tout = int(backdate) * 1000
   resp = None
   import_from_sra=False
   try:
      if not organism_name == None:
         data = [0] * numcount
         offset_count = 0
         cont = True
         try:
            while cont:
               
               
               URI = current_app.config['DOWNLOAD_URI'] + 'dump'
               params = dict(
                  organism = organism_name,
                  reldate = backdate,
                  start = offset_count,
                  num = numcount
               )
               offset_count += numcount
               resp = requests.get(url = URI, params = params, timeout=tout)
               if resp.status_code == 500:
                  app.logger.error('Could not access metadata')
                  return []
                  
               elif resp.status_code == 404:
                  app.logger.error('No response from NCBI')
                  #data = [0] * numcount
             
               else:
                  data = ujson.loads(resp.text)
                  message='Getting results from SRA for %s for the last %s days params: %s'%(organism_name,backdate,params)
                  app.logger.info(message)
                  if isinstance(data,dict):
                     if data.get("error"):
                        message=message+", Error Message:"+data.get("error")
                        ##############################################################################
                        ###send an email to the administrator to let him aware about error message comes from crobot for daily fetch from sra
                        ###K.M. 28/04/2020
                        if not import_from_sra:
                           try:
                              #only send warnning email in case of Salmonella or Escherichia does not reaturn any item
                              #K.M. 07/05/2021
                              if dbname=='ecoli' or dbname=='senterica':
                                 from entero.entero_email import sendmail
                                 subject = "Import from NCBI warnning"
                                 sendmail(to=app.config['ENTERO_MAIL_SENDER'], subject=subject, txt=message)
                           except Exception as e:
                              app.logger.error(
                                 "Error while sending Import from NCBI warnning, error message:%s" % e.message)
                           ############################################################################
                        app.logger.error(message)
                        break
                  if len (data) > 0:
                     import_from_sra = True
                     genome_id_list = load_sra_data(dbname, data)

                  
         except Exception as ex:
            msg = 'No Response'
            if resp:
               msg = resp.text
            app.logger.exception(URI+'\nparams:'+str(params)+'\nresponse:'+msg)            
      elif accession or sample or project:
         URI = current_app.config['DOWNLOAD_URI'] + '/metadata'
         if project:
            params=dict(project=project)
         elif sample:
            params = dict(sample = sample.split(","))
         else:
            params = dict(run = ','.join(accession))#(","))
         resp = requests.post(url=URI, params=params, timeout=20000)    
         if resp.status_code == 500:
            app.logger.error('Could not access metadata')
            return [] 
         elif resp.status_code == 404:
            app.logger.error('No response from NCBI')
            return []
         current_app.logger.info(resp)
         data = ujson.loads(resp.text)
         app.logger.info('Getting results from SRA for the following accessions:%s'%
                         (params))
         return load_sra_data(dbname, data, overwrite=False, nobad=True)
      elif not file_loc == None:
         with open(file_loc) as json_data:
            data = ujson.load(json_data)
            return load_sra_data(dbname, data)
      else:
         raise Exception
   except requests.exceptions.ReadTimeout as e:
      current_app.logger.exception('Timeout for %s' %organism_name)
      return results


def update_sra_fields(database,organism_name,fields) :
   conn= None
   curr_db = get_database(database)
   if not curr_db:
      app.logger.warning("update_sra_fields failed, could not find %s databae"%database)
      return


   backdate = 3000
   tout=20000
   try:
      strainparams = curr_db.get_dataparam('strains')
      sra_fields_dict = {}
      field_list = fields.split(",")
      for param in strainparams:
         if param['name'] in field_list:
            if param.get('sra_field'):
               sra_fields_dict[param['name']]= param['sra_field'].split(",")
      conn = psycopg2.connect(app.config['ACTIVE_DATABASES'][database][1])
      cursor = conn.cursor(cursor_factory=RealDictCursor)
      SQL= "SELECT strains.id AS sid, sample_accession FROM strains"
      cursor.execute(SQL)
      results = cursor.fetchall()
      accession_to_sid = {}
      for res in results:
         accession_to_sid[res['sample_accession']]=res['sid']
      
      
      offset_count=0
      numcount=50
      cont  = True
      max_offset=0
      while cont:
        
     
         URI = current_app.config['DOWNLOAD_URI'] + 'dump'
         params = dict(
            organism = organism_name,
            reldate = backdate,
            start = offset_count,
            num = numcount
         )
         #print offset_count
         offset_count += numcount
         resp = requests.get(url = URI, params = params, timeout=tout)
         if resp.status_code == 500:
            app.logger.error('Could not access metadata')
            return []
            
         elif resp.status_code == 404:
            app.logger.error('No response from NCBI')
            
         else:
            data = ujson.loads(resp.text)
            if isinstance(data,dict):
               if data.get("error"):
                  break                  
            if len (data) > 0:
               dirty  =False
               for rec in data:
                  acc = dictget(rec,["Sample","Main Accession"])
                  if not acc:
                     continue
                  sid = accession_to_sid.get(acc)
                  if not sid:
                     continue
                  max_offset=offset_count
                  for name in sra_fields_dict:
                     value = dictget(rec,list(sra_fields_dict[name]))
                     if value:
                        SQL = "UPDATE strains SET %s='%s' WHERE id= %i" % (name,value,int(sid))
                        #print SQL
                        cursor.execute(SQL)
                        dirty=True
               if dirty:
                  conn.commit()
      #print max_offset
      conn.close()
               
   except Exception as e:
         app.logger.exception("Update SRA failed")
         conn.rollback()
         conn.close()                     
    

def badCenter(center, filt_list, data) :
   '''
   K.M. 01/08/2019
   check if the center inside the list
   if it is, it will do another check 
   check for isolation year, country and source details 
   if it has at least two from the above three, it should go into enterobase
   otherwise it will not      
   check for the three parameters '''
   #print data['Sample']['Metadata'].keys()
   center = str(center)
   import re
   is_it_bad=False
   for f in filt_list :  
      if len(re.findall(f.lower(), center.lower())) > 0 :
         is_it_bad=True
         break
   if is_it_bad:      
      parameters=['Country','Year',{'source':['Source Details','Source Niche']}]
      counter=0
      source=0
      for item in parameters:
         if isinstance (item, dict):             
            for it in item['source']:
               if data['Sample']['Metadata'].get(it):
                  source+=1
         elif data['Sample']['Metadata'].get(item):            
            counter+=1             
      if counter>=2 or (source>1 and counter >0):
         is_it_bad=False
      else:
         is_it_bad=True
   return is_it_bad                  
       
def load_sra_data(dbname, data, overwrite=False,assembly=False, nobad=False):
   from entero.jobs.jobs import WholeAssemblyDownloadJob
   new_genome_list = []
   genome_ids = []
   dirty = {}
   curr_db = get_database(dbname)
   if not curr_db:
      app.logger.warning("load_sra_data failed, %s database could not found" % dbname)
      return genome_ids

   try:
      strainparams = curr_db.get_dataparam('strains')
      genomeparams = curr_db.get_dataparam('traces')
      skip_groups = ['Centers for Disease Control and Prevention Enteric Diseases Laboratory Branch',
                     'SC',
                     'THE SANGER CENTER',
                     'THE SANGER CENTRE',
                     'The Wellcome Trust Sanger Institute'
                     ]
      #check if the source contians any of that the following       
      skip_groups_fuzzy = [c.lower() for c in ['Centers for Disease Control and Prevention', 'Sanger', r'^SC$', 'PulseNet', 'CDC']]
      
      assemblies_to_accession ={}
      for idx, straindict in enumerate(data):
         if not assembly :
            run_acc = dictget(straindict, ['Run'])
         else :
            run_acc = dictget(straindict, ['Assembly'])
         # Ignore CDC and Sanger data. 
         if straindict.get('Sample'):
            if straindict.get('Sample').get('Source'):
               #if straindict.get('Sample').get('Source').get('Center') not in skip_groups:
               if nobad or not badCenter(straindict.get('Sample').get('Source').get('Center'), skip_groups_fuzzy, straindict):
                  if not dirty.has_key(run_acc) or overwrite:
                     if not curr_db.exists('traces', 'accession', run_acc) or overwrite:
                        strain_acc = dictget(straindict, ['Sample', 'SRA Accession'])
                        if strain_acc:
                           strain_id = curr_db.get_id('strains', 'secondary_sample_accession', strain_acc)
                        else:
                           strain_acc = dictget(straindict, ['Sample', 'Main Accession'])
                           strain_id = curr_db.get_id('strains', 'sample_accession', strain_acc)
                        new_genome = import_fields('sra_field', genomeparams, straindict)
                        if strain_id == None:
                           strainrow =  import_fields('sra_field', strainparams, straindict)
                           strainrow['date_entered']= datetime.now()
                           strain_id = curr_db.insert_strains([strainrow], commit = False)[0]
                           curr_db.update_barcodes('strains', strain_id)
                        elif overwrite:
                           strainrow =  import_fields('sra_field', strainparams, straindict)
                           strain_id = curr_db.update_strains([strainrow], commit = False)[0]
                        if type(strain_id) != int: strain_id = strain_id[0]
                        new_genome['strain_id'] =  strain_id
                        if assembly:
                           new_genome['status'] = "Assembly"
                           #create the new assembly adding the strain id
                           assembly = curr_db.models.Assemblies(status='Queued',other_data=str(strain_id))
         
                           curr_db.session.add(assembly)
                           ass_status = straindict['Sequencing']['Status']
                           assembly.other_data=ujson.dumps({"strain_id":strain_id,"status":ass_status})
                           curr_db.session.commit()
                           assembly.barcode = curr_db.encode(assembly.id, dbname, 'assemblies')
                           curr_db.session.commit()
                           #link barcode to nservs assambly accession
                           assemblies_to_accession[assembly.barcode]= dictget(straindict,['Assembly'])
                        else:
                           new_genome['status'] = "SRA"
                        if not overwrite and not assembly: 
                           new_genome_list.append(new_genome)
                     else :
                        Assemblies = curr_db.models.Assemblies
                        Traces = curr_db.models.Traces
                        Strains = curr_db.models.Strains
                        assembly_rec = curr_db.session.query(Assemblies).filter(Traces.strain_id==Strains.id).filter(Traces.accession==run_acc).filter(Assemblies.id==Strains.best_assembly).first()
                        try : 
                           if not assembly_rec.file_pointer or not os.path.isfile(assembly_rec.file_pointer) :
                              assemblies_to_accession[assembly_rec.barcode]= dictget(straindict,['Assembly'])
                        except :
                           pass
                     if run_acc:
                        dirty[run_acc] = True
         if (len(new_genome_list) % 1000 == 0 and len(new_genome_list) > 0) or idx >= len(data)-1:
            curr_db.commit()
            genome_ids += curr_db.insert_traces(new_genome_list)
            new_genome_list = []
            dirty = {}
      curr_db.update_barcodes('traces', genome_ids)
      #send job to download all assemblies
      if assembly:

         #add all the assemblies
         assembly_list = list(assemblies_to_accession.items())
         for id in range(0, len(assembly_list), 1000) :
            assembly_batch = assembly_list[id:(id+1000)]
            params={"acc":{}}
            for barcode, acc in assembly_batch:
               params['acc'][barcode]=acc
            job = WholeAssemblyDownloadJob(user_id=0,
                                          priority=-9,
                                          workgroup="public",
                                          database=dbname,
                                          params=params)
            job.send_job()

      return genome_ids
   except Exception as e:
      import traceback
      traceback.print_exc()
      app.logger.exception("load_sra_data failed, error message: %s"%e.message)
      curr_db.rollback_close_session()
      return []


def dictget(dic, fields):
   try:
      if len(fields) == 0 or dic == None:
         return None
      elif len(fields) == 1:
         name = fields.pop(0)
         if not isinstance(dic,dict):
            pass
         return dic.get(name)
      else:
         name = fields.pop(0)
         return dictget(dic.get(name), fields)
   except Exception:
      app.logger.error('Could not format %s' %fields)
      traceback.print_exc()
      traceback.print_stack()
      return None

def update_complete_genome(database,strain_id,assembly_name):
   dbase = get_database(database)
   if not dbase:
      app.logger.infor("update complete genome failed, can not find %s database"%database)
      return
   try:
      Assemblies =dbase.models.Assemblies
      #create the new assembly adding the strain id
      assembly = Assemblies(status='Queued',other_data=str(strain_id))
      dbase.session.add(assembly)
      ass_status = "Complete Genome"
      assembly.other_data=ujson.dumps({"strain_id":strain_id,"status":ass_status})
      dbase.session.commit()
      assembly.barcode = dbase.encode(assembly.id, database, 'assemblies')
      dbase.session.commit()

      URI = app.config['CROBOT_URI']+"/head/submit"
      params = {
         "source_ip":app.config['CALLBACK_ADDRESS'],
         "usr":"admin",
         "query_time":str(datetime.now()),
         "workgroup":"public",
         "_priority":0,
         "params":{"acc":{assembly.barcode:assembly_name}},
         "pipeline":"genome_download"
      }
   except Exception as e:
      app.logger.exception("update complete genome failed, error message: %s" % e.message)
      dbase.session.rollback()

   try:
      resp = requests.post(url = URI, data = {"SUBMIT":ujson.dumps(params)})
      if resp.status_code != 502:
         data = ujson.loads(resp.text)
         assembly.job_id = data['tag']
         assembly.version = data['version']
         job = UserJobs(id=data['tag'],pipeline='genome_download',date_sent=params['query_time'],database=database,
                        status=data["status"],user_id=0)
         db.session.add(job)
         db.session.commit()
   except Exception as e:
      app.logger.exception("update complete genome failed, error message: %s"%e.message)
      db.session.rollback()
      db.session.close()




def import_fields(field, data_param, straindict):
   dat = {}
   for row in data_param:
      if row.has_key(field) and row[field] != None:
         value = dictget(straindict, row[field].split(','))
         if value != None and value != '': dat[row['name']] = value
   #Sometimes year can be ambiguous - take first year and add to comment
   if dat.has_key('collection_year'):
      year = dat['collection_year']
      if year.find("/") <> -1:
         dat['collection_year']=year.split("/")[0]
         dat['comment']="Collection year is ambiguous - "+year
      if year.find("<") <> -1:
         dat['collection_year']=year.replace("<", '')
         dat['comment']="Collection year is "+year
      if year.find(">") <> -1:
         dat['collection_year']=year.replace(">", '')
         dat['comment']="Collection year is "+year      
      if year.find("-") <> -1:
         dat['collection_year']=year.split("-")[0]
         dat['comment']="Collection year is ambiguous - "+year
   for dub in ['latitude','longitude']:
      if dat.has_key(dub):
         try:
            fl = float(dat[dub])
         except:
            del dat[dub]
        
            
   if dat.has_key("source_details"):
      if len(dat['source_details']) >200:
         dat['source_details'] = dat['source_details'][0:200]
      
   #try and get the email(s) - nested list(s) so needs to be separate
   contact = dictget(straindict, ["Sample","Source","Contact"])
   if not contact:
      return dat
   emailList=[]
   for sublist in contact:
      if sublist[0]:
         emailList.append(sublist[0])
   dat['email'] = (",").join(emailList)
   return dat

@celery.task
def assemble_user_uploads(user_id,dbname,data,user_email="",username='test-test'):
   dbase=get_database(dbname)
   if not dbase:
      app.logger.warning("assemble_user_uploads failed, could not find %s database"%dbname)
      return
   from entero.jobs.jobs import AssemblyJob, QA_evaluation_Job
   session = dbase.session
   try:
      mod = dbase.models
      Strains = mod.Strains
      strain_id =  data.get('strain_id')
      strain_name = data.get('strain')
      editStrain = None
      #strain is having new reads associated with it
      if strain_id:
         editStrain = session.query(Strains).filter_by(id=strain_id).first()
      else:
         #check strain does not already exist
         editStrain = session.query(Strains).filter_by(strain=strain_name,owner=user_id).first()
         if editStrain:
            app.logger.info("Strain %s by user %i has already added - assembly being sent" % (strain_name,int(user_id)))
            #If we assume that the starin name is unique for the same user,
            #then if the strain is in table then the assembly is already sent,
            # it should return and not continue
            #if we allow stain name to be duplicated
            #Then continue and sending the assembly is not correct
            # as this methods is called after uploading reads
            #as it will overwrie the previous one
            #the only case this may valid is one would like to over wrriten starin he sent before
            #and this needs to be cheken
            #so I think a return statment shoud be added
            #Update
            #======
            #It seems that when the user has better reads files for a starin which has already submitted and assembled
            #Then he can submit the reads agaian with the same stain name, the the new reads will assembled and overwritten
            #and assigned as a best assembly

         else:
            editStrain = Strains()
            editStrain.owner = user_id          
            session.add(editStrain)
            session.commit()
   
      read_names = data.get('accession').split(",")
      platforms = data.get('seq_platform').split(",")
      libraries = data.get('seq_library').split(',')
      paired_sample =False;
      pos=0;
      trace_ids=[]
      prev_upload_rec=None
      prev_read_location=""
      prev_read_name=""
      user_upload_list=[]
      for pos in range(0,len(read_names)):
   
         platform = platforms[pos]
         library = libraries[pos]
         read_name = read_names[pos]
         rec = UserUploads.query.filter(UserUploads.file_name==read_name,UserUploads.species==dbname,UserUploads.user_id==user_id,UserUploads.type=='reads',UserUploads.status.in_(['Uploaded','Processing'])).first()
         
         read_location=os.path.join(rec.file_location,read_name)
         rec.status='Processing'
         #if platform == 'Complete Genome':
         #   rec.status = 'Assembled'
         user_upload_list.append(rec)
         db.session.add(rec)
         if library == 'Paired':
            if paired_sample:
               read_location = prev_read_location +","+read_location
               paired_sample=False
            else:
               paired_sample=True
               prev_upload_rec=rec
               prev_read_location = read_location
               
               continue
         trace_status = 'Uploaded'
         if platform == 'Complete Genome':
            trace_status = 'Complete Genome'
            
         newTrace = mod.Traces(strain_id=editStrain.id, status='Uploaded',
                               seq_platform=platform, seq_version=1,read_location=read_location,
                               seq_library=library,experiment_accession= 'NA',lastmodifiedby=user_id
                                                  )
        
         session.add(newTrace)
   
         session.commit()
         newTrace.barcode = dbhandle[dbname].encode(newTrace.id, dbname, 'traces')
         rec.genome_id = newTrace.id
         if prev_upload_rec:   
            prev_upload_rec.genome_id = newTrace.id
         trace_ids.append(newTrace.id)
         prev_upload_rec=None
   
      #Now update strain database
      if not strain_id:
         allowed = dbhandle[dbname].models.metadata.tables['strains'].c
      
         for key in data:
            if not key in allowed:
               continue
            setattr(editStrain, key, data[key])
            
         editStrain.barcode = dbhandle[dbname].encode(editStrain.id, dbname, 'strains')
         editStrain.created= datetime.now()
         editStrain.uberstrain= editStrain.id
         #strain has been created in database - add id to the upload ecord
         for upload in user_upload_list:
            info = ujson.loads(upload.data)
            info['strain_id']=editStrain.id
            upload.data=json.dumps(info)
         #work out release period
         rel_period=0
         try: 
            rel_period = int(data['release_period']) * 31
         except:
            #default 6 months
            rel_period =182
         editStrain.release_date = editStrain.created + timedelta(days=rel_period)
         dbhandle[dbname].update_record(user_id, editStrain, 'strains', True)
         
         session.commit()
     
      if platform =='Complete Genome':
         assembly = mod.Assemblies(status ='Queued',file_pointer=read_location)
         session.add(assembly)
         session.commit()
         assembly.traces.append(newTrace)
         editStrain.best_assembly=assembly.id
         assembly.barcode = dbhandle[dbname].encode(assembly.id, dbname, 'assemblies')
         session.commit()
         #send QC for the assembly to check it fulfils our QC criteria
         #15/12/2020
         job = QA_evaluation_Job(assembly_id=assembly.id,
                                 assembly=assembly,
                                 database=dbname,
                                 user_id=user_id,
                                 priority=-4,
                                 workgroup='user_upload')

         job.send_job()

      else:
         job= AssemblyJob(trace_ids=trace_ids, 
                                       database= dbname,
                                       user_id=user_id,
                                       priority=-4,
                                        workgroup='user_upload')
         job.send_job()
      
      db.session.commit()
      session.close()
      db.session.close()
     
   except Exception as e:
      app.logger.exception("****Error in assembling user jobs")
      db.session.rollback()
      db.session.close()
      dbase.rollback_close_session()
     

@celery.task
def build_cgmlst(database):
   from entero import dbhandle
   dbase  = get_database(database)
   if not dbase:
      app.logger.warning("build_cgmlst failed, could not find %s database"%database)
   # Get all STs
   st_num = 2714
   f = open('strains.tsv', 'w')
   for val in range(2714):
   # Pick Assembly with Highest N50 of all from ST
      sql = "select * from strains,assemblies,st_assembly where strains.best_assembly = assemblies.id and st_assembly.assembly_id = assemblies.id and assemblies.status = 'Assembled' and st_assembly.st_id = %d and assemblies.status != 'legacy' order by assemblies.n50 desc LIMIT 1;" % val
      results = dbase.session.execute(sql)
      for result in results:
         f.write( '\t'.join(str(x) for x in result))
         f.write('\n')
   f.close()
   # This is Reference list 
