import copy
try:
    from entero import db, get_database, rollback_close_system_db_session
    from entero.databases.system.models import UserPreferences, check_permission, BuddyPermissionTags, User, \
        query_system_database, execute_action
except :
    from ...entero import db, get_database, rollback_close_system_db_session
    from ...entero.databases.system.models import UserPreferences, check_permission, BuddyPermissionTags, User, \
        query_system_database, execute_action

class UserDefinedFields(object) :
    def __init__(self, dbname, user_id):
        # open connection to database
        self.dbname = dbname
        self.user_id = user_id
        self.dbase = get_database(dbname)

    def __del__(self):
        self.dbase = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def get_user_udfs(self) :
        udf_ids = db.session.query(UserPreferences.name, UserPreferences.id).filter(UserPreferences.database==self.dbname).filter(UserPreferences.user_id == self.user_id).filter(UserPreferences.type=='custom_column').all()
        return dict(udf_ids)

    def get_ids(self, udf_names):
        udf_ids = db.session.query(UserPreferences.id, UserPreferences.name).filter(UserPreferences.database==self.dbname).filter(UserPreferences.user_id == self.user_id).filter(UserPreferences.type=='custom_column').filter(UserPreferences.name.in_(udf_names)).all()
        return dict(udf_ids)
    
    def query(self, strain_ids, udf_ids=[], udf_names=[]):
        '''
        query UDFs for a set of strains
        At least one of udf_ids or udf_names are required
        returns UDFs by their names if udf_names are given, or udf_ids is a dict object
        otherwise returns UDFs by their IDs (str)
        
        '''
        res = {}
        if len(udf_names) :
            udf_ids = self.get_ids(udf_names)
        if len(udf_ids) :
            Strains = self.dbase.models.Strains
            for id, udf in self.dbase.session.query(Strains.id, Strains.custom_columns).filter(Strains.id.in_(strain_ids)) :
                if not isinstance(udf, dict) :
                    udf = {}
                res[id] = { str(udf_id):udf.get(str(udf_id), '') for udf_id in udf_ids }
        if isinstance(udf_ids, dict) :
            res = { k:{ udf_ids[int(udf)]:content for udf, content in v.items() } for k, v in res.items() }
        return res

    def upsert(self, data):
        # get strain ids, barcodes will be converted into ids
        Strains = self.dbase.models.Strains
        strain_ids = list(data.keys())
        try :
            strain_ids = [int(id) for id in strain_ids]
            data = { int(k):v for k, v in data.items() }
        except :
            barcode_conv = dict(self.dbase.session.query(Strains.barcode, Strains.id).filter(Strains.barcode.in_(strain_ids)).all())
            data = { barcode_conv[k]:v for k, v in data.items() if k in barcode_conv }
            strain_ids = list(data.keys())
        # get udf ids, udf names will be converted into ids
        udf_ids = set([ udf_id for udf in data.values() for udf_id in udf.keys() ])
        try :
            udf_ids = [int(id) for id in udf_ids]
            data = { k:{ str(udf):str(content).strip() for udf, content in udfs.items() } for k, udfs in data.items() }
        except :
            udf_ids = self.get_ids([str(id) for id in udf_ids])
            udf_conv = { v:k for k,v in udf_ids.items() }
            data = { k:{ str(udf_conv[udf]):str(content).strip() for udf, content in udfs.items() } for k, udfs in data.items() }
        
        n_dirty = 0
        for str_id, strain in enumerate(self.dbase.session.query(Strains).filter(Strains.id.in_(strain_ids))) :
            cc = copy.deepcopy(strain.custom_columns)
            if not isinstance(cc, dict) :
                cc = {}
            new_cc = data[strain.id]
            dirty = False
            for k, v in new_cc.items() :
                old_v = cc.get(k, None)
                if v != old_v :
                    if v != None and v != '' :
                        cc[k] = v
                    else :
                        cc.pop(k, None)
                    dirty = True
            if dirty :
                n_dirty += 1
                strain.custom_columns = cc
            if n_dirty % 500 == 499 :
                self.dbase.session.commit()
        self.dbase.session.commit()

    def delete(self, udf_id):
        pass


if __name__ == '__main__' :
    with UserDefinedFields('senterica') as udf:
        udf.get_ids(user_id=69, udf_names=['CC2', 'CC5'])