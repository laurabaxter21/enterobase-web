
from entero import dbhandle,app,db,celery, get_database, rollback_close_system_db_session
from entero.entero_email import send_email
#from entero.databases.generic_models import encode
from entero.ExtraFuncs.query_functions import process_strain_query,process_custom_view_query
from sqlalchemy import or_
from entero.jbrowse.utilities import __get_directory as get_jbrowse_directory
from entero.databases.system.models import UserPreferences, check_permission,BuddyPermissionTags,User,query_system_database,execute_action
import ujson,os,sys,csv
import gzip,requests
from shutil import rmtree,copyfile
from time import strftime
from Bio import SeqIO
import json
from datetime import datetime, timedelta



def get_analysis_objects(**kwargs):
    """This factory method will return an array of analysis objects based on the keywords supplied e.g.

    .. code-block:: python

        objs=get_analysis_objects(type="ms_sa_tree",database="senterica",user_id=55)
   
    Returns an array of the analysis objects
    """ 
    recs= db.session.query(UserPreferences).filter_by(**kwargs).all()
    arr = []
    for rec in recs:
        arr.append(analysis_classes[rec.type](rec=rec))
    return arr

        
#Factory method to return the correct object
def get_analysis_object(analysis_id):
    """This factory method will return the analysis object specified in the analysis id.
    The reason for using a factory method, is that the correct class will be  created, depending
    on the analysis object type.

    :param analysis_id: The id of the analysis 

    Returns the analysis object

    """
    try:
        rec= db.session.query(UserPreferences).filter_by(id=analysis_id).one()
        obj=analysis_classes[rec.type]
        #return analysis_classes[rec.type](rec=rec)
        return obj(rec=rec)
    except:
        db.session.rollback()
        return None


#Base Classs
class Analysis(object):
    """The base class for all analysis objects.  An object is normally created using  :meth:`get_analysis_object`,
    which ensures the correct class is chosen, based on the type value in the database record.

    :param rec:  An instance of  :class:`entero.databases.system.models.UserPreferences`
    :param data:  A dictonary which becomes the object's 'data'  (only used if rec is not supplied)
    :param params: If rec is not supplied, a dictionary of  parameters to create a database entry should be supplied
        which include

        * name
        * user_id
        * type
        * database
    """	  
    def __init__(self,rec=None,data=None,params=None):

        #create from scratch
        if params:
            if not data:
                data={}
            date_now = strftime("%Y-%m-%d %H:%M")
            data['date_created']= date_now
            data['date_modified']=date_now
            rec = UserPreferences(name=params['name'],user_id=params['user_id'],type=params['type'],database=params['database'])
            rec.data = ujson.dumps(data)
            db.session.add(rec)
            db.session.commit()
        #create the object
        self.id=rec.id
        self.database = rec.database
        self.name=rec.name
        if rec.data:
            self.data = ujson.loads(rec.data)
        else:
            self.data={}
        self.type= rec.type
        self.user_id=rec.user_id
        self.files=[]




    def get_folder(self):
        """ Returns the name of the  folder used to store files for this analysis object which is the following:-

        */<base_workspace_diectory>/<database>/<user_id><analysis_object_id>*

        If no folder exists a new one will be created.
        """
        folder = os.path.join(app.config['BASE_WORKSPACE_DIRECTORY'],str(self.database),str(self.user_id),str(self.id))
        if not os.path.exists(folder):
            os.makedirs(folder)
        return folder


    def update(self):
        """Updates the data parameter of the analysis object (if the object's data
        has been altered manually) and changes date_modified accordingly e.g. 

        .. code-block:: python

            ws = get_analysis_object(3474)
            ws.data['some_parameter']='some_data'
            ws.update()
        """	  	
        snp = db.session.query(UserPreferences).filter_by(id = self.id).one()
        self.data['date_modified']= strftime("%Y-%m-%d %H:%M")
        snp.data= ujson.dumps(self.data)
        snp.user_id=self.user_id
        snp.type=self.type
        db.session.commit()

    def get_owner(self):
        '''Returns the owner of the workspace (the original owner still
        owns a public workspace)

        Returns the owner id'''
        if self.user_id != 0:
            return self.user_id
        return self.data.get('original_owner_id')

    def has_edit_permission(self,user):
        """Determines whether a user has permission to edit the analysis object. At the moment,
        only if you own the object can you edit it.
        Returns True if the user has permission, otherwise False
        """        
        if not user.is_authenticated():
            return False
        if self.user_id == user.id: 
            return True
        try:
            SQL="select buddy_permission_tags.user_id, buddy_permission_tags.value from buddy_permission_tags where field='%s' and buddy_id=%i and user_id=%i and species='%s'"%('edit_strain_metadata', user.id,self.user_id, self.database)
            results=query_system_database(SQL)
            if len(results)==0:
                return False
            else:
                return json.loads(results[0]['value'].lower())
        except:
            return False
    

    def get_url(self):
        '''Returns the url to display the analysis or None if the analysis cannot be displayed'''
        url = app.config['ANALYSIS_TYPES'][self.type].get("url")
        if not url:
            return None
        url = url.replace("<database>",self.database)
        url = url.replace("<id>",str(self.id))
        return app.config['SERVER_BASE_NAME']+ url


    def has_permission(self,user):
        """Determines whether a user has permission to view the the analysis object, based on the following rules:-

        * administrators can view all
        * public objects (user_id=0) can be viewed by all
        * users can view their own obects or any shared with them
        * if a workspaces is shared then so are any trees it contains


        :param user: The current_user object

        Returns True or False depending on whether the user has permission

        """	
        if not user.is_authenticated():
            if self.user_id==0:
                return True
            return False
        if self.user_id==0:
            return True
        permission = False
        #user is administrator
        if user.administrator == 1:
            permission = True
        #user owns or is public
        elif user.id == self.user_id:		
            permission = True
        #is the workspace shared?
        else:
            query_id =self.id
            #if parent use this to see if it is shared
            parent_workspace = self.data.get("parent")
            if parent_workspace:
                query_id=parent_workspace
            bud = db.session.query(BuddyPermissionTags).filter_by(buddy_id=user.id,value = str(query_id)).all()
            if len(bud)<>0:
                permission = True
            #check if it is in the buddy shared folder
            if not permission:

                sql = "SELECT buddy_permission_tags.value as ws_id ,buddy_permission_tags.field, buddy_permission_tags.user_id AS ws_owner_id, users.username AS ws_owner_name FROM" + \
                      " buddy_permission_tags INNER JOIN users ON buddy_permission_tags.user_id = users.id WHERE buddy_id =%i AND species='%s' AND field ='workspace_folders'" % (
                        user.id, self.database)
                
                tags = query_system_database(sql)
                for tag in tags:
                    if tag.get('field') == 'workspace_folders':
                        shared = json.loads(tag.get('ws_id'))
                        shared_folder = shared.get('shared_folder')
                        shared_value = shared_folder[1]  # buddy_sub_folders
                        shared_id = shared_folder[0]
                        budy_shared_foler, work_spaces = get_shared_folders(self.database, tag.get('ws_owner_id'),
                                                                             shared_folder[0], shared_folder[1],
                                                                             str(tag.get('ws_owner_id')))
                        if query_id in work_spaces:
                            permission = True
                            break

        return permission

    def add_data(self,data):
        """Updates the object's data with the supplied dictionary (data) and updates date_modified.
        Existing entries in the object's data dictionary will be overwritten or new ones created.

        :param data: A dictionary of key value pairs to add the the object's data
        """
        try:
            snp = db.session.query(UserPreferences).with_for_update().filter_by(id=self.id).one()
            try:
                x = ujson.loads(snp.data)
            except:
                raise Exception("Could not load data: %s from snp %s"%(snp.data, snp))
            x.update(data)
            snp.data= ujson.dumps(x)
            db.session.commit()
        except Exception as e:
            rollback_close_system_db_session()
            app.logger.exception("add_data/Analysis failed, error message: %s"%e.message)


    def rename(self,new_name):
        """Renames the analysis object.

        :param new_name:  The new name of the analysis object
        """
        pref = db.session.query(UserPreferences).filter_by(id = self.id).one()
        pref.name=new_name
        db.session.commit()

    def copy(self,name,user_id):
        """Copies the object to a new user. This will duplicate all files etc.

        :param name: The name of the new object
        :param user_id: The id of the user to whom the copied object should be assigned

        Returns the id of the newly created object
        """	
        pref = UserPreferences(user_id=user_id,name=name,type=self.type)
        db.session.add(pref)
        db.session.commit()
        self.files=[]		
        self.get_files()
        if len(self.files)>0:
            new_folder = os.path.join(app.config['BASE_WORKSPACE_DIRECTORY'],str(self.database),str(user_id),str(pref.id))
            if not os.path.exists(new_folder):
                os.makedirs(new_folder)

            for file_info in self.files:
                file_pointer= self.data.get(file_info[0])
                if  file_pointer:	
                    new_pointer = os.path.join(new_folder,file_info[1])
                    copy_file(file_pointer,new_pointer)
                    self.data[file_info[0]]=new_pointer
            pref.data= ujson.dumps(self.data)
            db.session.commit()

        return pref.id

    def make_public(self):
        '''Makes the work space public and adds original_owner and original_owner_id to the object's data
        
        :param folder_name: The name of the folder to add the workspace to (root  by default)
        '''
        if self.user_id==0:
            return
        
        folder="RN"
        folders=db.session.query(UserPreferences).filter_by(type='public_workspace_folders',database=self.database).one()
        user = db.session.query(User).filter_by(id=self.user_id).one()
        data= ujson.loads(folders.data)
        #get or create folder with the users name
        folder = data.get(str(self.user_id))
        if not folder:
            folder={"id":self.user_id,"workspaces":[],"children":[],"text":user.username}
            data[self.user_id]=folder
            data['RN']['children'].append(self.user_id)
        folder['workspaces'].append(self.id)
        folders.data=ujson.dumps(data)
        db.session.commit()
        
        user_name = "%s %s (%s) " % (user.firstname,user.lastname,user.username)
        self.data['original_owner']=user_name
        self.data['original_owner_id']=self.user_id
        self.user_id=0
        self.update()
    
    def make_private(self,send_user_email=False):
        '''If the object has an original owner the object will be re-assigned to that owner
        
        :param send_user_email: If True (default False) then an email will be sent to the user informing
            them that the analysis has been made private
        '''
        original_owner = self.data.get("original_owner_id")
        if not original_owner:
            return
        del self.data['original_owner'];
        del self.data['original_owner_id'];
        self.remove_from_public_folders()
        self.user_id=original_owner
        self.update()
        if send_user_email:
            a_type= app.config['ANALYSIS_TYPES'][self.type]['label']
            user = db.session.query(User).filter_by(id=self.user_id).one()
            send_email(user.email,"Your %s has been removed" % a_type,
                            "auth/email/analysis_made_private",user=user,analysis=self,
                            type=a_type)


    def delete(self,send_user_email=False):
        """Completely deletes the analysis object including all files and buddy links.

        :param send_user_email: If True (default is False) then the user will be informed their workspace
            has been deleted
        """
        #delete if from public workspace folders
        if self.user_id==0:
            self.remove_from_public_folders()
        #delete all all buddy records
        sql = "DELETE FROM buddy_permission_tags WHERE id IN (SELECT id FROM buddy_permission_tags WHERE  field = '%s' AND  value::INTEGER = %i  AND species ='%s' AND  user_id=%i)" % (self.type,self.id,self.database,self.user_id)
        execute_action(sql)
        self.files=[]
        self.get_files()
        for file_info in self.files:
            file_pointer = self.data.get(file_info[0])
            if file_pointer:
                if os.path.exists(file_pointer):
                    os.remove(file_pointer)
        pref =db.session.query(UserPreferences).filter_by(id = self.id).one()
        db.session.delete(pref)
        db.session.commit()
        if send_user_email:
            user_id = self.get_owner()
            if user_id:
                a_type= app.config['ANALYSIS_TYPES'][self.type]['label']
                user = db.session.query(User).filter_by(id=user_id).one()
                send_email(user.email,"Your %s has been deleted" % a_type,
                           "auth/email/analysis_deleted",user=user,analysis=self,
                           type=a_type)
                

    def get_assembly_barcode(self,input_number):
        """Gets the assembly barcode for the given number (assembly id) 

        :param input_number: The assembly id

        Returns the assembly barcode
        """	
        number = input_number / 10000
        final_char = ''
        while number > 0:
            final_char += chr(number % 26 + 65)
            number =  (number / 26) 
        final_char= final_char.ljust(4,'A')#
        table_id='_AS'

        return app.config['DB_CODES'][self.database][1] + '_' + final_char[:2] + str(input_number % 10000).zfill(4) + final_char[2:] + table_id		


    def get_summary(self):
        """Get the details of this analysis object

        Returns a dictionary containing the following:-

        * **description** - a string describing the analysis
        * **links** - a dictionary where key is the link's text and value is the url
        * **ms_trees** - a list of strings containing tree_name:tree_id (workspaces only)
        * **snp_projects** - a list of strings containing project_name:project_id (workspaces only)
        * **parameters** - a dictionary of parameters (specified for the object in config.py) plus
        the oiginal owner if a public database
        * **date_created** - date created in YYYY-MM-DD HH:MM format
        * **date_modified** - date modfiied in YYYY-MM-DD HH:MM format	
        """
        ret= {
            "name":self.name,
            "type":self.type,            
        }
        info = self.data.get('data')
        if not  info:
            ret["description"]=""
            ret["links"]={}
            ret["ms_trees"]=[]
            ret["snp_projects"]=[]
        else:
            ret["description"]=info.get("description","")
            ret ["links"]=info.get("links",{})
            ret["ms_trees"]=info.get("ms_trees",[])
            ret["snp_projects"]=info.get("snp_projects",[]) 
        config = app.config['ANALYSIS_TYPES'][self.type]
        parameters = self.data.get("parameters")
        send_param={}
        #get the labels
        if parameters:
            labels = config['parameters']
            for param in parameters:
                send_param[labels[param]]=parameters[param]
        if self.data.get("original_owner"):
            send_param['Original Owner']=self.data.get("original_owner")
        #get status of job if any
        if  config.get("job_required"):
            send_param["Status"]="Running"
            if self.data.get("complete"):
                send_param['Status']="Complete"
            elif self.data.get("failed"):
                send_param["Status"]="Failed"
        url = self.get_url()
        if url:
            ret["url"]=url
        ret['parameters']=send_param
        ret['date_created'] = self.data.get("date_created","")
        ret['date_modified']=self.data.get("date_modified","")

        return ret

    def remove_from_public_folders(self):
        """removes the object from public folders -if it is there"""
        folders=db.session.query(UserPreferences).filter_by(type='public_workspace_folders',database=self.database).one()
        data= ujson.loads(folders.data)
        for f_id in data:
            delete=False
            for a_id in data[f_id]['workspaces']:
                if a_id == self.id:
                    delete=True
                    break
            if delete:
                data[f_id]['workspaces'].remove(self.id)
                break 
        folders.data=ujson.dumps(data)
        db.session.commit()

    def save_summary(self,description,links,trees_to_delete=None):
        """Get the details of this analysis object

        :param links: A dictionary where key is the link's text and value is the url
        :param description: A string describing the analysis
        :param trees_to_delete: A list containg the trees (and snp proects to delete) in the format  tree_name:tree_id
            e.g. *["tree1:233","SNP Project 1:456"]*	
        """		
        d= self.data.get('data')
        if not d:
            self.data['data']={}
            d=self.data['data']
        d['description']=description
        if links:
            d['links']=links
        if trees_to_delete:
            for item in trees_to_delete:
                tid = int(item[0].split(":")[1])
                tree = get_analysis_object(tid)
                if tree:
                    tree.delete()
                #now delete from record
                tree_type= item[1]
                tree_array =  d.get(tree_type)
                if tree_array:
                    if item[0] in tree_array:
                        tree_array.remove(item[0])
        self.update()


class StrainBasedAnalysis(Analysis):
    """The base class for all analysis objects that contain a collection of strains e.g .Trees, Workpaces"""    
    def __init__(self,rec=None,data=None,params=None):
        super(StrainBasedAnalysis,self).__init__(rec,data,params)

    def get_strain_ids(self):
        """Returns a list of all strain ids in the analysis."""
        return self.data['data']['strain_ids']


    def get_strain_number(self):
        """Returns the number of strains in the analysis."""
        return len (self.data['data']['strain_ids'])

    def get_assembly_ids(self):
        """Returns a list of all the assembly ids in the analysis."""
        data = self.data
        sids = self.get_strain_ids()
        sids = map(str,sids)
        sids = ",".join(sids)

        dbase = dbhandle[self.database]
        sql = "SELECT assemblies.id as ass_id FROM assemblies inner JOIN strains ON strains.best_assembly=assemblies.id WHERE strains.id in (%s)" % sids
        results = dbase.execute_query(sql)
        aids = []
        for result in results:
            aids.append(result['ass_id'])
        return aids	


    def get_assembly_barcodes(self):
        """Returns a list of all the assembly barcodes in  the analysis"""
        barcodes=[]
        aids=self.get_assembly_ids()
        for aid in aids:
            barcodes.append(self.get_assembly_barcode(aid))
        return barcodes



    def get_metadata(self):
        """Returns a dictionary of assembly barcode to dictionary of metadata.
        For legacy reasons, the metadata also includes
        ID (corresponding to assembly barcode and StrainID, which is the id of the strain

        .. code-block:: json

            {
                "SAL_YA2044AA_AS":{
        	        "StrainID:242044",
        		"ID":"SAL_YA2044AA_AS",
        		"strain":"bug2,
        		........,
        	},
        	..........
            }
        """
        table_code = app.config['DB_CODES'][self.database][1]
        ids = self.get_strain_ids()
        ids =map(str, self.data['data']['strain_ids'])
        strain_data,idsg=process_strain_query(self.database,",".join(ids),"ids",-1)
        return_dict={}
        for strain in strain_data:
            ass_barcode = self.get_assembly_barcode(strain['best_assembly'])
            strain['ID']= ass_barcode
            strain['StrainID']=strain['id']
            del strain['best_assembly']
            del strain['owner']
            del strain['assembly_status']
            return_dict[ass_barcode]=strain
        return return_dict

    def get_files(self):
        """Returns a list of all the files for this object. in the format of of a list containing the name of the key
        in the object's data pointing to the file and what the file should be called

        e.g. ["raxml_tree","tree.nwk"]

        subclasses should add any extra files with the code like the following 

        .. code-block:: python

            def get_files(self)
                self.files.append(["data_file","data.json"])
                self.files.append(["layout_file","layout.json"])
               #call to super class
               super(TreeAnalysis,self).get_files()
        """
        self.files.append(['core_accessory_info','core_accessory.json'])


    def make_core_accessory(self):
        """Calculates and returns the core accessory genome (see :meth:`calculate_accessory_genome` for the format).
        The data is stored in a file, pointed to by the *core_accessory_key* in the object's data and can be retrieved with
        with :func:`StrainBasedAnalysis.get_accessory_genome`
        """	
        aids =self.get_assembly_ids()
        accessory = calculate_accessory_genome(self.database,aids)
        record = db.session.query(UserPreferences).filter_by(id = self.id).one()
        data = ujson.loads(record.data)
        folder= os.path.join(app.config['BASE_WORKSPACE_DIRECTORY'],str(self.database),str(self.user_id),str(self.id))
        if not os.path.exists(folder):
            os.makedirs(folder)
        file_name = os.path.join(folder,"core_accessory.json")
        file_handle = open(file_name,"w")
        file_handle.write(ujson.dumps(accessory))
        file_handle.close()
        data['core_accessory_info']=file_name
        record.data = ujson.dumps(data)
        db.session.commit()
        return accessory

    def get_accessory_genome(self):
        """Returns the core accessory genome (see :meth:`calculate_accessory_genome` for the format).
        If it has not been calculated, None will be returned
        """	
        file_name = self.data.get("core_accessory_info")
        if not file_name:
            return None
        file_handle = open(file_name)
        data = ujson.loads(file_handle.read())
        return data

    def get_nwk_tree(self):
        """Returns a string of the tree (nwk format) associated with this object. Returns none if there is no 
        associated tree.
        """		
        return None

class TreeAnalysis(StrainBasedAnalysis):
    def __init__(self,rec=None,data=None,params=None):
        super(TreeAnalysis,self).__init__(rec,data,params)


    def get_layout(self,tree_type=None):
        """Gets the layout file which describes the presentation of the tree.
        Becauase the analysis may have more than one way of showing the
        tree, sometimes tree type has to be specified.

        :param tree_type: The type of tree

        The actual format of the returned data will depend on the type of tree
        """	
        if not self.data.get("layout_file"):
            return ""
        in_handle = open(self.data['layout_file'])
        layout = ujson.loads(in_handle.read())
        in_handle.close()
        return layout	    



    def save_layout(self,data,tree_type=None):
        """saves the layout file. Becauase the analysis may have more than one way of showing the
        tree sometimes tree type has to be specified

        :param data:  a jsonified version of the layout data (the structure will differ depending on tree_type)
        :param tree_type: The type of tree
        """		
        folder = self.get_folder()
        file_name= os.path.join(folder,"layout.json")
        file_handle= open(file_name,"w")
        file_handle.write(data)
        file_handle.close()
        self.data['layout_file']=file_name
        self.update()


    def get_metadata(self):
        """see :func:`StrainBasedAnalysis.get_metadata`
        """		
        if not self.data.get('data_file'):
            return None
        data_handle =open(self.data['data_file'])
        data= ujson.loads(data_handle.read())
        metadata= data['isolates']
        return_dict={}
        for strain in metadata:
            return_dict[strain['ID']]=strain
        return return_dict

    def get_files(self):
        """see :func:`StrainBasedAnalysis.get_files`
        """       
        self.files.append(["data_file","data.json"])
        self.files.append(["layout_file","layout.json"])
        super(TreeAnalysis,self).get_files()

    def save_metadata(self,metadata,metadata_options,current_metadata):
        """updates any changes in the metadata

        :param metadata: A dictionary of strain id to a dictionary of key value pairs e.g.

            .. code-block:: json

                {
                    34532:{
                        "color":"blue",
                        "source_details":"food"
                    },
                    34531:{
                        "color":"red"
                    },
                    ......
                }

        :param metadata_options: A dictionary of field name (category) to a dictionary
            describing that category e..g.

            .. code-block:: json

                {
                    "continent":{
                        "category":"Metadata",
                        "coltype":"character",
                        "grouptype":"size",
                        "colorscheme":"category",
                        "label":"Continent"
                    },
                    ..........
                }

        :param current_metadata: The last category displayed in the tree

        """
        data_handle =open(self.data['data_file'])
        data= ujson.loads(data_handle.read())
        data_handle.close()
        if metadata:
            for  strain in data['isolates']:	
                sid = str(strain['StrainID'])
                new_metadata= metadata.get(sid)
                if new_metadata:
                    for key in new_metadata:
                        strain[key]=new_metadata[key]

        data['metadata_options']=metadata_options
        data['current_metadata']=current_metadata
        data_handle=open(self.data['data_file'],"w")
        data_handle.write(ujson.dumps(data))
        data_handle.close()

    def synch_metadata(self):
        """Checks to seeif any data in Enterobase (both strain metadata and custom columns)
        is different to that in the tree and corrects the tree's data if this is the case i.e. modifies 
        the data file

        Returns information about any data in the tree which has been updated in the form of a 
        dictionary of strain id to a dictionary of field:new value
        """		
        dbase = dbhandle[self.database]

        data_handle =open(self.data['data_file'])
        data= ujson.loads(data_handle.read())
        data_handle.close()	
        sid_list=[]
        isolate_index={}
        ass_index={}
        ass_ids=[]
        #get the tree metadata
        for count,isolate in enumerate(data['isolates']):
            sid_list.append(isolate['StrainID'])
            isolate_index[isolate['StrainID']]=count
            ass_id = decode_barcode(isolate['ID'])
            ass_index[ass_id]=count
            ass_ids.append(str(ass_id))
        ass_ids= ",".join(ass_ids)
        sids = map(str,sid_list)
        #get strain_data
        strain_data,idsg=process_strain_query(self.database,",".join(sids),"ids",-1)
        #get custom_data
        custom_columns=[]
        custom_column_list=[]
        for key in data['metadata_options']:
            obj = data['metadata_options'][key]
            cat =obj.get("category")
            if cat == 'Custom':
                custom_columns.append({"id":key})
                custom_column_list.append(key)

        obj={"custom_columns":custom_columns}   
        cust_data,aids =process_custom_view_query (self.database,"",ass_ids,"assembly_ids",obj)

        fields = dbase.get_strain_fields()
        changes ={}
        for entero_record in strain_data:
            sid = entero_record['id']
            tree_record= data['isolates'][isolate_index[sid]]
            for  field in fields:
                entero_value = entero_record.get(field)
                tree_value = tree_record.get(field)
                if entero_value <> tree_value:
                    ch_dict= changes.get(sid)
                    if not ch_dict:
                        ch_dict={}
                        changes[sid]=ch_dict
                    if entero_value == None:
                        del tree_record[field]
                        ch_dict[field]=""
                    else:
                        tree_record[field]=entero_value
                        ch_dict[field]=entero_value
        for ass_id in cust_data:
            entero_record = cust_data[ass_id]

            tree_record = data['isolates'][ass_index[ass_id]]
            sid = tree_record['StrainID']
            for field in custom_column_list:
                entero_value = entero_record.get(field)
                tree_value = tree_record.get(field)
                if entero_value <> tree_value:
                    ch_dict= changes.get(sid)
                    if not ch_dict:
                        ch_dict={}
                        changes[sid]=ch_dict
                    if entero_value == None:
                        del tree_record[field]
                        ch_dict[field]=""
                    else:
                        tree_record[field]=entero_value
                        ch_dict[field]=entero_value                
        data_handle = open(self.data['data_file'],"w")
        data_handle.write(ujson.dumps(data))
        data_handle.close()
        return changes

    def get_nwk_tree(self):
        """Returns a string containing the nwk file (with assembly barcode as identifiers)
        """	
        if not self.data.get("raxml_tree"):
            return None
        nwk_file = open(self.data['raxml_tree'],"r")
        nwk = nwk_file.read()
        nwk_file.close()
        #legacy replace  project name with ref barcode
        name = self.name.replace(" ","_").replace("/","_").replace(".","_").replace("&","_")
        ref_barcode = self.ref_barcode
        if name in nwk:
            nwk = nwk.replace(name,ref_barcode)
            nwk_file = open(self.data['raxml_tree'],"w")
            nwk_file.write(nwk)
            nwk_file.close()
        if "Reference" in nwk:
            nwk = nwk.replace("Reference",ref_barcode)
            nwk_file = open(self.data['raxml_tree'],"w")
            nwk_file.write(nwk)
            nwk_file.close()

        return nwk	

class WorkSpace(StrainBasedAnalysis):
    """Represents a workspace i.e. a list of strains
    """
    def __init__(self,rec=None,data=None,params=None):
        super(WorkSpace,self).__init__(rec,data,params)

    @staticmethod
    def create_workspace(name,user_id,database,strain_ids,
                         current_page=None,experiment_data=None,
                         sort_order=None,grid_params=None):
        """Updates the view with the supplied

        :param name: the name of the workspace
        :param user_id: the user id or 0 if public
        :param strain_ids: a list of all strain ids in the in the workspace
        :param current_page: the number of the current page (optional)
        :param experimental_data: the name of the scheme displayed in the right hand grid.
            Will be 'assembly stats is none is supplied.

        :param sort_order: A list of the sort critera(optional), where the first value is None,followed
        	    by five arrays containing the table (main or experment), column name and 1 or -1 
        	    (ascending or descending) e.g. 

        	    .. code-block:: json

        	        [
        		    None,
        		    ["main","Location",1],
        		    ["assembly_stats","coverage",-1],
        		    ["None","None",1],
        		    ["None","None",1],
        		    ["None","None",1]
                    ]

        Returns the workspace object
        """

        params={"name":name,"user_id":user_id,"type":"main_workspace","database":database}
        base_data={"data":{}}
        data=base_data["data"]
        data['strain_ids']=strain_ids
        if  current_page or current_page==0:
            data['current_page']=current_page
        if not experiment_data:
            experiment_data='assembly_stats'
        data['experimental_data']='assembly_stats'
        if grid_params:
            base_data['grid_params']=grid_params
        if sort_order:
            data['sort_order']=sort_order

        ws= WorkSpace(params=params,data=base_data)
        return ws






    def update_view(self,data):
        """Updates the view with the supplied
        :param data: the data is dictionary of grid params and data containing
            current_page, experimental_data,strain_ids,sort_order. See 
            :func:`Workspace.create_workspace'

        """	

        self.data['grid_params']=data['grid_params']
        self.data['data']['current_page'] = data['data']['current_page'] 
        self.data['data']['experimental_data']=data['data']['experimental_data']
        self.data['data']['sort_order'] = data['data']['sort_order']
        self.data['data']['strain_ids'] = data['data']['strain_ids']
        self.update()


    def get_children(self):
        """Returns any SNP projects or MS trees owned by the workspace
        a list of workspace arrays i.e. name,user_name,user_id,id,type with
        the user_name and user_id being 'temp' e.g.
         .. code-block:: json

                [
        		    ["Tree 101","temp","temp",6353,"ms_sa_tree],
        		    ["SNP mice isolates,"temp","temp",27272,"snp_sa_project"]
        		]
        """
        snps = self.data['data'].get('snp_projects')
        children=[]
        if snps:
            for snp in snps:
                arr = snp.split(":")
                #K.M.
                # test length of of the list before calling it items, the length should be at lest 2
                #if not it will continue and will not add it to the list and send an error email so the system adminstrator should check that
                if len(arr)>=2:
                    children.append([arr[0],"temp","temp",int(arr[1]),"snp_sa_project"])
                else:
                    app.logger.error("Could load snp project for %s" % snp)
                    continue
        msts = self.data['data'].get("ms_trees")
        if msts:
            for mst in msts:
                arr=mst.split(":")
                #KM
                #the same as snp project
                #test length of of the list before calling it items as it is throw an error
                #and get_workspace_children call [GET] will failed
                if len(arr)>=2:
                    children.append([arr[0],"temp","temp",int(arr[1]),"ms_sa_tree"])
                else:
                    app.logger.error("Could load ms tree for %s"%mst)
                    continue

        return children

    def delete(self,send_user_email=False):
        """see :func:`Analysis.delete`"""       	
        #delete all trees
        for tree_type in ["ms_trees","snp_projects"]:
            trees = self.data['data'].get(tree_type)
            if trees:
                for item in trees:
                    tree_id = int(item.split(":")[1])
                    tree = get_analysis_object(tree_id)
                    if tree:
                        tree.delete()
        super(WorkSpace,self).delete(send_user_email)

    def get_assembly_ids(self):
        """see :func:`StrainBasedAnalysis.get_assembly_ids`""" 
        data = self.data
        sids = data['data']['strain_ids']
        sids = map(str,sids)
        sids = ",".join(sids)
        dbase = dbhandle[self.database]
        sql = "SELECT assemblies.id as ass_id FROM assemblies inner JOIN strains ON strains.best_assembly=assemblies.id WHERE strains.id in (%s)" % sids
        results = dbase.execute_query(sql)
        aids = []
        for result in results:
            aids.append(result['ass_id'])
        return aids

    def get_strain_ids(self):
        """see :func:`StrainBasedAnalysis.get_strains_ids`""" 
        return self.data['data']['strain_ids']

    def get_metadata(self):
        """see :func:`StrainBasedAnalysis.get_metadata`""" 
        table_code = app.config['DB_CODES'][self.database][1]
        ids =map(str, self.data['data']['strain_ids'])
        strain_data,idsg=process_strain_query(self.database,",".join(ids),"ids",-1)
        return_dict={}
        for strain in strain_data:
            ass_barcode = self.get_assembly_barcode(strain['best_assembly'])
            strain['ID']= ass_barcode
            strain['StrainID']=strain['id']
            del strain['best_assembly']
            del strain['owner']
            del strain['assembly_status']
            return_dict[ass_barcode]=strain
        return return_dict

    def get_strain_number(self):
        """see :func:`StrainBasedAnalysis.get_strain_number`""" 
        return len(self.data['data']['strain_ids'])


class SNPProject(TreeAnalysis):
    def __init__(self,rec,data=None,params=None):
        #call to super instructor
        super(SNPProject,self).__init__(rec,data,params)
        self.ref_id = self.data['ref_id']
        self.ref_barcode=self.data.get('ref_barcode')
        #legacy data
        if not self.ref_barcode:
            self.ref_barcode = self.get_assembly_barcode(self.ref_id)
            self.data['ref_barcode']=self.ref_barcode
            self.update()



    #creates SNP project from scratch
    @staticmethod
    def create_snp_project(data,params,workspace_id):
        """Creates the datbase with supplied data

        :param params: a dictionary containing

            * name
            * user_id
            * database

        :param data: a dictionary of the following

            * ref_id: The refererence id
            * snp_list: The id the predefined snp list (0 if none)
            * missing_sites: the min fraction sites present for an snp to be called (0 to 1.0)
            * send_email: Send an email to user on completion (True or False)
            * create_annotation: Annotate the project in Jbrowse (True or False)
            * assembly_ids: all the assembly ids (it doesn't matter if the ref id is present)
            * parameters
                * strain_number: The number of strains
        	* reference: The name of the reference strain
        	* snp_list: The name of the snp list or 'None'
        	* missing_sites: The missinf_sites (see above) as a % (0-100)


        Returns the the snp object
        """		


        database = params['database']
        user_id = params['user_id']

        #add barcode
        db_code = app.config['DB_CODES'][params['database']][1]
        ref_barcode = db_code +"_"+get_assembly_barcode(data['ref_id'])+"_AS"
        data['ref_barcode']=ref_barcode

        #stand alone or part of workspace
        p_type = 'snp_project'
        if not workspace_id:
            p_type = "snp_sa_project" 
        params['type']=p_type

        #remove reference if in assembly list
        if data['ref_id'] in data['assembly_ids']:
            data['assembly_ids'].remove(data['ref_id'])	       	

        #create the object - need the id to create the folder
        snp = SNPProject(None,data,params)


        folder = os.path.join(app.config[ 'BASE_WORKSPACE_DIRECTORY'],database,str(user_id),str(snp.id))
        if not (os.path.exists(folder)):
            os.makedirs(folder)
        data_file = os.path.join(folder,"data.json")
        snp.data['data_file']=data_file


        #get the metadata
        ass_ids = map(str,snp.data['assembly_ids'])
        ass_ids = ",".join(ass_ids)
        sid_to_bar={}
        strain_ids = [];
        sql = "SELECT strains.id AS sid,assemblies.barcode AS abc,assemblies.id AS aid FROM strains INNER JOIN assemblies ON best_assembly=assemblies.id WHERE best_assembly  IN (%s) OR best_assembly=%i" % (ass_ids,data['ref_id'])
        dbase =dbhandle[database]
        results = dbase.execute_query(sql)
        ref_barcode = ""
        for result in results:
            sid_to_bar[result['sid']]=result['abc']
            strain_ids.append(str(result['sid']))
        strain_data,idsg=process_strain_query(database,",".join(strain_ids),"ids",-1)
        for strain in strain_data:
            sid =  strain['id']
            strain['ID']= sid_to_bar[sid]
            strain['StrainID']=sid
            del strain['id']
        #get the fileds
        fields = dbase.get_strain_fields()
        metadata_options = {}
        for field in fields:
            metadata_options[field]={"label":fields[field],"category":"Metadata"}

        data_handle= open(data_file,"w")
        data_handle.write(ujson.dumps({"isolates":strain_data,"metadata_options":metadata_options}))
        data_handle.close()

        snp.update()


        #update parent workspace with the id
        if p_type=="snp_project":
            orig = db.session.query(UserPreferences).filter_by(id=workspace_id,type="main_workspace",database=database).first()
            data = ujson.loads(orig.data)
            snp_projects= data['data'].get("snp_projects")
            if not snp_projects:
                snp_projects=[]
                data['data']['snp_projects']=snp_projects
            snp_projects.append(snp.name+":"+str(snp.id))
            orig.data = ujson.dumps(data)
            db.session.commit()

        return snp

    def save_layout(self,data,tree_type=None):
        """See :func:`TreeAnalysis.save_layout`. By default it is assumed the layout data
        is a phylogenetic tree. If tree_type is 'ms' then the layout data should be that of an
        MS tree"""

        folder = self.get_folder()
        name= "layout.json"
        if tree_type=='ms':
            name="mst_layout.json"
        file_name= os.path.join(folder,name)
        file_handle= open(file_name,"w")
        file_handle.write(data)
        file_handle.close()
        name = 'layout_file'
        if tree_type=='ms':
            name= "mst_layout_file"
        self.data[name]=file_name
        self.update()    

    def send_ref_mapper_job(self,workgroup="user_upload",priority=-9):
        '''Checks to see if any assemblies are required to be sent to refMapper
        and will send any if necessay
        
        Returns True if successfully sent.,False if not or None if none had to be sent
        '''
        from entero.jobs.jobs import RefMapperJob
        success=True
        dbase = dbhandle[self.database]
        try:
            Schemes = dbase.models.Schemes
            Lookup = dbase.models.AssemblyLookup
            
            snp_scheme = dbase.session.query(Schemes).filter_by(description='snp_calls').first()
            already_called = dbase.session.query(Lookup).filter(Lookup.scheme_id==snp_scheme.id,
                                                                    Lookup.assembly_id.in_(self.data['assembly_ids']),
                                                                    Lookup.st_barcode==self.ref_barcode).all()
            existing_aids=set()
            for item in already_called:
                existing_aids.add(item.assembly_id)
            
            aids_to_send=list(set(self.data['assembly_ids'])-existing_aids)
            if len(aids_to_send)==0:
                self.data['ref_mapper']="not_needed"
                self.update()
                return None
            #need the reference file pointer
            aids_to_send.append(self.ref_id)
            Assemblies=dbase.models.Assemblies
            assemblies =dbase.session.query(Assemblies).filter(Assemblies.id.in_(aids_to_send)).all()
            inputs={"queries":{}}
            params={"prefix":self.ref_barcode}
            for ass in assemblies:
                if not ass.file_pointer or not os.path.exists(ass.file_pointer):
                    continue
                    raise Exception("file_pointer (%s) is not valid or exist for this assembly %s " % (ass.file_pointer, ass.barcode))
                if ass.id == self.ref_id:
                    inputs['reference']=ass.file_pointer
                else:
                    inputs['queries'][ass.barcode]=ass.file_pointer
            tag = self.name+":"+str(self.id)
            job = RefMapperJob(inputs=inputs,params=params,
                                              database=self.database,tag=tag,priority=priority,
                                              user_id =self.user_id,workgroup=workgroup)
            # WVN 5/3/18 Misspelling of success is sure to be a bug - not sure of the ramifications
            # of fixing it in the obvious way yet (e.g. could that expose another bug)
            #KM (25/07/2018) it the job is not sent (success is False) which idicates the job is failed and should be marked as failed
            #Otherwise it will be assumed that it sent and will marked as queued which is not correct
            # and it will generate error while getting job.id as we already get from server error emails
            success = job.send_job()
            if success:
                self.data["ref_mapper_id"]=job.job_id
                self.data["ref_mapper"]="queued"
            
        except Exception as e:
            app.logger.exception("RefMapper for snp project %i could not be sent" % self.id )
            dbase.rollback_close_session()
            success=False
        if not success:
            self.data['ref_mapper']="not_sent"
            self.data['failed']="true"
        self.update()
        return success
    
    def send_matrix_phylogeny_job(self,priority=-9,workgroup="user_upload",
                                                            force=True):
        from entero.jobs.jobs import MatrixPhylogenyJob
        if not force and (self.data.get("raxml_tree", False) or self.data.get('matrix_phylogeny', 'not_sent') == 'queued'):
            return False
        self.data['matrix_phylogeny']="queued"
        self.update()
        
        inputs ={"matrix":self.data['matrix']}
        params={"prefix":self.ref_barcode}
        tag = self.name+":"+str(self.id)
        job = MatrixPhylogenyJob(inputs=inputs,params=params,
                                                  database=self.database,tag=tag,
                                                  priority=priority,workgroup=workgroup,
                                                  user_id=self.user_id)
        success=job.send_job()
        if success:
            self.data['matrix_phylogeny_id']=job.job_id
            
        else:
            if not self.data.get("failed"):
                self.send_job_failed_email()
            self.data['matrix_phylogeny']="not_sent"
            self.data['failed']="true"
        self.update()

    def send_job_failed_email(self):
        user = db.session.query(User).filter_by(id=self.user_id).one()
        if self.data.get("send_email") and not self.data.get("failed"):
            send_email(user.email,"Your SNP Project Has Failed","auth/email/snp_failed",user=user,name=self.name)
        if app.config['ANALYSIS_TYPES']['snp_project']["inform_administrator_on_fail"]:
                send_email(app.config['ENTERO_MAIL_SENDER'],"Your SNP Project Has Failed","auth/email/snp_failed",user=user,name=self.name)
    
    def send_job_complete_email(self):
        if self.data.get("send_email") and not self.data.get("complete"):
            user = db.session.query(User).filter_by(id=self.user_id).one()
            url = "enterobase.warwick.ac.uk/species/%s/snp_project/%s" %(self.database,str(self.id))
            send_email(user.email,"Your SNP Project is Complete","auth/email/snp_done",user=user,url=url)

    def send_ref_mapper_matrix_job(self,force =False,priority=-9,workgroup="user_upload"):
        from entero.jobs.jobs import RefMapperMatrixJob
        if not force and (self.data.get("matrix", False) or self.data.get('ref_mapper_matrix', 'not_sent') == 'queued' ):
            return None
        self.data['ref_mapper_matrix']="queued"
        self.update()
        success = True
        try:
            dbase=dbhandle[self.database]
            #get the snp scheme id i
            
            Schemes=dbase.models.Schemes
            snp_scheme= dbase.session.query(Schemes.id).filter_by(description='snp_calls').first()
            snp_scheme_id=snp_scheme.id
    
            #need  to get other data about the assembly
            Assemblies = dbase.models.Assemblies
            Lookup = dbase.models.AssemblyLookup 
            reference = dbase.session.query(Assemblies).filter(Assemblies.id ==self.ref_id).first()
            
            
            lookups = dbase.session.query(Lookup).filter(Lookup.assembly_id.in_(self.data['assembly_ids']),
                                                         Lookup.scheme_id==snp_scheme_id,
                                                         Lookup.st_barcode == self.ref_barcode
                                                         ).all()
            #get barcode:gff files
            gff_files={}
            for item in lookups:
                data= ujson.loads(item.other_data) 
                #at the moment is double encoded
                if type(data) ==unicode:
                    data = ujson.loads(data)
                gff_pointer = data.get('file_pointer')
                barcode = dbase.encode(item.assembly_id,self.database,"assemblies")
                gff_files[barcode]=gff_pointer
    
            #get the refmasker
            ref_masker_scheme= dbase.session.query(Schemes).filter_by(description="ref_masker").first()
            ref_masker = dbase.session.query(Lookup).filter_by(assembly_id=self.ref_id,scheme_id=ref_masker_scheme.id).first()
            masker_filepointer= ref_masker.other_data['file_location']
            #check for gff file
            #first of all check genbank file
            gene_file=None
            if reference.other_data:
                data = ujson.loads(reference.other_data)
                if data.get("annotation"):
                    gene_file=data.get("annotation")
            #then check prokka annotation
            if not gene_file:
                sql = "SELECT other_data FROM assembly_lookup INNER JOIN schemes ON schemes.description='prokka_annotation' AND schemes.id = scheme_id AND assembly_id=%i" % reference.id
                results = dbase.execute_query(sql)
                if len(results) > 0:
                    try:
                        gene_file = results[0]['other_data']['results']['gff_file']
                    except Exception as e:
                        gene_file=None

            #format all job parameters
            inputs={}
            params={}
            params['prefix']=reference.barcode
            params['relax_core']=self.data.get('missing_sites',0.9)
            if gene_file:
                inputs['genes']=gene_file
            #change the reference filepointer from fastq to fasta
            ref_filepointer = reference.file_pointer
            if ref_filepointer.endswith("q"):
                ref_filepointer = ref_filepointer[:-1]+"a"
            if not ref_filepointer or not os.path.exists(ref_filepointer):
                raise Exception ("ref file pointer (%s) is not valid or exist for this assembly %s"%(ref_filepointer, reference.barcode))
            inputs['reference']=ref_filepointer
            inputs['repeat'] = masker_filepointer
            inputs['queries']= gff_files
            
            #add predifned snp list if any
            snp_list_id=self.data.get("snp_list")
            if snp_list_id:
                snp_list = get_analysis_object(int(snp_list_id))
                inputs['snp_sites']=snp_list.data['snp_list_simple']

            job = RefMapperMatrixJob(inputs=inputs,params=params,
                                                    user_id=self.user_id,database=self.database,
                                                    tag=self.name+":"+str(self.id),priority=priority,
                                                    workgroup = workgroup)
            success = job.send_job()
            if success:
                self.data['ref_mapper_matrix_id']=job.job_id
        except Exception as e:
            app.logger.exception("Unable to send refMapperMatrix Job for snp project:%i" % self.id)
            success=False
        if not success:
            self.data['ref_mapper_matrix']="not_sent"
            self.data['failed']="true"
        self.update() 
        return success

    def get_barcode_to_field(self,field):
        """This method will return a dictionary of assembly barcode to the value of the specified field

        :param field: The field in metadata to get the value for

        e.g. if country was given as field, the following would be returned

        ..code-block ::json

            {
                  "SAL_WA9237AA_AS":"Germany",
        	  "SAL_YA1005AA_AS": "France",
        	  ..............
            }
        """	
        ass_list = ",".join(map(str,self.data['assembly_ids'] + [self.ref_id]))
        sql = "SELECT assemblies.barcode AS bar ,strains.%s AS val FROM strains INNER JOIN traces ON traces.strain_id=strains.id INNER join trace_assembly ON traces.id = trace_assembly.trace_id INNER JOIN assemblies ON assemblies.id = trace_assembly.assembly_id WHERE assembly_id IN (%s)" % (field,ass_list)
        results= dbhandle[self.database].execute_query(sql)
        ret_dict={}
        for result in results:
            ret_dict[result['bar']]=result['val']
        return ret_dict




    def get_assembly_ids(self):
        """See :func:`StrainBasedAnalysys.get_assembly_ids`"""
        ret_list = self.data['assembly_ids'] + [self.ref_id]
        return ret_list

    def get_strain_ids(self):
        """See :func:`StrainBasedAnalysys.get_strain_ids`"""
        dbase =dbhandle[self.database]
        Strains=dbase.models.Strains
        ass_list =self.data['assembly_ids'] + [self.ref_id]
        id_list=[]
        records = dbase.session.query(Strains.id,Strains.best_assembly).filter(Strains.best_assembly.in_(ass_list)).all()
        for rec in records:
            id_list.append(rec.id)
        return id_list


    def get_strain_number(self):
        """See :func:`StrainBasedAnalysys.get_strain_number`"""
        return len(self.data['assembly_ids'])+1

    def get_gff_files(self):
        """Returns  a dictionary of assembly id to gff file ponters (that contain snp information 
        for all the strains in the snp project)"""
        dbase =dbhandle[self.database]
        Lookup = dbase.models.AssemblyLookup
        Schemes = dbase.models.Schemes
        snp_scheme=dbase.session.query(Schemes.id).filter_by(description="snp_calls").first()
        lookups = dbase.session.query(Lookup).filter(Lookup.assembly_id.in_(self.data['assembly_ids']),
                                                     Lookup.scheme_id==snp_scheme.id,
                                                     Lookup.st_barcode == self.data['ref_barcode']
                                                     ).all()
        aid_to_gff={}
        for item in lookups:
            data= ujson.loads(item.other_data)
            #sometimes entry is double json encoded
            if type(data) is not dict:
                data =ujson.loads(data)
            gff = data.get('file_pointer')

            aid_to_gff[item.assembly_id] = gff   
        return aid_to_gff


    def get_gaps(self):
        """Returns  a dictionary of assembly id to gff files (that contain snp information 
        for all the strains in the snp project as dictionary with the following format:-

        .. code-block :: json

            {
                contig_id:{
        	    start_position:{
        	          strain_id: deletion_length,
        		  .........

        	    },
        	    ........

        	},
        	..........
        """
        dbase =dbhandle[self.database]
        ass = dbase.session.query(dbase.models.Assemblies.file_pointer).filter_by(id=self.ref_id).first()
        if ass.file_pointer[-1]=="q":
            fp = ass.file_pointer[:-1]+"a"
        else:
            fp =ass.file_pointer
        node_lengths={}
        fasta_sequences = SeqIO.parse(open(fp),'fasta')

        for fasta in fasta_sequences:
            node_lengths[fasta.id]=len(fasta.seq)
        gff_files=self.get_gff_files()
        all_gaps={}
        for gff in gff_files:

            in_handle = open(gff_files[gff])
            data = in_handle.readlines()
            last_node=None
            max_node_end=0
            last_node_length=0
            node_length=0
            for line in data:
                line=line.strip()      
                arr = line.split("\t")
                if arr[2] <> 'misc_feature': 
                    break


                start = int(arr[3])
                node_length =node_lengths[arr[0]]
                end= int(arr[4])
                if  arr[0] <> last_node:
                    if last_node:
                        if max_node_end<last_node_length:
                            self._add_gap_to_dict(all_gaps,last_node,max_node_end+1,last_node_length,gff)
                    if start>1:
                        self._add_gap_to_dict(all_gaps,arr[0],1,start-1,gff)
                    max_node_end=0
                else:
                    if start>max_node_end+1:
                        self._add_gap_to_dict(all_gaps,arr[0],max_node_end+1,start-1,gff)

                last_node=arr[0]
                last_node_length=node_length
                if end>max_node_end:
                    max_node_end=end
            if max_node_end+1<node_length:
                self._add_gap_to_dict(all_gaps,arr[0],max_node_end+1,node_length,gff)

        return all_gaps



    def _add_gap_to_dict(self,gaps,contig,start,end,ass_id):
        if end-start>2000:
            print str(end-start)
        item = gaps.get(contig)
        if not item:
            item = {}
            gaps[contig]=item
        record = item.get(start)
        if not record:
            record={}
            item[start]=record
        record[ass_id]=end


    def create_stats(self):
        """Generates stats for the snp project which include

        * the number of snps for each strain
        * the total number of variant sites
        """
        strain_pos = {}
        snp_number={}
        ref_pos=0
        total_snps=0



        #get some stats from the matrix file
        with gzip.open(self.data['matrix'],"r") as f: 
            for line in f:
                line=line.strip()
                if  line.startswith("##"):
                    continue
                if line.startswith("#Seq"):
                    arr = line.split("\t")
                    for count,barcode in enumerate(arr):
                        if barcode.startswith("#"):
                            continue
                        if ref_pos==0:
                            ref_pos=count
                        else:
                            strain_pos[count]=barcode
                            snp_number[barcode]=1
                    continue
                arr=line.split("\t")
                ref_base = arr[ref_pos]
                total_snps+=1
                for pos in strain_pos:
                    if arr[pos]<>ref_base:
                        barcode = strain_pos[pos]
                        snp_number[barcode]+=1

        self.data['parameters']['variant_sites']=total_snps
        self.data['snp_count']=snp_number
        self.update()





    def create_vcf_file(self):
        """Creates a vcf file from the snp project, which can be used by JBrowse
        """	
        folder = self.get_folder()
        all_gaps = self.get_gaps()
        vcf_name = "%i.vcf" % self.id
        vcf_file=os.path.join(folder,vcf_name)

        meta= self.get_metadata()
        to_write={}
        for barcode in meta:
            to_write[barcode]={"name":meta[barcode]['strain']}
        out_file= open(vcf_file,"w")
        #snp list stuff
        snp_list_info=None
        if self.data.get("snp_list"):
            snp_list_info=get_analysis_object(int(self.data.get("snp_list")))
        out_file.write("##fileformat=VCFv4.2\n")
        out_file.write("##metadata=")
        out_file.write(ujson.dumps(to_write)+"\n")
        out_file.write('##FORMAT=<ID=GT,Number=1,Type=Integer,Description="Genotype">\n')
        out_file.write('##FORMAT=<ID=LN,Number=1,Type=Integer,Description="Length of SV">\n')
        out_file.write('##INFO=<ID=SYN,Number=1,Type=String,Description="SNP Effect">\n')
        out_file.write('##INFO=<ID=NIF,Number=1,Type=String,Description="Non Informative">\n')
        if snp_list_info:
            for field in snp_list_info.fields:
                name = field.replace(" ","_")
                out_file.write(('##INFO=<ID=%s,Number=1,Type=String,Description="%s">\n')% (name,field))

        out_file.write('##ALT=<ID=DEL,Description="Deletion">\n')
        out_file.write("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT")
        pos_to_ass_id={}
        strain_end=0
        syn_pos=None
        dbase =dbhandle[self.database]
        f_file={}
        strain_number = self.get_strain_number()-1
        ass = dbase.session.query(dbase.models.Assemblies.file_pointer).filter_by(id=self.ref_id).first()
        if ass.file_pointer[-1]=="q":
            fp = ass.file_pointer[:-1]+"a"
        else:
            fp =ass.file_pointer
        node_lengths={}
        fasta_sequences = SeqIO.parse(open(fp),'fasta')

        for fasta in fasta_sequences:
            f_file[fasta.id]=fasta.seq

        with gzip.open(self.data['matrix']) as f:
            snp_id=1
            for line in f:
                arr = line.strip().split("\t")
                if line.startswith("#Seq"):
                    pos=0
                    #get all the sample names and store their positions
                    for name in arr[3:]:
                        if name.startswith("#"):
                            syn_pos=pos+4
                            break
                        out_file.write("\t"+name)
                        pos_to_ass_id[pos]=decode_barcode(name)
                        pos+=1
                    out_file.write("\n")
                    strain_end=pos+3
                    continue
                if line.startswith("##"):
                    continue
                #add any gaps (deletions) to the vcf
                gap = all_gaps.get(arr[0])
                if gap:
                    #get all deletions that are before the next snp
                    to_add=[]
                    for pos in gap:
                        if pos<int(arr[1]):
                            to_add.append(pos)
                    to_add.sort()
                    #add them to the vcf file and delete them from the dictionary
                    for pos in to_add:
                        info = gap[pos]
                        this_base=f_file[arr[0]][pos-1]
                        out_file.write(arr[0]+"\t"+str(pos)+"\t.\t"+this_base+"\t<DEL>")
                        out_file.write("\t.\tPASS\tSYN=NA\tGT:LN")
                        for i in pos_to_ass_id:
                            end = info.get(pos_to_ass_id[i])
                            if not end:
                                out_file.write("\t0:0")
                            else:
                                out_file.write("\t1:"+str(end))
                        out_file.write("\n")
                        del gap[pos]





                alt= {arr[2]:0}
                alt_list=[]
                count=1
                ref_base=arr[2]
                mut_number=0
                for base in arr[3:strain_end]:
                    if base=='-':
                        base='N'
                    already = alt.get(base)
                    if base <> ref_base:
                        mut_number=mut_number+1
                    if already == None:
                        alt[base]=count
                        alt_list.append(base)
                        count+=1


                #indels corresponding 
                if arr[2]==".":
                    plus_or_minus = alt_list[0][0]
                    bef_base= f_file[arr[0]][int(arr[1])-2]
                    this_base = f_file[arr[0]][int(arr[1])-1]
                    ref_base= alt_list[0][1:]
                    #deletion
                    #CTACGC -> CT--GC would be TAC   T at pos 2
                    pos = str(int(arr[1])-1)
                    if plus_or_minus=="-":

                        out_file.write(arr[0]+"\t"+pos+"\tsnp_"+str(snp_id)+"\t"+bef_base+ref_base+"\t")
                        out_file.write(bef_base)
                #insertion CTACGC -> CTAgggCGC would be TA TAGGGG	
                    else:
                        out_file.write(arr[0]+"\t"+pos+"\tsnp_"+str(snp_id)+"\t"+bef_base+"\t")
                        out_file.write(bef_base+this_base+ref_base)			

                else:
                    out_file.write(arr[0]+"\t"+arr[1]+"\tsnp_"+str(snp_id)+"\t"+ref_base+"\t")
                    out_file.write(",".join(alt_list))
                syn="NA"
                if syn_pos:
                    if syn_pos < len(arr):
                        syn=arr[syn_pos]
                        if syn=='S':
                            syn='Synonymous'
                        elif syn=='NS':
                            syn="Non-Synonymous"
                    else:
                        syn='Intergenic'
                inf="SYN=%s"%syn
                #all non reference the same - non informative
                if mut_number==strain_number:
                    inf=inf+";NIF=True"
                if snp_list_info:
                    info = snp_list_info.get_info_at(arr[0],arr[1])
                    if info:
                        for field in info:
                            inf=inf+";%s=%s"%(field.replace(" ","_"),info[field])


                out_file.write("\t.\tPASS\t%s\tGT"%inf)
                if arr[2]==".":
                    for base in arr[3:strain_end]:
                        if base =='.' or base == '-':
                            out_file.write("\t0")
                        else:
                            out_file.write("\t1")		    

                else:
                    for base in arr[3:strain_end]:
                        if base=='-':
                            base='N'
                        out_file.write("\t"+str(alt[base]))
                snp_id+=1		
                out_file.write("\n")
        out_file.close()
        command = "bgzip -f %s" % vcf_file
        os.system(command)
        vcf_file= vcf_file +".gz"
        command = "tabix -f %s" % vcf_file
        os.system(command)
        self.data['vcf_file']= vcf_file



    def get_files(self):
        """see :func:`StrainBasedAnalys.get_files`"""
        self.files.append(["raxml_tree","raxml.nwk"])
        self.files.append(["matrix","snps.aln.matrix.gz"])
        self.files.append(["mst_layout_file","ms_layout_file"])
        super(SNPProject,self).get_files()


    def get_layout(self,tree_type=None):
        """If tree_type is 'ms' the layout of the ms (grapetree) will be returned
        see :func:`StrainBasedAnalys.get_files`

        :param tree_type: the type of tree whose layout to return. Default is None (phylogentic).
            if 'ms' then the gtapetree layout will be returned"""
        if tree_type=='ms':
            if not self.data.get("mst_layout_file"):
                return ""
            in_handle = open(self.data['mst_layout_file'])
            layout = ujson.loads(in_handle.read())
            in_handle.close()
            return layout
        else:
            return super(SNPProject,self).get_layout()




    def delete(self,send_user_email=False):
        """see :func:`Analysis.delete`"""
        #delete all jbrowse stuff
        ref_id = self.data['ref_id']
        base_folder =get_jbrowse_directory(self.ref_barcode)
        folder = os.path.join(base_folder,"tracks")
        if os.path.exists(folder):
            file_names = os.listdir(folder)
            tag = "_"+str(self.id)+"_snps"
            for name in file_names:
                if tag in name:
                    to_delete= os.path.join(folder,name)
                    if os.path.exists(to_delete):
                        rmtree(to_delete)
            to_delete= os.path.join(base_folder,"snps_"+str(self.id)+"'json")
            if os.path.exists(to_delete):
                os.remove(to_delete)


        super(SNPProject,self).delete(send_user_email)


class MSTree(TreeAnalysis):
    def __init__(self,rec=None,data=None,params=None):
        #create the tree
        self.crobot_data=None
        super(MSTree,self).__init__(rec,data,params)


    @staticmethod
    def create_ms_tree(params, data,algorithm,parameters,workspace_id=0,data_type='default',nwk_file=None): 
        """Creates an MSTree (GrapeTree) either from data about STs (in which case an MSTrees job will 
        have to be sent) or from a nwk file and list of assembly barcodes

        :param params: a dictionary of
            * name
            * user_id
            * database

        :param data: if the tree is to be created by CRobort the the data should be a dictionary of strain id to 
            a list containing st barcode,  st number and assembly id

            .. code-block:: json

                {
                    strain_id:[St_barcode,ST_number,assembly_ID]
        	  ..}

            If data_type is 'assembly_barcodes' and a nwk file is supplied, data should be a string of comma 
            delimited assembly barcodes
        :param algorithm: the algorithm to create the tree 
        :param workspace_id: the id of the parent workspace (or 0 id stand alone)
        :param parameters: a dictionary the paramters of what the user sees
            * strain_number
            * algorithm 
            * scheme
        :param data_type: If 'assembly_barcodes',  a nwk tree (with assembly barcodes as identifiers) 
            should also be supplied
        :param nwk_file: A path to a nwk file (if the tree is to be constructed from an existing nwk file)

        Returns MSTree object
        """
        database=params['database']
        dbase = dbhandle[database]
        original_list=data
        #get data (only if nwk already provided)
        if data_type=='assembly_barcodes':
            not_found=None
            assembly_barcodes=reduce(lambda x,y: x+"','"+y,data)
            assembly_barcodes = "'"+assembly_barcodes+"'"
            sql = "SELECT traces.strain_id AS sid,assemblies.barcode AS bc,assemblies.id AS aid FROM assemblies left outer JOIN trace_assembly ON assemblies.id =trace_assembly.assembly_id INNER JOIN traces ON traces.id= trace_assembly.trace_id WHERE assemblies.barcode IN (%s)" % assembly_barcodes    
          
            results = dbase.execute_query(sql)
            
            data={}
            bc_dict={}
            for item in results:
                bc_dict[ item['bc']]=[item['sid'],item['aid']]
            for bc in original_list:
                item = bc_dict.get(bc.strip())
                if not item:                      
                    if not not_found:
                        not_found="'%s'"%bc                        
                    else:                        
                        not_found=not_found+",'%s'"%bc                        
                    continue                  
                data[str(item[0])]=["NA",0,item[1]]
            if not_found:    
                sql=" select strains.id AS sid,assemblies.barcode AS bc,assemblies.id AS aid from strains inner join assemblies on assemblies.id=strains.best_assembly where assemblies.barcode in (%s)"%not_found
                results = dbase.execute_query(sql)
                bc_dict_={}
                for item in results:
                    bc_dict_[ item['bc']]=[item['sid'],item['aid']]                
                print results
                for bc in original_list:
                    item = bc_dict_.get(bc.strip())
                    if not item:                                                                   
                        continue                  
                    data[str(item[0])]=["NA",0,item[1]]                
            print "data length:", len(data)
         
        tree_type= "ms_tree"
        if workspace_id==0:
            tree_type="ms_sa_tree" 
        params['type']=tree_type
        ms_data={'parameters':parameters,"algorithm":algorithm}
        #need to create the ms tree to get the id which is needed for the directory
        ms = MSTree(data=ms_data,params=params)
        folder = ms.get_folder()
        data_file =os.path.join(folder,"data.json")
        ms.data['data_file']=data_file

        #update parent workspace if needed
        if workspace_id<>0:
            workspace = db.session.query(UserPreferences).filter_by(id=workspace_id).first()
            ms.data["parent"]=workspace.id
            ws_data = ujson.loads(workspace.data)
            ms_trees= ws_data['data'].get("ms_trees")
            if not ms_trees:
                ms_trees=[]
                ws_data['data']['ms_trees']=ms_trees

            ms_trees.append(ms.name+":"+str(ms.id))
            workspace.data = ujson.dumps(ws_data)
            db.session.commit()
        #update the database
        ms.update()

        #the data sent to the pipeline an array  of st_barcode,assembly_barcode
        to_send=[]
        db_code = app.config['DB_CODES'][database][1]
        strain_ids = map(str,data.keys())
        strain_ids = ",".join(strain_ids)
        for strain_id in data:
            item = data[strain_id]
            ass_barcode = db_code+"_"+get_assembly_barcode(item[2])+"_AS"
            item.append(ass_barcode)
            to_send.append([ass_barcode,item[0]])
        #get the metadata
        to_write=[];
        strain_data,idsg=process_strain_query(database,"strains.id IN (%s)"%strain_ids,"query",-1,show_substrains=True)
        for strain in strain_data:
            sid =  strain['id'] 
            item = data.get(str(sid))
            #is it a sub master/strain that is not needed
            if not item:
                continue
            strain['ID']= item[3]
            strain['StrainID']=sid
            del strain['id']
            if data_type=='default':
                strain['ST']= "ST"+str(item[1])
            to_write.append(strain)

        fields = dbase.get_strain_fields()
        metadata_options={}
        for key in fields:
            metadata_options[key]={"label":fields[key],"category":"Metadata"}
        if data_type=='default':
            metadata_options["ST"]={"label":"ST","category":"Metadata"}
        #write to file
        data_handle= open(data_file,"w")
        data_handle.write(ujson.dumps({"isolates":to_write,"metadata_options":metadata_options}))
        data_handle.close()
        if (nwk_file):
            copy_nwk = os.path.join(folder,"tree.nwk")
            copyfile(nwk_file,copy_nwk)
            ms.data['nwk']=copy_nwk
            ms.data['complete']='true'
            ms.update()
            return ms.id

        ms.crobot_data=to_send

        return ms

    def get_files(self):
        """see :func:`StrainBasedAnalysis.get_files`""" 
        self.files.append(["nwk","tree.nwk"])
        self.files.append(["profiile","profile.list"])
        super(MSTree,self).get_files()

    def remove_from_parent(self):
        parent = get_analysis_object(self.data['parent'])
        parent.data['data']['ms_trees'].remove(self.name+":"+str(self.id))
        self.type='ms_sa_tree'
        del self.data['parent']
        self.update()
        parent.update()

    def get_assembly_ids(self):
        """see :func:`StrainBasedAnalysis.get_assembly_ids`""" 
        if not self.data.get('data_file'):
            return None
        data_handle =open(self.data['data_file'])
        data= ujson.loads(data_handle.read())
        metadata= data['isolates']
        return_list=[]
        for strain in metadata:
            return_list.append(decode_barcode(strain['ID']))
        return return_list

    def send_job(self,priority=-9,workgroup="user_upload"):
        """Will  try and  send the 'MSTrees' job to CRobort
        Returns False if the job cannot be sent
        """
        from entero.jobs.jobs import MSTreeJob
        #get the nserv input if not already calculated
        if not self.crobot_data:
            data = self.get_metadata()
            dbase = dbhandle[self.database]
            #get the scheme id 
            Schemes = dbase.models.Schemes
            scheme = dbase.session.query(Schemes.id).filter_by(name=self.data['parameters']['scheme']).one()
            bar_list=[]
            for ass_bar in data:
                bar_list.append(ass_bar)
            bars  ="('"+"','".join(bar_list)+"')"
            sql = "SELECT assemblies.barcode AS aid ,st_barcode  AS stb FROM assemblies INNER JOIN assembly_lookup ON  assemblies.barcode IN %s  AND assemblies.id = assembly_id AND scheme_id=%i" % (bars,scheme.id)
            results = dbase.execute_query(sql)
            self.crobot_data= []
            for res in results:
                self.crobot_data.append([res['aid'],res['stb']])
        tag = self.name+":"+str(self.id)
        params={"task":self.data['algorithm'],"data":self.crobot_data}
        job = MSTreeJob(params=params,database=self.database,
                                   tag=tag,user_id=self.user_id,
                                   priority=priority,workgroup=workgroup)
        success = job.send_job()
        if not success:
            self.data['failed']="true"
        else:
            self.data['job_id']=job.job_id
        self.update()
        return success


    def get_strain_number(self):
        """see :func:`StrainBasedAnalysis.get_strain_number`""" 
        try:
            return int(self.data['parameters']['strain_number'])
        except:
            return len(self.get_metadata())

    def get_nwk_tree(self):
        """see :func:`StrainBasedAnalysis.get_nwk_tree`""" 
        if not self.data.get("nwk"):
            return None
        nwk_file = open(self.data['nwk'],"r")
        nwk = nwk_file.read()
        nwk_file.close()

        return nwk

    def make_distance_matrix(self):
        '''Creates a difference matrix based on the profiles and stores it in a file,
        which is pointed to by data['difference_matrix']
        '''
        data=[]
        matrix ={}
        length=0
        with open(self.data["profile"]) as f:
            for line in f:
                if line.startswith("#"):
                    continue
                arr = line.strip().split("\t")
                length = len(arr)-2
                data.append(arr)
                matrix[arr[0]]={}
        total = len(data)
     
        for start in range(0,total-1):
            key1 = data[start][0]
            alleles1=data[start][2:]
            for index in range(start+1,total):
                key2=data[index][0]
                alleles2=data[index][2:]
                differences=0
                for a in range(0,length):
                    if int(alleles1[a])<1 or int(alleles2[a])<1:
                        continue
                    if alleles1[a] !=  alleles2[a]:
                        differences+=1
                matrix[key1][key2]=differences
                matrix[key2][key1]=differences
        metadata = self.get_metadata()
        name_map = {}
        key_list = matrix.keys()
        name_list=[]
        for key in key_list:
            name_list.append(metadata[key]['strain'])
        folder = self.get_folder()
        out_name = os.path.join(folder,"diff_matrix.txt")
        out = open(out_name,"w")
        out.write("\t"+"\t".join(name_list)+"\n")
        for key in key_list:
            out.write(metadata[key]['strain'])
            for key2 in key_list:
                if key2==key:
                    out.write("\t0")
                else:
                    out.write("\t"+str(matrix[key][key2]))
            out.write("\n")
        out.close()
        self.data['difference_matrix']=out_name
        self.update()




class LociBasedAnalysis(Analysis):
    """The base class for all Loci based analysis"""
    def __init__(self,rec=None,data=None,params=None):


        super(LociBasedAnalysis,self).__init__(rec,data,params)		

    def get_files(self):
        """see :func:`StrainBasedAnalysis.get_files`"""
        pass


class SNPList(LociBasedAnalysis):
    """A list contaning a predefined set of SNPs. At is simplest it is just two columns. 'contig'
    and 'position', but can contain any number of fields."""


    def __init__(self,rec=None,data=None,params=None):
        self.positions= None
        if params:
            params['type']='snp_list'	

        super(LociBasedAnalysis,self).__init__(rec,data,params)
        self._read_file()

    def get_files(self):
        self.files.append(["snp_list","snp_list.txt"])
        self.files.append(["snp_list_simple","snp_list_simple.txt"])
        super(SNPList,self).get_files()

    def get_info_at(self,contig,position):
        """Gets all information about the specified snp

        :param contig: The name of the contig
        :param position: The position of the snp on the specified contig

        Returns a dictionary of field:value keys for the specified SNP
        """
        contig= self.positions.get(contig)
        if not contig:
            return None
        info = contig.get(position)
        return info




    def _read_file(self):
        self.positions ={}
        if not self.data.get("snp_list"):
            return
        reader = csv.DictReader(open(self.data['snp_list']), delimiter='\t')
        self.fields=[]
        for field in reader.fieldnames:
            if field <> 'contig' and field <> 'position':
                self.fields.append(field)
        for row in reader:
            contig = row.get('contig')
            if not self.positions.get(contig):
                self.positions[contig]={}
            info = {}
            for field in self.fields:
                val = row.get(field)
                if val:
                    info[field]=row.get(field)
            self.positions[contig][row.get("position")]=info



    @staticmethod
    def createList(params,data,headers):
        """Creates a predefined snp list from scratch

        :param params: A dictionary containing

        * name
        * user_id
        * database

        :param data: A list of key:value pairs (the minimum being contig and position) e.g

            .. code-block:: json

                [
        	   {
        	       "contig":"node_1",
        	       "position":444,
        	       "Ancestral Base":"A"+
        	    },
        	    ...........
        	]

        :param headers: A dictionary of header position to name e.g

            .. code-block:: json

               {
                   0:"contig",
        	   1:"position",
        	   2:"Ancestral Base"      
               }

        Returns the snp list object
        """	
        snp_list = SNPList(params=params)
        folder = snp_list.get_folder()
        file_name=os.path.join(folder,"snp_list.txt")
        file_name2=os.path.join(folder,"snp_list_simple.txt")
        out_file= open(file_name,"w")
        out_file2=open(file_name2,"w")
        sort_list = sorted (headers.keys())
        sorted_headers=[]
        for i in sort_list:
            sorted_headers.append(headers[i])
        out_file.write("\t".join(sorted_headers)+"\n")
        for line in data:
            out_file2.write(line['contig']+"\t"+line['position']+"\n")
            sorted_data=[]
            for i in sort_list:
                sorted_data.append(line[headers[i]])
            out_file.write("\t".join(sorted_data)+"\n")

        out_file.close()
        out_file2.close()
        snp_list.data['snp_list']=file_name
        snp_list.data['snp_list_simple']=file_name2
        snp_list.update()
        return snp_list



class LocusSearch(LociBasedAnalysis):

    def __init__(self,rec=None,data=None,params=None):

        super(LocusSearch,self).__init__(rec,data,params)

    @staticmethod
    def create_locus_search(params,scheme,fasta):
        """Creates a locus search object

        :param params: a dictionary containing database,name and user_id. if the name is
            "latest_locus_search" then a new database entry is not created but the old one overwritten
        :param scheme: the scheme for the locus search
        :param fasta: a string representing the fasta file
        :param use_default: If true then the existing last_locus_search entry 
            is used instead of adding a new one
        Returns the locus search object 
        """
        data={'scheme':scheme}
        database=params['database']
        params['type']='locus_search'
        if params['name']=='latest_locus_search':
            #retrieve or create the database entry
            rec=db.session.query(UserPreferences).filter_by(database=params['database'],
                                                                                    user_id=params['user_id'],
                                                                                   type="locus_search",
                                                                                   name="latest_locus_search").first()
            if not rec:
                rec = UserPreferences(database=params['database'],user_id=params['user_id'],type="locus_search",name="latest_locus_search")
                db.session.add(rec)
                db.session.commit()
            #create the locus search object
            ls = LocusSearch(rec=rec)
            ls.data=data
        else:
            ls = LocusSearch(data=data, params=params)
        folder = ls.get_folder()
        #write fasta file
        path = os.path.join(folder,'%s.fasta' % str(ls.id))
        file_handle = open(path,"w")
        lines = fasta.splitlines()
        contig_info={}
        current_contig=""
        for line in lines:   
            if line.startswith(">"):
                line=line.replace(" ","_").replace(",","_")
                contig_name =line[1:]
                contig_info[contig_name]=0
                current_contig=contig_name
            else:
                contig_info[contig_name]+=len(line)
            file_handle.write(line+"\n")
        total_len=0     
        for contig_name in contig_info:
            total_len+=contig_info[contig_name]
        file_handle.close()

        #master_scheme
        Schemes = dbhandle[database].models.Schemes
        scheme_info = dbhandle[database].session.query(Schemes).filter_by(description=scheme).one()
        master_scheme=None
        master_scheme = scheme_info.param.get("master_scheme")
        nserv_scheme= scheme_info.param.get("scheme")
        data = {"fasta_file":path,"scheme":scheme,"total_length":total_len,"contig_lengths":contig_info}
        #need to send the masterscheme to Nserv
        if master_scheme:
            data['master_scheme']=master_scheme

        ls.add_data(data)
        # I have added the path to when sent the job
        ls.data['fasta_file']=path

        return ls


    def get_files(self):
        """see :func:`StrainBasedAnalysis.get_files`"""
        self.files.append(["fasta_file","sequence.fasta"])
        self.files.append(["gff_file","annotation.gff"])
        super(LocusSearch,self).get_files()	


    def delete(self,send_user_email=False):
        """see :func:`Analysis.delete`"""
        #delete all the custom files
        allele_info_dict=self.data.get('allele_info')
        if allele_info_dict:
            for ws_id in allele_info_dict:
                file_pointer = allele_info_dict[ws_id]
                if os.path.exists(file_ponter):
                    os.remove(file_pointer)
        super(LocusSearch,self).delete(send_user_email)


    def  get_locus_info(self):
        """Returns a dictionary of information about each locus in the search,containing

        * loci: a list of all loci
        * loci_info:  a dictionary of locus to information
        * status: present, duplicated, fragmented
        * contig: name of contig
        * start: the start position in the contig
        * end: the end position in the contig
        * locus: The name of the locus (same as the key)
        * direction: + or -
        * description: the description of the locus    
        """
        locus_info={};
        locus_list = []
        file_handle = open(self.data['gff_file'],"r")
        lines = file_handle.readlines()
        dbase = dbhandle[self.database]
        Param  = dbase.models.DataParam
        description = {}
        records = dbase.session.query(Param.name,Param.description,Param.label).filter(Param.name.in_(self.data['loci']),Param.tabname ==self.data['scheme']).all()
        for rec in records:
            description[rec.name]=[rec.description,rec.label]

        for line in lines[1:]:
            arr=line.strip().split("\t")
            info = arr[8].split(";")
            locus = info[0].split('=')[1]

            locus_list.append(locus)
            locus_info[locus]={"start":arr[3],
                               "end":arr[4],
                               "direction":arr[6],
                               "status":info[2].split("=")[1],
                               "allele_id":info[3].split("=")[1],
                               "description":description[locus][0],
                               "contig":arr[0],
                               "locus":description[locus][1]
                               }
        file_handle.close()
        return {"loci":locus_list,"loci_info":locus_info}


    def is_complete(self):
        """Returns true if the locus job is complete"""
        return self.data.get('complete')


    def get_contig_info(self):
        """Returns a dictionary with the following:

        * total_distance: total length of all the contigs
        * contig_lengths: a dictionary of contig name to length
        """	
        return {"total_distance":self.data.get("total_length"),"contig_lengths":self.data.get("contig_lengths")}


    def send_job(self,workgroup="user_upload",priority=-9):
        """sends/resends the Locus Search job to Nserv"""
        from entero.jobs.jobs import LocusSearchJob
        scheme_name =  self.data.get('master_scheme')
        if not scheme_name:
            scheme_name= self.data.get("scheme")
        Schemes = dbhandle[self.database].models.Schemes
        scheme= dbhandle[self.database].session.query(Schemes).filter_by(description=scheme_name).one()
        nserv_scheme= scheme.param.get("scheme")
        params={"genome_id":str(self.id),"scheme":nserv_scheme,"reliable":0}
        inputs={"genome_seq":self.data['fasta_file']}
        job = LocusSearchJob(params=params,inputs=inputs,tag=str(self.id),
                                            user_id=self.user_id,database=self.database,
                                            workgroup=workgroup,priority=-9)
        success = job.send_job()
        if not success and not self.data.get("complete"):
            self.data['failed']="true"
        else:
            self.data['job_id']=job.job_id
        self.update()
        


    def get_allele_info(self,ws_id):
        """ Get allele information for all the loci in the locus search for all the strains in the supplied workspace.
        The allele info is cached so  a call to Nserv will only occur, the first time this method is called for 
        a given workspace.

        Returns a dictionary consisting of:

        * contig_info: see :func:`LocusSearch.get_contig_info`
        * loci: a list of all loci
        * loci_info: see :func:`LocusSearch.get_locus_info`
        * nwk: the nwk tree associated with the supplied workspace
        * data: the actual allele data in the format of assembly barcode to a dictionary of 
        locus to allele id (see :meth:`get_allele_info`)
        """
        ret_object={}
        allele_data =None
        allele_info_dict=self.data.get('allele_info')
        if allele_info_dict:
            file_name=allele_info_dict.get(str(ws_id))
            if file_name:
                file_handle = open(file_name)
                allele_data = ujson.loads(file_handle.read())
        ws = get_analysis_object(ws_id)
        if  not allele_data:
            assembly_ids=ws.get_assembly_ids()
            if not self.data.get('scheme') or not self.data.get('loci'):
                app.logger.error("get_allele_info failed fro LocusSearch, scheme %s and loci %s are required"%(self.data.get('scheme'), self.data.get('loci')))
                return ret_object
            allele_data = get_allele_info(self.database,self.data.get('scheme'),assembly_ids,self.data.get('loci'))
            folder = os.path.join(app.config['BASE_WORKSPACE_DIRECTORY'],self.database,"locus_search",str(self.user_id))		
            if not os.path.exists(folder):
                os.makedirs(folder)			
            file_name = os.path.join(folder,str(self.id)+"_"+str(ws_id)+"_alleles.json")
            file_handle = open(file_name,"w")
            file_handle.write(ujson.dumps(allele_data))
            file_handle.close()
            if not allele_info_dict:
                allele_info_dict={}
                self.data['allele_info']=allele_info_dict
            allele_info_dict[ws_id]=file_name
            self.update()
        locus_info=self.get_locus_info()
        ret_object['contig_info']=self.get_contig_info()
        ret_object['loci']=locus_info['loci']
        ret_object['loci_info']=locus_info['loci_info']
        ret_object['nwk']=ws.get_nwk_tree()		
        ret_object['data']=allele_data
        ret_object['metadata']=ws.get_metadata()
        return ret_object


class CustomView(LociBasedAnalysis):
    """Represents a custom view containing experiment and custom columns"""

    def __init__(self,rec=None,data=None,params=None):
        super(CustomView,self).__init__(rec,data,params)


    def get_columns(self,custom_only=False):
        """Get all the columns, in a suitibale format for an Enterobase grid

        :param custom_only: If True then only custom columns will be returned

        Returns a dictionary of with the keys custom_columns and experiment columns,
        which both point to lists of column information. Column names for experiment columns
        are schemename_experimentfield and the column name for custom columns is the custom
        column id. All experiment columns are not editable
        """

        columns= []
        if not custom_only:
            columns=self.data.get('experiment_columns',[])
        for column in columns:
            column['editable']=False
            column['name']=column['name']+"_"+column['scheme']
        custom_columns = self.data.get("custom_columns")
        if custom_columns:
            col_ids=[]
            id_to_order={}
            for item in custom_columns:
                col_ids.append(item["id"])
                id_to_order[int(item['id'])]=item['display_order']
            column_info = db.session.query(UserPreferences).filter(UserPreferences.id.in_(col_ids)).all()
            for column in column_info:
                data = ujson.loads(column.data)
                data['name']=column.id
                data['display_order']=id_to_order[column.id]
                data['description']="custom_column"
                columns.append(data)
        return columns



    @staticmethod
    def get_records(user_id,database):
        """Get all the custom view/records that the user has access to.

        :param user_id: the id of the user
        :param database: the name of the database

        Returns a list of dictionaries containing:-

        * id: the id of the record
        * type: either 'custom_view' or 'custom_column'
        * name: The name of the custom view/column
        """

        '''
        I have divided the query into two queries to optmize the query time
        using one query takes about 6 seconds
        but after divding the query into two, it takes about 00.05 seconds
        K.M. 27/08/2020
        '''
        #sql = "SELECT id,type,name FROM user_preferences WHERE (type IN ('custom_view','custom_column') AND (user_id=%i OR user_id=0) AND database='%s') OR id IN (SELECT value::INTEGER FROM buddy_permission_tags WHERE buddy_id=%i AND database ='%s' AND field IN ('custom_view','custom_column'))" % \
        #      (user_id, database, user_id, database)

        #records= query_system_database(sql)
        #print len(records)
        #return records
        sql_2 = "SELECT value FROM buddy_permission_tags WHERE buddy_id=%i AND species ='%s' AND field IN ('custom_view','custom_column')" % (
        user_id, database)
        records_2 = query_system_database(sql_2)
        ids = [rc['value'] for rc in records_2]
        if len(ids)>0:
            ids_ = "(" + ",".join(ids) + ")"
            sql = "SELECT id,type,name FROM user_preferences WHERE (type IN ('custom_view','custom_column') AND (user_id=%i OR user_id=0) AND database='%s') or  id in %s " % (
        user_id, database, ids_)
        else:
            sql = "SELECT id,type,name FROM user_preferences WHERE (type IN ('custom_view','custom_column') AND (user_id=%i OR user_id=0) AND database='%s')" % (
                user_id, database)


        records = query_system_database(sql)

        return records

    @staticmethod
    def get_shared_view_in_shared_folders(user_id,database):
        '''
        This method is used to get all the views in the folder swhich are sahred with the user'''
        sql = "SELECT buddy_permission_tags.value as ws_id ,buddy_permission_tags.field, buddy_permission_tags.user_id AS ws_owner_id, users.username AS ws_owner_name FROM"+\
            " buddy_permission_tags INNER JOIN users ON buddy_permission_tags.user_id = users.id WHERE buddy_id =%i AND species='%s' AND field='workspace_folders'" % (user_id,database)
        tags = query_system_database(sql)
        records=[]
        for tag in tags:
            shared = json.loads(tag.get('ws_id'))
            shared_folder = shared.get('shared_folder')
            shared_value = shared_folder[1]  # buddy_sub_folders
            shared_id = shared_folder[0]
            budy_shared_foler, work_spaces = get_shared_folders(database, tag.get('ws_owner_id'),
                                                                 shared_folder[0], shared_folder[1],
                                                                 str(tag.get('ws_owner_id')))  
            
            prefs = db.session.query(UserPreferences.name,UserPreferences.id, UserPreferences.type)\
                .filter(UserPreferences.type=='custom_view',UserPreferences.database==database,(UserPreferences.id.in_(work_spaces))).all()
            for per in prefs:                
                records.append({'id':per[1],'type':per[2],'name':per[0]})
                pass
        return records
    

    
    @staticmethod
    def make_subscheme(scheme,database,loci,name,categories=None,user_id=0,description=None):
        """Creates a custom view  with the supplied loci

        :param scheme: the name of the scheme
        :param database: the name of the database
        :param loci: a list of of lists, either containing just the locus or locus and category

            .. code-block:: json

                 [
                     ["locus_a"],
        	         ["locus_b"]
                 ]

            or if categories are to be given

            .. code-block:: json

                 [
                     ["locus_a","Category A"],
        	         ["locus_b","Category B"],
                 ]

        :param name: the name of subscheme
        :param categories: A dictionary of category to the color that should be used
           for the category (only required if categories are specified in the loci parameter)

           .. code-block:: json

               {
                    "Category A":"#81D4FA",
        	        "Category B":"#B39DDB"
               }

        :param user_id: the user id of owner (default is 0 i.e. public)
        :param description: The description of the subscheme (None by default)
        """	
        columns=[]
        order=1
        for locus in loci:
            column = {"scheme":scheme,"name":locus[0],"datatype":"integer","label":locus[1],"display_order":order}
            if categories:
                column["category"]=locus[2]
            columns.append(column)
            order+=1
        data= {"experiment_columns":columns}
        if categories:
            data['categories']=categories
        if description:
            data['data']={"description":description}
        view = UserPreferences(name=name,user_id=user_id,data=ujson.dumps(data),database=database,type='custom_view')
        db.session.add(view)
        db.session.commit()

    @staticmethod
    def update_custom_data(database,data, custom_view_id=None, user_id=-1):
        ''':param database: the name of the database
        :param data: A dictionary of strain ids (as strings) to a dictionary of custom column data e.g.

            .. code-block:: json

                {
        	    "13535":{
        	        "6762":"blue",
        		    "8721":"yes"
        	    },
        	    "13539":{
        	        "6762":"red",
        		    "8721":"no"
        	    },
        	    "13540":{
        	        "6762":""  //delete this value
        	    } 
        	}

            If an empty string is specified for a custom colum,data for that column will be deleted (if there is any)


        Returns a list of all the columns that were actually updated

        '''       
        columns_updated =set()
        if len(data)==0:
            return list(columns_updated)        
        
        if user_id!=-1:              
            ###Add edit permission check before writing the values inside the database           
            ##K.M 13/05/2019
            ## please not t hat the custome view id may id for a tree which contains
            ##the custom column within strain metadata table inside the tree page            
            ##K.M 15/5/2019

            permissions={}
            ## ZZ 20200418 - permisson has to be checked for every column
            #if custom_view_id:
                #if isinstance(custom_view_id, int) or custom_view_id.isdigit():                  
                    #custom_view_id=int(custom_view_id)
                #else:
                    #return list(columns_updated)
                #permission=has_edit_permission_analysis_item(custom_view_id, user_id, database)
                #if not permission:
                    #return list(columns_updated)  
            #else:    
            ##this check has been added when custome_view id is not provided             
            ##it will check each custom field column for edit permission 
            ##K.M 15/05/2019
            ## ZZ 20200418 - duplicated checking removed
            fields = { k for key, value in data.iteritems() for k, v in value.iteritems() }
            for k in fields :
                try: 
                    permission=has_edit_permission_analysis_item(int(k), user_id, database)
                    permissions[k]=permission
                except :
                    continue
            for key, value in data.iteritems() :
                for k in value.items() :
                    if k not in permissions :
                        value.pop(k, None)
            strain_ids=[]
            sql = "SELECT id, strains.custom_columns AS data FROM strains WHERE id IN (%s)" % ",".join(data.keys())
            dbase= dbhandle[database]
            results = dbase.execute_query(sql)
            sql = []
            for item in results:  
                update_dict=data.get(str(item['id']))
                new_dict = item['data']
                if not new_dict:
                    new_dict={}
                dirty  = False  
                for key, new_value in update_dict.iteritems(): 
                    old_value =new_dict.get(key, "")
                    if new_value != old_value:
                        if new_value == "" or new_value is None :
                            new_dict.pop(key, None)
                        else :
                            # check if the custom column value contains single quotation (') and replace it by ''
                            #so postgres accept it. It will check if the value is string or unicode and then check
                            # if the value has '
                            #K.M 08/06/2020
                            #The check has been moved outside this loop as the value which contains "'" can be in the new_dict and has not ben changed
                            #K.M 06/07/2020
                            #if (isinstance(new_value, str) or isinstance(new_value, unicode)) and "'" in new_value:
                            #    new_dict[key]=new_value.replace("'", "''")
                            #else:
                            new_dict[key] = new_value
                        dirty=True
                        columns_updated.add(key)
                # ZZ 20200418 - batch update rather than for every record
                if dirty:
                    #I have moved the check as "'" can be at values which have not been changed
                    #K.M. 06/07/2020
                    for k, val in new_dict.items():
                        if (isinstance(val, str) or isinstance(val, unicode)) and "'" in val:
                            new_dict[k] = val.replace("'", "''")
                    if len(new_dict)==0:
                        sql.append("UPDATE strains SET custom_columns=NULL WHERE id =%i ;"% item['id'])
                    else:
                        sql.append("UPDATE strains SET custom_columns = '%s' WHERE id = %i ;" % (ujson.dumps(new_dict),item['id']))
                    if len(sql) > 300 :
                        dbase.execute_actions(sql)
                        sql = []
            if len(sql) :            
                dbase.execute_actions(sql) #('\n'.join(sql))
        return list(columns_updated)


def get_allele_info(database,scheme_name,assembly_ids,locus_list):
    ''''Gets the allele ids given a list of assembly ids and a list of loci

    :param database: the name of the database
    :param scheme_name: the name (description) of the scheme
    :param assembly_ids: a list of assembly ids
    :param locus_list: a list of loci whose alllels in each assembly will be retieved

    Returns a dictionary containing the assembly barcode as key to a dictionary of  locus:allele id e.g.

    .. code-block:: json

        {
            "SAL_WA9237AA_AS":{
                "STMMWW_23871:27",
                "STMMWW_24721:23",
                ............
            },
            .................
       }
    '''
    dbase = dbhandle[database]
    scheme = dbase.session.query(dbase.models.Schemes).filter_by(description = scheme_name).one()
    assembly_ids_text = map(str,assembly_ids)
    assembly_ids_text= ",".join(assembly_ids_text)
    sql = "SELECT assemblies.barcode as ass_bar, st_barcode as st_bar FROM assembly_lookup INNER JOIN assemblies ON assemblies.id = assembly_lookup.assembly_id AND  scheme_id=%i WHERE  assembly_id IN (%s)" \
        % (scheme.id,assembly_ids_text)
    results = dbase.execute_query(sql)
    st_barcodes= set()
    #st_barcode to list of [assembly barcodes]
    mappings = {}
    ass_barcode_list=[]
    for result in results:
        st_barcode = result['st_bar']
        if not st_barcode:
            continue
        st_barcodes.add(st_barcode)

        ass_barcodes= mappings.get(st_barcode)
        if not ass_barcodes:
            ass_barcodes=[]
            mappings[st_barcode] = ass_barcodes
        ass_barcodes.append(result['ass_bar'])
        ass_barcode_list.append(result['ass_bar'])
    st_barcode_list = list(st_barcodes)
    chunks=[st_barcode_list[x:x+100] for x in xrange(0, len(st_barcode_list), 100)]
    #st barcode to dictionary of locus:allele
    all_dict = {}

    for ass_bar in ass_barcode_list:
        all_dict[ass_bar]={}
    #all loci present
    all_loci=set()
    arr= scheme.param['scheme'].split("_")
    url = "search.api/"+arr[0]+"/"+arr[1]+"/types"
    url = app.config['NSERV_ADDRESS'] +url
    for chunk in chunks:

        resp = requests.post(url=url,data={"barcode":",".join(chunk),"sub_fields":",".join(locus_list)},timeout=app.config['NSERV_TIMEOUT'])       
        data = ujson.loads(resp.text)
        for record in data:
            allele_dict={}
            for allele in record['alleles']:
                locus = allele['locus']
                if locus in locus_list:
                    ass_barcodes= mappings[record['barcode']]
                    for ass_barcode in ass_barcodes:
                        all_dict[ass_barcode][locus]=allele['allele_id']


    return all_dict


def calculate_accessory_genome(database,assembly_ids):
    '''Calculates the core and accessory genome  based on the main MLST scheme in the database
    (usually wgMLST)

    :param database: The name of the database
    :param assembly_ids: A list of all the assembly ids for the analysis

    Returns a dictionary containing 'core' and 'accessory' keys. Both point to lists of two items.The first
     being a list of all loci
    and the the second a dictionary of strain assembly barcode to a list of allele
     numbers (0 being absent). The order of the
    alleles being the same as the order of the loci. For the accessory loci, their order is goverened by
     their co-occurence in
    the strains, with the most frequently occuring being near the beginning.

    ..code-block :: json

    {
    "core":[
    [
    "STMMWW_23871",
    "STMMWW_24721"
    ],
    {
    "SAL_WA9237AA_AS":[7,72],
    "SAL_YA1005AA_AS": [7,17]
    }
         ],
    "accessory":[
    [
    "B619_gp66",
    "ST64Tp37"
    ],
    {
    "SAL_WA9237AA_AS":[1,15],
    "SAL_YA1005AA_AS": [1,0]
    }
    ]
    }

    '''
    dbase = get_database(database)

    sql = "SELECT id FROM schemes WHERE param->>'main_scheme' ='true'"
    results = dbase.execute_query(sql)
    scheme_id = results[0]['id']

    assembly_ids = map(str,assembly_ids)
    assembly_ids= ",".join(assembly_ids)
    sql = "SELECT assemblies.barcode as ass_bar,assembly_lookup.st_barcode as st_bar FROM assemblies INNER JOIN assembly_lookup ON assemblies.id = assembly_lookup.assembly_id AND assembly_lookup.scheme_id=%i WHERE assemblies.id IN (%s)" \
        % (scheme_id,assembly_ids)
    results = dbase.execute_query(sql)
    st_barcodes= set()
    ass_barcode_list=[]
    #st_barcode to list of [assembly barcodes]
    mappings = {}
    for result in results:
        st_barcode = result['st_bar']
        #the next clause is used to test the st_barcode
        #if does not have value it will not be added
        #otherwise it will generate errors as it is explained below
        if not st_barcode:
            continue

        st_barcodes .add(st_barcode)
        ass_barcode_list.append(result['ass_bar'])
        ass_barcodes = mappings.get(st_barcode)
        if not ass_barcodes:
            ass_barcodes=[]
            mappings[st_barcode] = ass_barcodes
        ass_barcodes.append(result['ass_bar'])
    st_barcode_list = list(st_barcodes)
    chunks=[st_barcode_list[x:x+100] for x in xrange(0, len(st_barcode_list), 100)]
    #st barcode to dictionary of locus:allele
    all_dict = {}
    #all loci present
    all_loci=set()
    for chunk in chunks:
        #some times one of the item inside chunk is None
        # so it throws error when using join in the following lines
        #I think we need to remove the none item before using list join
        # or check for none value before adding item to st_barcode_list
        url = app.config['NSERV_ADDRESS']+"/retrieve.api"
        resp = requests.post(url=url,data={"barcode":",".join(chunk)},timeout=app.config['NSERV_TIMEOUT'])       
        data = ujson.loads(resp.text)
        for record in data:
            allele_dict={}
            for allele in record['alleles']:
                allele_dict[allele['locus']]=allele['allele_id']
                all_loci.add(allele['locus'])
            all_dict[record['barcode']]= allele_dict        
    loci_list = list(all_loci)

    strain_number  = len(ass_barcode_list)

    #check which are core
    core_loci_set= set()
    accessory_loci_set=set()
    for locus in loci_list:
        missing = 0
        prev_id=None
        all_same=True
        for barcode in st_barcode_list:
            if all_dict.get(barcode) and all_dict[barcode].get(locus):
                allele_id = all_dict[barcode].get(locus)
                if allele_id <> prev_id and prev_id <> None:
                    if prev_id>0 and allele_id>0:
                        all_same=False
                prev_id = allele_id

            else:#  allele_id == None:
                missing += 1


        if (missing*100)/strain_number>2:
            accessory_loci_set.add(locus)
        if not all_same:
            core_loci_set.add(locus)

    #create two sets of core accessory alleles
    core_loci=[]
    accessory_loci=[]
    core_alleles = {}
    accessory_alleles={}
    for ass_barcode in ass_barcode_list:
        core_alleles[ass_barcode]=[]
        accessory_alleles[ass_barcode]=[]
    for locus in loci_list:
        if locus in core_loci_set:
            temp_loci=core_loci
            temp_alleles=core_alleles
        elif locus in accessory_loci_set:
            temp_loci=accessory_loci
            temp_alleles=accessory_alleles
        else:
            continue
        temp_loci.append(locus)
        for st_barcode in st_barcode_list:
            ass_info = mappings[st_barcode]
            #sometimes all_dict does not contain st_barcode key, so the following
            #line generates error so a check needs to be done before attemping to get the value
            #but we need to consider what will be allele_id is
            #or just continue when do not find it
            allele_id =  all_dict[st_barcode].get(locus)
            if not allele_id or allele_id<0:
                allele_id =0
            for info in ass_info:
                ass_barcode =info
                temp_alleles[ass_barcode].append(allele_id)
    #accessory alleles
    barcodes_to_alleles={}

    for count,locus in enumerate(accessory_loci):
        barcodes_present =[]
        for barcode in ass_barcode_list:
            if accessory_alleles[barcode][count]<>0:
                barcodes_present.append(barcode)
        key = ",".join(barcodes_present)
        if not barcodes_to_alleles.get(key):
            barcodes_to_alleles[key] = [[locus,count]]
        else:
            barcodes_to_alleles[key].append([locus,count])

    key_list =[]
    new_accessory_loci = []
    new_accessory_alleles= {}
    for ass_barcode in ass_barcode_list:	
        new_accessory_alleles[ass_barcode]=[]	

    for key in barcodes_to_alleles:
        key_list.append(key)
    key_list = sorted(key_list,key=len,reverse=True)


    for key in key_list:
        if not key:
            continue

        info_list =  barcodes_to_alleles[key]
        for info in info_list:
            locus = info[0]
            old_position=info[1]
            new_accessory_loci.append(locus)
            for barcode in ass_barcode_list:
                new_accessory_alleles[barcode].append(accessory_alleles[barcode][old_position])




    return {"core":[core_loci,core_alleles],"accessory":[new_accessory_loci,new_accessory_alleles]}

def decode_barcode(barcode,int_val=10000):
    prefix = barcode.split('_')[1][:2]
    suffix = barcode.split('_')[1][6:]
    char_string = (prefix+suffix).rstrip('A')
    int_string = 0
    for idx, cha in enumerate(char_string):
        int_string += 26 ** idx  * ((ord(cha) - 65))
    int_string = int_string * int_val
    return int_string + int(barcode[6:10])


def get_assembly_barcode(input_number):
    number = input_number / 10000
    final_char = ''
    while number > 0:
        final_char += chr(number % 26 + 65)
        number =  (number / 26) 
    final_char= final_char.ljust(4,'A')#
    table_id='_AS'

    return   final_char[:2] + str(input_number % 10000).zfill(4) + final_char[2:] 	


def get_strains_in_shared_workspace(database, user_id):
    '''
    get a list of the strains (ids) which are shared with a user within a database
    @user_id: user id
    @database: database name    
    '''
    has_permission_to=[]
    
    sql = "SELECT buddy_permission_tags.value as ws_id ,buddy_permission_tags.field, buddy_permission_tags.user_id AS ws_owner_id, users.username AS ws_owner_name FROM" + \
        " buddy_permission_tags INNER JOIN users ON buddy_permission_tags.user_id = users.id WHERE buddy_id =%i AND species='%s' AND field in ('workspace_folders','main_workspace') " % (user_id, database)
    
    shared_strains_owners=[]
    tags = query_system_database(sql) 
    shared_workspaces=[]
    for tag in tags:
        shared_strains_owners.append(tag.get('ws_owner_id'))
        if tag.get('field') == 'workspace_folders':
            shared = json.loads(tag.get('ws_id'))
            shared_folder = shared.get('shared_folder')
            budy_shared_foler, work_spaces = get_shared_folders(database, tag.get('ws_owner_id'),
                                                                shared_folder[0], shared_folder[1], str(tag.get('ws_owner_id')))
            shared_workspaces.extend(work_spaces)
        else:
            shared_workspaces.append(tag['ws_id'])            
        for ws_id in shared_workspaces:
            ws=get_analysis_object(ws_id)
            if ws and ws.data.get('data') and ws.data.get('data').get('strain_ids'):
                has_permission_to.extend(ws.data['data']['strain_ids'])            
      
    dbase=get_database(database)
    shared_strains_ids=[]
    #Strains=dbase.models.Strains
    #strains_= dbase.session.query(Strains.id, Strains.owner).filter(Strains.id.in_(has_permission_to)).filter(Strains.owner.in_(shared_strains_owners)).all()
    if len(has_permission_to) >0 and len (shared_strains_owners)>0:
        has_permission_to_= ', '.join(str(x) for x in has_permission_to)
        shared_strains_owners_= ', '.join(str(x) for x in shared_strains_owners)
        sql="select id from strains where id in (%s) and owner in (%s)"%(has_permission_to_,shared_strains_owners_)
        strains=dbase.execute_query(sql)   
        for strain in strains:
            shared_strains_ids.append(strain['id'])            
    return shared_strains_ids


def get_access_permission(database, user_id, strains_ids=None, strains=None, return_all_strains_permissions=False):
    '''
    this method is used to return the permission for a goroup of strains for a user within a database
    @database: database name
    @user_id: user id
    the user needs to provide one the two attributes
    @strains: a list contains the strains
    @strains_ids: a list contians strains ids to check
    @return_all_strains_permissions: a flag to control the return type, 
    if it is false it will return when first false permission occurs
    otherwise, it is true, 
    otherwise it will return a dict of all the strains permissions, key is the strain id 
    the value is 0, 1, 2 or 3: 
    
    3: owner, admin, curatior, buddy with edit permission
    2: buddy without edit permission
    1: can download as strain is public strain, released strain
    0: can not download data as strain is private
    '''
    from dateutil import parser
    try:  
        strains_permissions={}        
        if not strains_ids and not strains:
            if not return_all_strains_permissions:
                return False
            else:
                return strains_permissions
        dbase=get_database(database)   
        if not dbase:
            return False#'ujson.dumps("No valid database name is provided ")   
        if check_permission(user_id, 'edit_strain_metadata', database):
            if not return_all_strains_permissions:
                return True        
            else:
                if strains_ids:
                    for sid in strains_ids:
                        strains_permissions[sid]=3
                elif strains:
                    for strain in strains:
                        strains_permissions[strain.get('id')]=3                        
                return strains_permissions  
        if user_id==-1:
            if not return_all_strains_permissions:
                return False        
            else:
                if strains_ids:
                    for sid in strains_ids:
                        strains_permissions[sid]=0
                elif strains:
                    for strain in strains:
                        strains_permissions[strain.get('id')]=0                       
                return strains_permissions              
            
        if not strains:        
            if len(strains_ids)==0:
                return {}
            #Strains=dbase.models.Strains
            #strains= dbase.session.query(Strains.owner, Strains.created, Strains.release_date, Strains.id).filter(Strains.id.in_(strains_ids)).all()
            sql='select owner, created, release_date, id from strains where id in (%s)'%",".join("'{0}'".format(n) for n in strains_ids)#','.join(strains_ids)
            strains=dbase.execute_query(sql)
            '''
            0:owner
            1:created
            2:release_date
            3:id
            '''
        
        all_owners=[]           
        for strain in strains:              
            if not strain['owner'] in all_owners and strain['owner']!= user_id and strain['owner']!=0:
                all_owners.append(strain['owner'])
        owner_permissions_ = db.session.query(BuddyPermissionTags.user_id).filter(BuddyPermissionTags.user_id.in_(all_owners)).filter(BuddyPermissionTags.buddy_id==user_id).filter(BuddyPermissionTags.field=='edit_strain_metadata').filter(BuddyPermissionTags.value=='True').filter(BuddyPermissionTags.species==database).all()        
        owner_permissions=[]
        for own in owner_permissions_:
            owner_permissions.append(own[0])
        #sql="select user_id from buddy_permission_tags where user_id in (%s) and buddy_id=%s and field='edit_strain_metadata' and value='True'"%(",".join("'{0}'".format(n) for n in all_owners),user_id)
        #owner_permissions=query_system_database(sql)
        shared_strains_ids=get_strains_in_shared_workspace(database, user_id)         
        for strain in strains:   
            if strain ['owner']==user_id:
                strains_permissions[strain['id']]=3
                continue  
            if strain['id'] in shared_strains_ids:
                if strain['owner'] in owner_permissions:
                    strains_permissions[strain['id']]=3               
                else:
                    strains_permissions[strain['id']]=2                    
                continue                            
            if not strain['release_date']:
                if strain ['created']:
                    if isinstance(strain ['created'], basestring):                    
                        created=parser.parse(strain['created'])             
                    else:
                        created=strain['created']                      
                if strain['owner']==0:
                    strains_permissions[strain['id']]=1
                    continue
                else:
                    if datetime.now()>=(created+ timedelta(days=360)):
                        strains_permissions[strain['id']]=1
                        continue
                    else:
                        strains_permissions[strain['id']]=0
                        if not return_all_strains_permissions:
                            return False        
            #check the strain release date, if it is already released so it is public and the user can see it and work with it   
            else:
                if isinstance(strain ['release_date'], basestring):                    
                                    release_date=parser.parse(strain['release_date'])
                                    #created=parser.parse(strain['created'])
                else:
                    release_date=strain['release_date']
                    #created=strain['created']                
                
                if datetime.now()>=release_date:
                    strains_permissions[strain['id']]=1
                    continue
                #otherwise the user can see it only but not download it
                else:
                    strains_permissions[strain['id']]=0
                    if not return_all_strains_permissions:
                        return False              
        if not return_all_strains_permissions:
            return True
        else:
            return strains_permissions
    except Exception as e:
        app.logger.exception("get_access_permission failed, Erro message: %s"%e.message)
        if not return_all_strains_permissions:
            return False
        else:
            return {}

def get_shared_folders(dbname, buddy_id, shared_folder_id, shared_folder_path, owner_id):
    '''
    get  the budy shared folder
    @param dbname: database name
    @param shared_folder_id: shared folder id
    @param shared_folder_path

    '''
    shared_folders = {}
    work_spaces = []
    # get the folder structure for the user
    ws_folders = db.session.query(UserPreferences).filter_by(database=dbname, user_id=buddy_id,
                                                             type="workspace_folders", name='default').first()
    # for new users
    if not ws_folders:
        return
    else:
        workspace_folders = ujson.loads(ws_folders.data)
        if len(workspace_folders) == 0:
            workspace_folders = {"RN": {"text": "Root", "id": "RN", "workspaces": []}}
            # for ws in workspaces:
            #    #put in any workspaces they own
            #    if ws[2]=='mine':
            #        workspace_folders['RN']['workspaces'].append(ws[3])

    _get_shared_folder(shared_folder_id, workspace_folders, shared_folders, work_spaces, owner_id)
    return shared_folders, work_spaces


def _get_shared_folder(shared_folder_id, workspace_folders, shared_folders, work_spaces, owner_id):
    '''
    This methos is called to add the shared folder and its childern to the shared_folders dict
    in addition to add the workspaces inside the shared folders if any
    '''
    if shared_folder_id in workspace_folders.keys():
        shared_folders[shared_folder_id + '_' + owner_id] = workspace_folders.get(shared_folder_id)
        workspace_folders.get(shared_folder_id)['id'] = shared_folder_id + '_' + owner_id        
        if workspace_folders.get(shared_folder_id).get('workspaces'):
            for vv in workspace_folders.get(shared_folder_id).get('workspaces'):
                #Adding a check to the item id and append it inside the list as integer                
                #K.M. 14/05/2019
                u'210'
                if isinstance(vv, int) or vv.isdigit():
                    work_spaces.append(int(vv))     
                else:
                    #send an email to the server in case it is not integer
                    #to check it
                    app.logger.exception("Error in _get_shared_folder, error message: vv %s is not integer, owner id: %s"%(vv, owner_id))
                #work_spaces.append(vv)
        childs = workspace_folders.get(shared_folder_id).get('children')[:]
        if childs:
            for child in childs:
                workspace_folders.get(shared_folder_id).get('children').remove(child)
                workspace_folders.get(shared_folder_id).get('children').append(child + '_' + owner_id)
                _get_shared_folder(child, workspace_folders, shared_folders, work_spaces, owner_id)

def has_edit_permission_analysis_item(query_id, user_id, database): 
    '''
    check if user has edit permission for the the analysis item
    query_id: analysis id
    user_id : current user id
    database: species    
    '''
    #check if the user is an admin or cruator
    permission=check_permission(user_id, "edit_strain_metadata", database)   
    if permission:
        return permission
    #check if the user is the owner of the item
    sql="select user_id from user_preferences where id=%s"%query_id
    res=query_system_database(sql)  
    if len(res)==0:
        return False

    owner_id=res[0]['user_id']
    if owner_id==user_id:
        return True
    else:      
        #check if the it is shared with user
        sql="select * from buddy_permission_tags where buddy_id=%s and value='%s' and user_id=%s"%(user_id, query_id, owner_id)
        res=query_system_database(sql)
        
        if len(res)>0:
            permission=True
    if not permission:
        #if the user is not shared the item directley, th the systme will chekc if it is shared inisde a shared folder
        sql = "SELECT buddy_permission_tags.value as ws_id ,buddy_permission_tags.field, buddy_permission_tags.user_id AS ws_owner_id, users.username AS ws_owner_name FROM" + \
                              " buddy_permission_tags INNER JOIN users ON buddy_permission_tags.user_id = users.id WHERE buddy_id =%i AND species='%s' AND field ='workspace_folders'" % (
                                user_id, database)                    
        tags = query_system_database(sql)
        for tag in tags:
            if tag.get('field') == 'workspace_folders':
                shared = json.loads(tag.get('ws_id'))
                shared_folder = shared.get('shared_folder')
                budy_shared_foler, work_spaces = get_shared_folders(database, tag.get('ws_owner_id'),
                                                                        shared_folder[0], shared_folder[1],
                                                                        str(tag.get('ws_owner_id')))                        
                if query_id in work_spaces:                
                    permission = True
                    break   
    if permission:        
        owner_permission = db.session.query(BuddyPermissionTags.user_id).filter(BuddyPermissionTags.user_id==owner_id).filter(BuddyPermissionTags.buddy_id==user_id).filter(BuddyPermissionTags.field=='edit_strain_metadata').filter(BuddyPermissionTags.value=='True').filter(BuddyPermissionTags.buddy_id==user_id).filter(BuddyPermissionTags.species==database).first()        
        if not owner_permission:
            permission=False   
        
    return permission


analysis_classes={}



if app.config.get('ANALYSIS_TYPES'):
    for analysis_type in app.config['ANALYSIS_TYPES']:
        class_name = app.config['ANALYSIS_TYPES'][analysis_type].get("Class")
        if class_name:
            analysis_classes[analysis_type] = getattr(sys.modules[__name__],class_name )

