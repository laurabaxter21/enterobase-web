tabname	name	sra_field	mlst_field	display_order	nested_order	label	datatype	min_value	max_value	required	default_value	allow_pattern	description	vals	max_length	no_duplicates	allow_multiple_values	group_name
strains	strain	Sample,Metadata,Strain		1	0	Name	text			1			A unique name identifying the isolate -  required					
traces	accession	Run		2	1	Accession No.	text			1						1	1	Accession
traces	seq_platform	Sequencing,Platform		2	2	Sequencing Platform	combo			1	ILLUMINA			ILLUMINA|LS454|ION_TORRENT|PACBIO_SMRT|7GeneMLST|ABI_SOLID			1	Accessioin
traces	seq_library	Sequencing,Library		2	3	Sequencing Library	combo			1	Paired			Paired|Mate Pair|Single|ND			1	Accession
traces	seq_insert	Sequencing,Insert		2	4	Insert Size	integer	1	100000	0	500							Accession
traces	experiment_accession	Experiment,Accession		2	5	Experiment	text			1								Accession
strains	source_niche	Sample,Metadata,Source Niche		3	1	Source Niche	combo			0				Aquatic|Companion Animal|Environment|Feed|Food|Human|Laboratory|Livestock|ND|Poultry|Wild Animal				Source
strains	source_type	Sample,Metadata,Source Type		3	2	Source Type	combo			0				Air|Amphibian|Animal Feed|Avian|Bat|Bovine|Camelid|Canine|Composite Food|Dairy|Deer|Equine|Feline|Fish|Human|Invertebrates|Laboratory|Marine Mammal|Marsupial|Meat|ND|Other Mammal|Ovine|Plant|Primate|Reptile|Rodent|Shellfish|Soil/Dust|Swine|Water				Source
strains	source_details	Sample,Metadata,Source Details		3	3	Source Details	text			0								Source
strains	collection_year	Sample,Metadata,Year		4	1	Collection Year	integer	1800	2100	1								Collection Date
strains	collection_month	Sample,Metadata,Month		4	2	Collection Month	integer	1	12	0								Collection Date
strains	collection_date	Sample,Metadata,Date		4	3	Collection Day	integer	1	31	0								Collection Date
strains	collection_time	Sample,Metadata,Time		4	4	Collection Time	text			0		[012][0-9]:[0-5][0-9]						Collection Date
strains	continent	Sample,Metadata,Continient		5	1	Continent	text			1								Location
strains	country	Sample,Metadata,Country		5	2	Country	custom			1								Location
strains	admin1	Sample,Metadata,admin1		5	3	Region	custom			0								Location
strains	admin2	Sample,Metadata,admin2		5	4	District	custom			0								Location
strains	city	Sample,Metadata,City		5	5	City	custom			0								Location
strains	postcode	Sample,Metadata,Postcode		5	6	Post Code	text			0								Location
strains	latitude	Sample,Metadata,latitude		5	7	Latitude	double	-90	90	0								Location
strains	longitude	Sample,Metadata,longitude		5	8	Longitude	double	-180	180	0								Location
strains	contact	Sample,Source,Center		6	0	Lab Contact	suggest			1								Location
strains	antibiotic_resistance			12	0	Antibiotic Resistance	text			0								
strains	comment			13	0	Comment	text			0			An optional brief description of the strain					
strains	study_accession	Project,Bioproject ID		14	1	Bio Project ID	text			1								Project
strains	secondary_study_accession	Project,Accession		14	2	Project	text			1								Project
strains	sample_accession	Sample,Main Accession		15	1	Sample	text			1								Sample
strains	secondary_sample_accession	Sample,SRA Accession		15	2	Secondary Sample	text			1								Sample
strains	created			16	0	Date Entered	text			0
strains	release_date	Creation Time		17	0	Release Date	text			0
strains	barcode			18	0	Barcode	text			0
strains	uberstrain			-1000	0	Uberstrain	integer										
assembly_stats	status			1	0	Status	text
assembly_stats	pipeline_version			2	0	Version	double
assembly_stats	n50			3	0	N50	integer
assembly_stats	total_length			4	0	Length	integer
assembly_stats	top_species			5	0	Species	text
assembly_stats	contig_number			6	0	Contig Number	integer
assembly_stats	low_qualities			7	0	No. N's	integer
assembly_stats 	barcode			8	0	Barcode	text