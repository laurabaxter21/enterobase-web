from sqlalchemy import Integer, Column, ForeignKey, Table, String, DateTime, Time, Text, MetaData
from sqlalchemy.ext.declarative import declarative_base
from entero.databases import generic_models as mod

metadata = MetaData()
Base = declarative_base(metadata=metadata)

### Lookup tables ###
# N.B Table order matters! defined relationships() in raw data tables
# need lookup, index, and dependent child tables to exist first.
#STAssembly = mod.getSTAssemblyTable(metadata)
#STAllele = mod.getSTAlleleTable(metadata)
#SchemeLoci= mod.getSchemeLociTable(metadata)
#AlleleAssembly = mod.getAlleleAssemblyTable(metadata)
TracesAssembly = mod.getTraceAssemblyTable(metadata)

### Raw data tables ###
class Assemblies(Base, mod.AssembliesRel):
    pass

class AssembliesArchive(Base, mod.AssembliesArchive):
    pass

class Traces(Base, mod.Traces):
    pass

class TracesArchive(Base, mod.TracesArchive):
    pass

class Schemes(Base,mod.Schemes):
    pass

class SchemesArchive(Base,mod.SchemesArchive):
    pass

class Strains(Base,mod.Strains): 
    species = Column("species",String(200))
       
class StrainsArchive(Base,mod.StrainsArchive):
    species = Column("species",String(200))
    
class DataParam(Base,mod.DataParam): 
    pass

class AssemblyLookup(Base,mod.AssemblyLookup):
    pass




