from sqlalchemy import Integer, Column, ForeignKey, Table, String, DateTime, Time, Text, MetaData
from sqlalchemy.ext.declarative import declarative_base
from entero.databases import generic_models as mod

metadata = MetaData()
Base = declarative_base(metadata=metadata)

### Lookup tables ###
# N.B Table order matters! defined relationships() in raw data tables
# need lookup, index, and dependent child tables to exist first.
#STAssembly = mod.getSTAssemblyTable(metadata)
#STAllele = mod.getSTAlleleTable(metadata)
#SchemeLoci= mod.getSchemeLociTable(metadata)
#AlleleAssembly = mod.getAlleleAssemblyTable(metadata)
TracesAssembly = mod.getTraceAssemblyTable(metadata)

### Raw data tables ###
class Assemblies(Base, mod.AssembliesRel):
    pass

class AssembliesArchive(Base, mod.AssembliesArchive):
    pass

class Traces(Base, mod.Traces):
    pass

class TracesArchive(Base, mod.TracesArchive):
    pass

class Schemes(Base,mod.Schemes):
    pass

class SchemesArchive(Base,mod.SchemesArchive):
    pass

class Strains(Base,mod.Strains): 
    date_excavated=  Column("date_excavated",DateTime)
    date_collected = Column("date_collected",DateTime)
    date_received= Column("date_received",DateTime)
    date_returned = Column("date_returned",DateTime)
    material_type = Column("material_type",String(200))
    material_detail = Column("material_detail",String(500))
    material_condition =Column("material_condition",String(500))
    source_species =Column("material_species",String(200))
    source_sex = Column("source_sex",String(200))
    source_age = Column("source_age",String(200))
    archeological_age = Column("archeological_age",String(200))
    geological_age = Column("geological_age",String(200))
    age_details = Column("age_details",String(500))
    item_tracker_id = Column("item_tracker_id",Integer)
    original_id = Column("original_id",String(200))
    project = Column("project",String(200))
    project_details= Column("project_details",String(500))
  

       
class StrainsArchive(Base,mod.StrainsArchive):
    date_excavated=  Column("date_excavated",DateTime)
    date_collected = Column("date_collected",DateTime)
    date_received= Column("date_received",DateTime)
    date_returned = Column("date_returned",DateTime)   
    material_type = Column("material_type",String(200))
    material_detail = Column("material_detail",String(500))
    material_condition =Column("material_condition",String(500))
    source_species =Column("material_species",String(200))
    source_sex = Column("source_sex",String(200))
    source_age = Column("source_age",String(200))
    archeological_age = Column("archeological_age",String(200))
    geological_age = Column("geological_age",String(200))
    age_details = Column("age_details",String(500))
    item_tracker_id = Column("item_tracker_id",Integer)
    original_id = Column("original_id",String(200))
    project = Column("project",String(200))
    project_details= Column("project_details",String(500))    
    
    
    
class DataParam(Base,mod.DataParam): 
    pass

class AssemblyLookup(Base,mod.AssemblyLookup):
    pass




