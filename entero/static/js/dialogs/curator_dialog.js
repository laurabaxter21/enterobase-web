EnteroCuratorDialog.prototype = Object.create(null);
EnteroCuratorDialog.prototype.constructor= EnteroCuratorDialog;


/**
* Creates a dialog from which the user can carry out the following curator tasks on strains
* <ul> <li>delete</li>
* <li>change ownership</li>
* <li>change assembly status</li>
* <li>change release date</li></ul>
* @constructor
* @param {string} database - The name of the database
* @param {SRAValidationFrid } strain_grid -A pointer to the Strain table
*/
function EnteroCuratorDialog(database,strain_grid){
        this.database = database;
        this.dialog = null;
        this.strain_grid=strain_grid;
        this.experiment_grid=null;
}


/**
* Shows the dialog -if it is the first time then data e.g user_permissions will be loaded in 
* @param {BaseExperimentGrid} experiment_grid - A pointer to the Experiment table (needed to update assembly status)
*/
EnteroCuratorDialog.prototype.show=function(experiment_grid){
        this.experiment_grid=experiment_grid;
        if (!this.dialog){
                this._init();
        
        }
        else{
                this.dialog.dialog("open");
        }

}

EnteroCuratorDialog.prototype.showError=function(msg){
       Enterobase.modalAlert(msg);

}

/**
* Changes the assembly status of the selected strains
*/
EnteroCuratorDialog.prototype.changeAssemblyStatus=function(){
       var self = this;
       var status = $("#curator-status-select").val()
        var ids = self.strain_grid.getSelectedRows();
        var to_send={
                ids:ids.join(","),
                status:status,
                database:self.database
        }
       Enterobase.call_restful_json("/species/"+ self.database+"/assembly_status","POST",to_send,self.showError).done(function(data){
                if (data['status']==='OK'){
                        if (self.experiment_grid.constructor.name === 'AssemblyGrid'){
                                var col_index = self.experiment_grid.getColumnIndex("status");
                                for (var i in ids){
                                        var row_index =  self.experiment_grid.getRowIndex(ids[i]);
                                        self.experiment_grid.setValueAt(row_index,col_index,status)
                                        
                                }
                                self.experiment_grid.refreshGrid();
                        }
                        Enterobase.modalAlert(ids.length+"strain(s) assembly status have been updated");
                }
                else{
                        Enterobase.modalAlert("There was a problem changing assembly status");
                }
        });
}

/**
* Updates the release date of the selected strains
*/
EnteroCuratorDialog.prototype.updateReleaseDate=function(){
        var self = this;
        var release = $("#release-public-check").prop('checked');
        var increase = $("#increase-release-spin").val();
        var colIndex = self.strain_grid.getColumnIndex("release_date");
        
        if (release || increase !== '0'){
                var ids = [];
                var dates =[]
                if (release){   
                        var yesterday = Enterobase.getDateAsString(new Date,-1)
                        for (var id in self.strain_grid.selectedRows){
                                ids.push(id);
                                dates.push(yesterday);
                        }
                }
                else{
                        for (var id in self.strain_grid.selectedRows){
                              var rowIndex = self.strain_grid.getRowIndex(id);
                              var rel_date = self.strain_grid.getValueAt(rowIndex,colIndex);
                              rel_date = Enterobase.getDateAsString(new Date(rel_date),30*increase);
                              ids.push(id);
                              dates.push(rel_date);
                        }

                }
               var  to_send={
                        dates:dates.join(","),
                        ids:ids.join(",")
                };
                Enterobase.call_restful_json("/auth/update_release_dates/"+self.database,"POST",to_send,self.showError).done(function(data){
                        if (data['status']=== 'OK'){
                                Enterobase.modalAlert('Updated Release Dates!',"Information");
                                for (var index in ids){
                                        var rowIndex= self.strain_grid.getRowIndex(ids[index]);
                                        self.strain_grid.setValueAt(rowIndex,colIndex,dates[index], false);
                                }
                                self.strain_grid.refreshGrid();
                                
                       }
                       else{
                                 Enterobase.modalAlert('Error Updating Release Dates!',"Warning");
                       }
                       
               });       
        }
}

/**
* Deletes the selected strains
*/
EnteroCuratorDialog.prototype.deleteStrains= function(){
        var self = this;
        var ids = this.strain_grid.getSelectedRows();
        var col_index = self.strain_grid.getColumnIndex("strain");
        var names = [];
        for (var i in ids){
                var row_index = self.strain_grid.getRowIndex(ids[i]);
                var name = self.strain_grid.getValueAt(row_index,col_index);
                names.push("<li>"+name+"</li>");
        }
        msg = "Are you sure you want to delete the following strains <ul>"
        msg+= names.join("")+"</ul>";
        to_send= {
                ids:ids.join(",")
        };
        Enterobase.oKorCancelDialog("Delete Strains",msg,function(okOrcancel){
        
                //if click on 'OK'
                if(okOrcancel){
                        Enterobase.call_restful_json("/auth/delete_strains/"+self.database,"POST",to_send,self.showError).done(function(data){
                                if (data['status']==='OK'){
                                        for (var i in ids){
                                                var id = ids[i];
                                                self.strain_grid.removeRow(id);
                                                self.experiment_grid.removeRow(id);
                                        }
                                        self.strain_grid.selectedRows={};
                                        self.strain_grid.refreshGrid();
                                        self.experiment_grid.refreshGrid();
                                        Enterobase.modalAlert("The strains have been deleted");         
                                }
                                else{
                                        Enterobase.modalAlert(data['msg']);
                                }
                        
                        });                
                
                }else{
                        //click on 'Cancel'
                        //display nothing
                        //Enterobase.modalAlert('you have cancled it');
                }
      
        });
}

/**
* Changes ownership of the selected strains
*/
EnteroCuratorDialog.prototype.changeStrainOwner= function(){
        var self = this;
        var user_id  = $("#new-strain-owner").data("user_id");
        if (! user_id){
                return;
        }
        var ids = this.strain_grid.getSelectedRows();
        var col_index = self.strain_grid.getColumnIndex("strain");
        var to_send = {
                               user_id:user_id,
                               ids: ids.join(','),
        };        
                                Enterobase.call_restful_json("/species/"+ self.database+"/change_owner","POST",to_send,self.showError).done(function(data){
                                       if (data['status']==='OK'){
                                                Enterobase.modalAlert("The user for the selected strains has been changed");
                                       }
                                       else{
                                                Enterobase.modalAlert("The was an error trying to change user");
                                       }
                               });                        
        
       
     
}


EnteroCuratorDialog.prototype._init= function(){
        var self = this;
        var uri = '/auth/get_user_permissions?database='+this.database
        Enterobase.call_restful_json(uri,"GET","",this.showError).done(function(permissions){
        
                self.dialog=$("<div>");
                self.dialog.append($("<div style='font-size:16px'> <b>Perform these tasks on selected rows </b></div>")).append("<hr>");
                
                //deleting strains
                if (permissions['delete_strains']){
                        var delete_strain_div = $("<div>").attr("id","delete-strain-div").css({"margin-top":"7px","margin-bottom":"7px"});
                      
                        delete_strain_div.append($("<label> Delete Strains</div>"));
                        var but =$("<button>").css("margin-left","4px").text("Delete").click(function(e){
                                self.deleteStrains();
                        })
                        delete_strain_div.append(but);
                        self.dialog.append(delete_strain_div).append("<hr>");
                }
                if (permissions['change_assembly_release_date']){
                        var release_date_div  =  $('<div>').attr("id","release-date-div").css({"margin-top":"7px","margin-bottom:":"7px"});
                        release_date_div.append($('<label>Update Release Date</label><br>'));
                        release_date_div.append($('<span>Extend by</span>'));
                        var spinner  = $("<input>");
                                spinner.css({
                                width:'25px',
                                height:'15px',
                        })
                        .attr({
                                value:0,
                                id:'increase-release-spin'
                        });
      
                        release_date_div.append(spinner);
                        release_date_div.append($("<span>(months)</span>")).append("<br>");
       
                        spinner.spinner({
                                min:0,
                                max:12
                         });
                         release_date_div.append($("<span>Make Public Now<span>"));
                        var chkbox = $("<input>").attr({
                                 type:'checkbox',
                                id:"release-public-check"
                        }).css("left-margin","3px");
                        release_date_div.append(chkbox);
                        self.dialog.append(release_date_div).append("<hr>");
                        
                        var but= $("<button>").css("margin-left","4px").text("Update").click(function(e){
                                self.updateReleaseDate();
                        });
                        release_date_div.append(but)
                         
                }
                
                if (permissions['change_strain_owner']){
                        var change_owner_div = $("<div>").attr("id","change-owner-div").css({"margin-top":"7px","margin-bottom":"7px"});
                        change_owner_div.append($("<div><label> Change Strain Owner To:</label> </div>"));        
                      
                        change_owner_div.append($('<input  id ="new-strain-owner">'));
                        self.dialog.append(change_owner_div).append("<hr>");
                    
                         var but = $("<button>").css("margin-left","4px").text("Change").click(function(e){
                                self.changeStrainOwner();
                         });
                         change_owner_div.append(but);
                }
                if (permissions['change_assembly_status']){
                        var change_status_div = $("<div>").css({"margin-top":"7px","margin-bottom":"7px"});
                        change_status_div.append($("<div> <label> Change Assembly Status To:</label></div>"));
                        change_status_div.append($('<select class = "table-input" id ="curator-status-select"><option value ="Poor Quality">Poor Quality</option><option value ="Assembled">Assembled</option><option value ="Contaminated">Contaminated</option><option value ="Failed QC">Failed QC</option><label>Change assembly status</label><br>'));
                        var but = $("<button>").css("margin-left","4px").text("Change").click(function(e){
                                self.changeAssemblyStatus($("#curator-status-select").val())
                        });
                        change_status_div.append(but);
                         self.dialog.append(change_status_div);
                        
                
                }
 
                /*
                         self.dialog.dialog({
                               autoOpen: true,
                               modal: false,
                               title: "Curator tools ",
                               width: 'auto',
                               height:'auto',
                               buttons: [{
                                       text:"Close",
                                       click: function () {
                                               $(this).dialog("close");
                                       }
                               }]
                       });
                */        
                    
                if( isAdministor == "None" || isAdministor == "none"){
                        self.dialog.dialog({
                                autoOpen: false,
                                modal: false,
                                title: "Curator tools ",
                                width: 'auto',
                                height:'auto',
                                buttons: [{
                                        text:"Close",
                                        click: function () {
                                                $(this).dialog("close");
                                        }
                                }]
                        });                
                }else{
                
                        self.dialog.dialog({
                               autoOpen: true,
                               modal: false,
                               title: "Curator tools ",
                               width: 'auto',
                               height:'auto',
                               buttons: [{
                                       text:"Close",
                                       click: function () {
                                               $(this).dialog("close");
                                       }
                               }]
                       });                
                
                }
                
                
                if (permissions['change_strain_owner']){
                         new Enterobase.UserNameSuggest("new-strain-owner",function(value,label){
                                $("#new-strain-owner").data("user_id",value);
                            
                         });
                }
        });     
};