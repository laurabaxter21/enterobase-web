AlleleViewDialog.prototype = Object.create(null);
AlleleViewDialog.prototype.constructor= AlleleViewDialog;


/**
* Creates a dialog showing information about an allele. the dialog will automatically open
* @constructor
* @param {lstring} schemes The name of the scheme
* @param {string} database The name of the database
* @param {string}-locus The name od the locus
* @param {integer} id The id of the allele
* @param {ass_barcode} The assembly barcode of the strain associated with this allele (allows viewing of the allele in the genome)
* @param {object} other_data -  should be 
* <ul><li>strain_name</li>
* <li>scheme_label</li>
* <li>locus_label</li>
* <li>locus_description</li></ul>
*/
function AlleleViewDialog(scheme,database,locus,id,ass_barcode,other_data){
        this.id=locus+id+ass_barcode;
        this.scheme=scheme;
        this.database=database;
        this.locus=locus;
        this.allele_id=id;
        this.ass_barcode=ass_barcode;
        if (! other_data){
                other_data={};
        }
        this.strain_name = other_data['strain_name'];
        this.scheme_label = other_data['scheme_label'];
        this.locus_label = other_data['locus_label'];
        this.locus_description=other_data['locus_description'];
        
        this._init();
}


/** Opens up a window showing the allele (coloured red) in the parent genome
*/
AlleleViewDialog.prototype.showInContext= function(){
        var args="?database="+this.database+"&locus="+this.locus+"&assembly_id="+this.ass_barcode+"&highlight_genes="+this.locus;
        var win = window.open("/view_jbrowse_annotation"+args,'jbrowse_window','width=1000, height=600');
}

AlleleViewDialog.prototype._init =  function (){
        var self = this;
        this.div  = $("<div>");
       
        $("body").append(this.div);
        
        this.div.dialog({     
                title: this.locus+":"+this.allele_id, 
                buttons:[{
                        id: this.id+'-ok-button',
                        label:"OK",
                        click: function() {
                                self.div.dialog("close");
                        }
                }  
                ],
                close:function(){
                       $(this).dialog('destroy').remove();        
                },
                autoOpen:true,
                width: 450,
                height:500 
        });
        if (this.strain_name){
                 this.div.append($("<label>Strain Name:</label>"));
                  this.div.append($("<span>"+this.strain_name+"</span><br>"));
                
        }
        this.div.append($("<label>Scheme:</label>"))
        
         this.div.append($("<span>").attr("id",this.id+"-scheme_label").text(this.scheme_label));
         this.div.append($("<br><label>Locus:</label>"));
         this.div.append($("<span>").attr("id",this.id+"-locus_label").text(this.locus_label));
         this.div.append(("<br><label>Description:</label>"));
         this.div.append($("<span>").attr("id",this.id+"-locus_description").text(this.locus_description));
         this.div.append($("<br><label>Allele ID:</label>"));
         this.div.append($("<span>").text(this.allele_id));
         var button = $("<button>").html("View In Context").click(function(e){
                self.showInContext();
         
         });
         this.div.append($("<br>")).append(button);
         this.div.append($("<br><label>Sequence:</label>"));
         this.div.append($("<img>").attr({"src":"/static/img/upload/queue.gif","id":"sequence-waiting-icon"}));
         $("#"+this.id+"-ok-button").button('option', 'label', "OK");
         var self = this;
         
        var to_send = {
                scheme:this.scheme,
                database:this.database,
                locus:this.locus,
                allele_id:this.allele_id
        
        }
        Enterobase.call_restful_json("/get_allele_view_info","GET",to_send,this.showErrorMessage).done(function(data){
               var format_seq="";
                for (var n=0;n<data['sequence'].length;n+=60){
                        var end = n+60;
                        if (end>data['sequence'].length){
                        end = data['sequence'].length;   
                        }
                        format_seq+=data['sequence'].substring(n,end)+"\n"
                }
                $("#sequence-waiting-icon").remove();
                self.div.append($("<pre>").text(format_seq));
                var arr = ['scheme_label','locus_label','locus_description'];
                        for (var i in arr){
                                var name  = arr[i];
                                if (!self[name]){
                                        self[name] = data[name];
                                $("#"+self.id+"-"+name).text(self[name]);
                        }
                }
              
                
        });
         
     

}