
MicroReactParser.prototype = Object.create(null);
MicroReactParser.prototype.constructor = MicroReactParser;

function MicroReactParser(metadata,metadata_options,nwk,name,tree_id, ){
    this.metadata=metadata;
    this.metadata_options = metadata_options;
    this.name=name;
    this.nwk=nwk;
    this.tree_id=tree_id;
    this.max_sends=250;

}

MicroReactParser.prototype.start=function(callback,get_latlong){
    this.callback=callback;
    this.map_calls=0;
    if (get_latlong){
        this.initiateLatLongSearch();
        return;
    }
    this.sendToMicorReact();
}



MicroReactParser.prototype.initiateLatLongSearch= function(){
    var location_to_barcodes={};

    //work out what we are sending google maps
    for (var strain_id in this.metadata){
        var item = this.metadata[strain_id];
        var info ="";
        if (!item['latitude'] && item['latitude']!==0){
            if (item['city']){
                info = item['city']+" "+item['admin2']+" "+item["admin1"]+" "+item['country'];
            }
            else if (item['admin2']){
                info = item['admin2']+" "+item['admin1']+" "+item['country'];
            }
            else if (item['admin1']){
                info = item['admin1']+" "+item['country'];
            }
            else if (item['country']){
                info= item['country'];
            }
        }
        info = info.replace(" null "," ");
        if (info){
            var bar_list = location_to_barcodes[info];
            if (!bar_list){
                bar_list=[];
                location_to_barcodes[info]=bar_list;

            }
            bar_list.push(item['ID']);
        }
    }
   
    this.location_send_list=[];
    for (var location in location_to_barcodes){
        this.location_send_list.push({"location":location,"barcodes":location_to_barcodes[location]})
    }
    if (this.location_send_list.length >0){
        this.sendLatLongSearch(0);
        return;
    }
    //no geographic details to work out
    this.sendToMicroReact();
}


MicroReactParser.prototype.sendLatLongSearch= function(list_index){
    var self = this;
    if (self.map_calls>self.max_sends ||  list_index >=self.location_send_list.length){
        self.sendToMicroReact();
        return;
    }
    var location = self.location_send_list[list_index]['location']

    GMaps.geocode({
        address: location,
        callback: function(results, status){

            if(status=='OK'){
                var latlng = results[0].geometry.location;
                var barcodes = self.location_send_list[list_index]['barcodes']
                for (var i in barcodes){
                    var barcode = barcodes[i];
                    for(var strain_id in self.metadata){
                        var item = self.metadata[strain_id];
                        if (item['ID']===barcode){
                            item['latitude']=latlng.lat();
                            item['longitude']=latlng.lng();
                            break;
                        }
                    }
                }
                list_index++;
                self.callback({status:"running","msg":"Retreived GPS for "+list_index+"/"+self.location_send_list.length});
            }
            else{
                console.log(status);
            }
            self.map_calls++;
            setTimeout(function(){
                self.sendLatLongSearch(list_index);
            },300);
        }
    });
}

MicroReactParser.prototype.sendToMicroReact= function(){
    var self = this;
    //first parse of data to remove empty columns and add colours
    self.callback({status:"running",msg:"Formatting Metdata"});
    var meta = this.metadata;
    var fields = [];
    for (var name in this.metadata_options){
        if (name=='Accession'){
            continue;
        }
        fields.push(name);
        
    }
    var categories={};
    for (var i in fields){
        categories[fields[i]]={};

    }
    for (var strain_id in meta){
        var info = meta[strain_id];
        var date = ""
       
        if (info['collection_month']){
            date = info['collection_year']+"-"+info['collection_month'];
        }

        if (info['collection_year']){
            date = info['collection_year'];
        }
        delete info['collection_year'];
        delete info['collection_month'];
        delete info['collection_date'];
        if (date){
            info['collection_date']=date;
        }
        
        for (var i in fields){
            var label = fields[i];
            var val = info[label];
            if (val){
                categories[label][val]=true;
            }
        }	
    }
    var colors={};
    var temp_fields=[];
    var colour_pallete =d3.scale.category20().range().concat(d3.scale.category20b().range(),d3.scale.category20c().range());
    for (var label in categories){	
        //no colours for these just add it
        if (label==='Name' || label==='Latitude' || label ==='Longitude' || label==='Collection Date'){
            temp_fields.push(label);	
        }
        else{
            colors[label]={};
            var color_index=0;
            var values  = categories[label];
            for (var value in values){

                colors[label][value]=colour_pallete[color_index];
                color_index++;			
            }
            //only add those with categories
            if (color_index !==0){
                temp_fields.push(label);
            }
        }

    }
    fields=temp_fields;


    var header =  fields.join(",");


    for (var i in fields){
        var label = fields[i];
        if (colors[label]){		
            header+=","+label+"__colour";
        }
    }
    var lines = ["id,"+header];
    lines[0]=lines[0].replace(/\./g, '');
    lines[0]=lines[0].replace(/\s+/g, '_');
    lines[0]=lines[0].toLowerCase();

    for (var strain_id in meta){
        var info =  meta[strain_id];
        var line = info['ID'];
        for (var i in fields){
            var label = fields[i];
            var val = info[label];
           
            if (!val && val !==0){
                val="";
            }
            if (val.replace){
                val=val.replace(/,/g,"-");
            }
            line+=","+val;
            if (line.split(",").length>35){
                console.log(line);
            }
        }
        for (var i in fields){
            var label = fields[i];
            if (colors[label]){
                var val = info[label];
                if (!val){
                    line+=","
                }
                else{
                    line+=","+colors[label][val];
                }
            }

        }
        lines.push(line);
    }
    var commas= lines[0].split(",").length-1;

    var obj={
        name:this.name,
        timeline_field:"collection_date",
        timeline_format:"YYYY-MM",
        tree:this.nwk,
        data:lines.join("\n")
    };
    obj['map_countryField']="country";
    obj['map_grouped']=true;


    var to_send={
        "info":JSON.stringify(obj),
        "snp_id":this.tree_id
    }
    self.callback({status:"running",msg:"Sending data to microreact"})
    Enterobase.call_restful_json("/send_microreact","POST",to_send,displayErrorMessage).done(function(data){
        if (data['error']){
            msg = "There were the following errors in submission<ul>";
            for (var i in data['log']){
                msg+="<li>"+data['log'][i]+"</li>"		
            }
            msg+="</ul>";
            self.callback({status:"error",msg:msg}); 
        }
        else{
            self.callback({status:"complete",msg:data['url']});
    }
    });
}