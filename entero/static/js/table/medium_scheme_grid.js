/* global ValidationGrid, Enterobase, SchemeGrid */

MediumSchemeGrid.prototype = Object.create(BaseSchemeGrid.prototype);
MediumSchemeGrid.prototype.constructor= MediumSchemeGrid;


//create new grid giving the name of the species and the scheme
function MediumSchemeGrid(name,config,species,scheme,scheme_name){
    
    BaseSchemeGrid.call(this,name,config,species,scheme,scheme_name);
   
    //data for retreiving all allele data for records
    this.alleleChunkSize=200;
    this.alleleData={};
    this.arrBarcodes=[];
    this.alleleChunkTotal=0;
    this.waitingDialog=null;
    this.selectedOnly=false;
  
   
   
};

MediumSchemeGrid.prototype.networkError= function(msg){
     
     if (this.waitingDialog){
        this.waitingDialog.close();
     }
     Enterobase.modalAlert("There was an error retreiving data","Warning");

}




MediumSchemeGrid.prototype.showMessage = function(msg){
     window.open(Enterobase.wiki_url+"Experiment_Fields#"+msg,'newwindow','width=1000, height=600');
};



MediumSchemeGrid.prototype.displayAllelePopup = function(barcode,strain,st){
    var self = this;
    Enterobase.call_restful_json("/get_sts_by_barcode","POST",{barcode:barcode},this.showMessage).done(function(data){
        var lines=[];
        var alleles = data[barcode];

        var objKey=Object.keys(alleles);
        objKey.sort(function(a,b){
            return a === b ? 0 : (a>b ? 1 : -1);
        });        
        
        for (var locus in objKey){
            lines.push("<li>"+objKey[locus]+":"+alleles[ objKey[locus] ]+"</li>");            
        }
        Enterobase.modalAlert("<ul>"+lines.join("")+"</ul>",strain+" ST:"+st);
    
    });
};






MediumSchemeGrid.prototype.setMetaData= function(data){
     
    var self =  this;
    var grid_data=[];
    for (var index in data){
         
          var val = data[index]['name'];
          //if (val=== 'st' || val ==='st_complex' || val ==='lineage' || val === 'subspecies'|| val === 'serotype_pred'){
            grid_data.push(data[index]);
            //continue;
          //}               
          self.loci.push(val);
                  
    }
    grid_data.push({
        name:"view_alleles",
        label:"<span class='glyphicon glyphicon-eye-open'></span>",
        display_order:0,
        not_write:true
    
    });
    this.addExtraRenderer("view_alleles",function(cell,value,rowID){
        if (self.extraRowInfo[rowID]){
            $(cell).html("<span class='glyphicon glyphicon-eye-open style='padding:0px;margin:0px;display:inline-block;font-size:+"+self.fontSize+"px'></span>")                            
            .css("cursor","pointer");
        }
        else{
            $(cell).html("&nbsp");
        }
    });
    this.addExtraRenderer("st",function(cell,value,rowID){
        if (value<0){
            $(cell).html("New") ;                                   
        }
        else{
            $(cell).html(value);                                     
        }

    });  
    
    
   
   this.addCustomColumnHandler("view_alleles",function(cell,rowIndex,rowID){
        var barcode = self.extraRowInfo[rowID];
        var colIndex = self.strainGrid.getColumnIndex('strain');
        var strain = self.strainGrid.getValueAt(rowIndex,colIndex);
        colIndex = self.getColumnIndex("st");
        var st = self.getValueAt(rowIndex,colIndex);
        if (barcode){
            self.displayAllelePopup(barcode,strain,st);
        }
   
   });
    
    BaseSchemeGrid.prototype.setMetaData.call(self,grid_data);
    this.columnWidths["view_alleles"]=30;
         
};

MediumSchemeGrid.prototype.getMethodData= function(){
    var list = BaseSchemeGrid.prototype.getMethodData.call(this);
    var self = this;
    var add_list = [{
        name:"download-alleles",
        label:"Download Allelic Profile",
        method:function(){
        self.selectedOnly=false;
            self.saveAllAlleleData(); 
        }
    }];
    
    return list.concat(add_list);

}




MediumSchemeGrid.prototype.getAllAtLevel = function(rowIndex,colIndex){
        var value = this.getValueAt(rowIndex,colIndex);
        var col_name= this.col_list[colIndex]['name']
        var query = col_name+"="+value;
        to_send={
                experiment_query:query
        }
        submitQuery(to_send);
}

MediumSchemeGrid.prototype.addToContextMenu =  function (rowIndex,colIndex,target){
    var self = this;
    var extramenu= BaseSchemeGrid.prototype.addToContextMenu.call(this,rowIndex,colIndex,target);
    extramenu.push ({
                 name: 'Get at this level',
                 title: 'Get all this level',
                 fun: function () {
                         self.getAllAtLevel(rowIndex,colIndex);
                }
         });
    extramenu.push ({
             name: 'Download Allelic Profile',
             title: 'Download Allelic Profile',
             fun: function () {
             self.selectedOnly=false;
                 self.saveAllAlleleData();
             }
         });
      extramenu.push (  { 
            name:"Download Allelic Profile (Selected Only)",
            label:"Download Allelic Profile (Selected Only)",
            fun:function(){
                self.selectedOnly=true;
                self.saveAllAlleleData(true); 
            }
            });
    return extramenu;
};


MediumSchemeGrid.prototype.saveAlleleDataToDisk= function(self){
    self.waitingDialog.setMessage("Processing Allele Information");
    var lines=[];
    //get all the alleles
    var allele_set={}
    for (var barcode in self.alleleData){
        allele_data =self.alleleData[barcode];
        for (var locus in allele_data){
            allele_set[locus]=true;
        }
        
    
    }
    var header = "Name\tST";
    for (var locus in allele_set){
           header+="\t"+locus;
    }
    lines.push(header);
    var nameIndex = self.strainGrid.getColumnIndex('strain');
    var   stIndex = self.getColumnIndex("st");
        //add all the other lines
        for (var n=0;n<self.getRowCount();n++){  
            var id =  self.getRowId(n);
            if (self.selectedOnly && ! self.strainGrid.selectedRows[id]){
                continue;
            }
            //add the strain name to the line
            var line = self.strainGrid.getValueAt(n,nameIndex)+"\t"+self.getValueAt(n,stIndex);
            //get st barcode for this record
            var barcode = self.extraRowInfo[id];
            if (! barcode){
                line+="\tno data";
                continue;
            }
            //now add the alleles
            var alleles = self.alleleData[barcode];
            for (var locus in allele_set){
                var id  = alleles[locus];
                if (! id){
                    id="-";                
                }
                else if (id <0){
                    id =-1;
                }
                line+="\t"+id;
            }
            lines.push(line)
        }
        //save as one big text file
        self.waitingDialog.close();
        self.waitingDialog=null;
        Enterobase.showSaveDialog(lines.join("\n"));
        self.alleleData={};

}

MediumSchemeGrid.prototype.getProfilesForPhyloviz = function(callback,input,onlySelected){
        var stColIndex = this.getColumnIndex('st');     
       
        var barcode_to_ST ={};
        var barcodes=[];
        var self = this;
        //get the barcodes and link to st
        for (var rowIndex=0;rowIndex<this.getRowCount();rowIndex++){
                var id =  this.getRowId(rowIndex);
                if (onlySelected && ! this.selectedRows[id]){
                        continue;                
                }            
                var st = this.getValueAt(rowIndex,stColIndex);
                var barcode = this.extraRowInfo[id];
                if (! barcode){
                    continue;
                }
                barcode_to_ST[barcode]=st;
                
        }
        for (var barcode in barcode_to_ST){
            barcodes.push(barcode);
        }
        if (true){
            input.barcodes=barcodes;
            input.barcode_to_ST=barcode_to_ST;
            callback(input);
            return;
        
        }
        var self = this;
        if (!$.isEmptyObject(self.alleleData)){
            var profiles = [];
            for (var barcode in self.alleleData){
                var record = self.alleleData[barcode];
                record["ID"]="ST"+barcode_to_ST[barcode];
                profiles.push(record);
            }
            input.profiles=profiles;
            callback(input)
            return;
        }
        //strart the allele request and send the function
        //that will process the final data;
        this.startAlleleRequest(barcodes,function(){
            var profiles = [];
            for (var barcode in self.alleleData){
                var record = self.alleleData[barcode];
                record["ID"]="ST"+barcode_to_ST[barcode];
                profiles.push(record);
            }
            input.profiles=profiles;
            self.waitingDialog.close();
            self.waitingDialog=null;
            callback(input);                
        });
       
          
}


MediumSchemeGrid.prototype.sendAlleleRequest = function (index,processFunction){
    var self = this;
    Enterobase.call_restful_json("/get_sts_by_barcode","POST",{barcode:self.arrBarcodes[index].join(",")},this.networkError).done(function(data){
        if (! self.waitingDialog){
            return;
        }
        $.extend(true,self.alleleData,data);
        var chunkTotal = self.arrBarcodes.length;
        index++;
        var percent =  (index *100)/chunkTotal;
        self.waitingDialog.setStatus(percent);
        
        if (index >= chunkTotal){
            processFunction(self);
        }
        else{
            self.sendAlleleRequest(index,processFunction);
        }
                    
     });

}
/** Initiates the calling of alleles according to the supplied bar codes
 * @param {list} barcodes - The ST barcodes 
 * @param {function} processFunction The callback used to process the allele data once all
 * data has been retrieved
 *
*/

MediumSchemeGrid.prototype.startAlleleRequest = function(barcodes,processFunction){
    var self = this;
    
    //reset data
    this.alleleData={};
    this.alleleChunksTotal=0;
    this.waitingDialog = new Enterobase.ModalWaitingDialog("Retrieving Allele Information",
    function(){
        //will not send any more requests
        self.waitingDialog.close();
        self.waitingDialog=null;
    }
    );
    this.arrBarcodes = [];
    
    while(barcodes.length>0){
        this.arrBarcodes.push(barcodes.splice(0,this.alleleChunkSize));
    }
    
    self.sendAlleleRequest(0,processFunction);

}


MediumSchemeGrid.prototype.panGenomeAnalysis= function(){
    //get all the st barcodes (present in extraRowInfo);
    var barcodes = [];
    for (var n=0;n<this.getRowCount();n++){  
          var id =  this.getRowId(n);
          var barcode = this.extraRowInfo[id];
          if (! barcode){
              continue;
          }
          barcodes.push(barcode);
    }
    
    Enterobase.call_restful_json("/get_pan_genome","POST",{barcodes:barcodes.join(",")},this.networkError).done(function(data){
        
     });
    
};

MediumSchemeGrid.prototype.saveAllAlleleData= function(){
    //get all the st barcodes (present in extraRowInfo);
    var barcodes = [];
    for (var n=0;n<this.getRowCount();n++){  
          var id =  this.getRowId(n);
          if (this.selectedOnly && ! this.strainGrid.selectedRows[id]){
                continue;
          }
          var barcode = this.extraRowInfo[id];
          if (! barcode){
              continue;
          }
          barcodes.push(barcode);
    }
    if (barcodes.length===0){
        return;
    }
    this.startAlleleRequest(barcodes,this.saveAlleleDataToDisk)
   
  
       
};
