UploadReadsValidationGrid.prototype = Object.create(StrainValidationGrid.prototype);
UploadReadsValidationGrid.prototype.constructor = UploadReadsValidationGrid;
/**
 * Represents a Grid allowing the addition of metadata describing a strain
 * and also allows choosing of local files
 * @constructor
 * @extends StrainGrid
 * @param {string} name - The name of the grid
 * @param {object} config - Configuration options
 * @param {string} species - The name of the database associated with this table
 */
function UploadReadsValidationGrid(name,config,species,allowed_reads){
	//call super constructor
    StrainValidationGrid.call(this,name,config,species);
    var self = this;
    this.editMode=true;
	this.files = {};
	this.id_to_files={};
	this.serverReadStatus={};
	this.failed_file_rows = {};
	this.ignoreList= ['Project','barcode','release_date','created','Sample','uberstrain'];
	this.fileUpload = null;
	this.bad_local_files = {};
	this.allowed_reads=[];
	if (allowed_reads){
		this.allowed_reads=allowed_reads.split(",");
	}
	//a custom validator for the reads
	// this.setCustomFileParser("Accession",function(rowIndex,colIndex){

	//	},
	//	function(text,columnName,rowID,headers,values){
	//			text= text.replace(", ", "");
	//			text= text.replace(" , ", "");
	//			return self.comp_values.addValuesFromString(columnName, rowID, text);

	//});

	this.addCustomCellValidator("Accession", function(colName, rowID){
		var rowIndex = this.getRowIndex(rowID);
		var colIndex= this.getColumnIndex(colName);
		//check basics e.g. platform,library present and correct
		if (!this.validateCompoundField(colName, rowID)){
			msg = "One or more values in this field are incorrect";
			return this.updateErrors(colName, rowID, msg);
		}

		//Next check that reads are correctly paired
		var ids = this.comp_values.getRowIDs("Accession", rowID);
		var pair = 0;
		var insertSize = 0;
		var msg = "";

		for (var index in ids){
			row = this.comp_values.getRow("Accession",rowID,ids[index]);
			var len = row[0].length;
			if (row[0] === 'Click to Add File'){
				return this.updateErrors(colName,rowID,"Files Need To Be Added");
			}
			if (this.serverReadStatus[row[0]]){
				return this.updateErrors(colName,rowID,"Read has already been assigned To an Assembly");
			}
			if (row[1] !== 'ILLUMINA' && this.allowed_reads.indexOf(row[1]) === -1){
				return this.updateErrors(colName,rowID,"You do not have permission to upload this kind of read");
			}
			if (row[1] === 'ILLUMINA' && row[0].substring(len-3,len) !== ".gz"){
				return this.updateErrors(colName,rowID,"File must be a gzip file (.gz)");
			}
			//if (row[1]==='Complete Genome' && row[0].substring(len-6,len)!==".fasta"){
			//	return this.updateErrors(colName,rowID,"File must be a fasta file");
			//}
			if (row[2] === "Paired" && row[1] !== 'ILLUMINA' && pair === 0){
				pair = 1;
				insertSize = row[3];
				continue;
			}
			if (row[2] !== "Paired" && pair === 1){
				msg = "Paired reads not adjacent";
				break;
			}
			if (row[2] === "Paired" && pair === 1){
				pair = 0;
				if (row[3] !== insertSize){
					msg = "Paired reads have different insert size";
					break;
				}
				insertSize=0;
			}
		}
		if (pair === 1){
			msg = "Reads are not paired"
		}
		if(msg){
			return this.updateErrors(colName, rowID, msg);
		}
		this.updateErrors(colName, rowID, msg);
		//check that no reads have been duplicated
		this.duplicateRecord();
		//this.refreshGrid();

		//add a listener - checks for duplicate reads
		//and adds two lots of



    });
	var fileList = ['']
	//check any duplicates have been removed
	this.addRowRemovedListener=function(rowID){
		var rowIndex = this.getRowIndex(rowID);
		var colIndex = this.getColumnIndex("Accession");
		var val1 = this.getValueAt(rowIndex,colIndex);
		if (val1){
			this.duplicateRecord(rowID);
		}
	};


	//make sure rows are pairs
	this.rowAddedListener = function(rowID){
		this.comp_values.addRow("Accession", rowID);
		this.comp_values.addRow("Accession", rowID);
	};


};

/**Validates the local file -
 * a) ends with .gz
 * b) greater than 2MB
 * c) is a fastq file
 * @param {File} file The file object to be validated
 * @param {Function} callback The method called when file is validated
 * passing the file object and reason  why the validation failed or empty string
 * if the file was valid {@link fileValidated}
*/

UploadReadsValidationGrid.prototype.validateReadFile= function(file,seq_platform,callback){
		var name = file.name
		var self = this;
		if (seq_platform === 'Complete Genome' || seq_platform === 'legacy'){
			callback(file,"");
			return;
		}
		if (!Enterobase.endsWith(name,[".gz"])){
			callback(name,"Not a gzip File");
			return;
		}
		if (file.size < 2000000){
			callback(name,"File less than 2MB ");
			return;
		}
		var reader = new FileReader();
		reader.readAsArrayBuffer(file.slice(0,1000));
		reader.onload = function(e){
		try{
				var result= pako.inflate(reader.result,{to:'string'});
				var lines = result.split("\n");
				if (lines[0].substring(0,1) === '@' && lines[2].substring(0,1)=='+'){
						callback(file,"")
						return;
				}
				callback(file,"Not a fastq file")

		}catch(e){
			callback(file,"Not a fastq file");
		}
	};
}

/** This callback is is supplied to the {@link UploadReadsValidationGrid.validateReadFile} method
* and is used to add extra elements and functionality to the dialog
* @name fileValidated
* @function
* @param {File} file - The file that has been valiated
* @param {string} reasonFailed - An empty string if the file failed or one of the
* following messages:- Not a gzip file,File less then 2MB, Not a fastq file
*
*/






UploadReadsValidationGrid.prototype.setReadDefaults = function(){
    var comp = this.comp_values;
    var cn = 'Accession'
    for	(var i =0;i<this.getRowCount();i++){
		var rowID = this.getRowId(i);
		var ids= comp.getRowIDs(cn,rowID);
		var type= 'Single';
		if (ids.length ==2){
			type = 'Paired';
		}
		for (var i2 in ids){
			if(!comp.getValue(cn,rowID,ids[i2],'seq_platform')){
				comp.setValue(cn,rowID,ids[i2],'seq_platform',"ILLUMINA");
			}
			if(!comp.getValue(cn,rowID,ids[i2],'seq_library')){
				comp.setValue(cn,rowID,ids[i2],'seq_library',type);
			}
		}
    }
}

UploadReadsValidationGrid.prototype.validateReads = function(){

	var self = this;
	var info_table= [];
	for (var r=0;r<this.getRowCount();r++){
		var rowID = this.getRowId(r);
		var row_data ={};
		var all_good = true;
		var c= this.getColumnIndex("strain");
		//loop through all the files to see their status
		row_data['name'] = this.getValueAt(r,c);
		var ids = this.comp_values.getRowIDs("Accession",rowID);
		var file_list = [];
		var good = true;
		for (var index in ids){
			var tag = "read"+index;
			var read = this.comp_values.getRow("Accession",rowID,ids[index])[0];
			var fileHandle = read;
			var status = this.serverReadStatus[read];
			var fileHandle = this.files[read];

			if (status === "Uploaded No Metadata"){

				row_data[tag+"_info"] = "OK:Uploaded";
			}
			else if (status  === "Queued" || status === "Assembled" || status === "Uploaded"){
				row_data[tag+"_info"] = "FAIL:Already Processed";
				good = false;

			}

			else if (fileHandle){
				var reason = this.bad_local_files[fileHandle.name];
				if (reason){
					row_data[tag+"_info"] = "FAIL:"+reason;
					good= false;
				}
				else{
					row_data[tag+"_info"] = "OK:Local Directory";
				}
			}
			else{
				if (! read || read === 'Click to Add File' ){
					row_data[tag] = "None"
					row_data[tag+"_info"] = "FAIL:No File specified";
				}
				else{
					row_data[tag+"_info"] = "FAIL:Cannot Find File -associate local folder or upload";
				}
				good = false;
			}

		}
		row_data['pass'] = "OK"
		if (!good){
			row_data['pass'] = "FAIL";
			this.failed_file_rows[rowID]=true;
			all_good=false;
		}

		row_data['metadata'] = this.errorRows[rowID]?"FAIL":"OK"
		if (row_data['metadata'] !== "OK"){
			all_good=false;
		}

		info_table.push(row_data);

	}
	var msg ="";
	if (!all_good){
		msg = "There are errors in your submission - please correct these before continuing<br><br>"
	}
	else{
		msg = "Your submission contains no errors - Press OK to upload any files which are required and proceed with the assembly or cancel to make any changes<br><br>"
	}
	msg+="<table class='info-table'>";
	msg+= "<tr style='font-weight:bold'><td>Strain</td><td>Read 1</td><td>Read 1 Info</td><td>Read 2</td><td>Read 2 Info </td><td>Metadata</td><td>Reads</td></tr>"

	for (index in info_table){
		var row =info_table[index];

		msg+= "<tr><td>"+row['name']+"</td><td>"+row["read0"]+"</td><td>"+row["read0_info"]+"</td><td>"+row["read1"]+"</td><td>"+row["read1_info"]+"</td><td>"+row['metadata']+"</td><td>"+row["pass"]+"</td></tr>";
	}
	msg+="</table>";
	var dia = Enterobase.modalAlert(msg,"Information",false,function(){
		if (all_good){
			self.uploadAndAssemble();
		}
	}).dialog("option","width","auto").dialog("option","height","auto");
	if (all_good){
		var buttonSet = dia.parent().find('.ui-dialog-buttonset');
		var newButton = $('<button>Cancel</button>');
		newButton.button().click(function () {
			dia.dialog("close");
		});
		buttonSet.append(newButton);
	}






}


UploadReadsValidationGrid.prototype.associateFolder = function(){
	var self = this;
	var fb = new Enterobase.FileBrowser(function(files){
		for (index in files){
			var  file = files[index];
			self.files[file.name]=file;
		}
	},"",true,true);
	fb.getFiles();
}



UploadReadsValidationGrid.prototype.readUploader = function(){
	//now check reads for content

	//
	f_list=[];
	for (var name in this.files){
		f_list.push(this.files[name]);

	}
	this.fileUpload.fileupload("add",{files:f_list});

	Enterobase.modalAlert("Reads have been processed - They now need to be uploaded","Information",false,function(){
		 $( "#tabs" ).tabs("option","active",1);


	})
	//store all metadata in record for all data/reads which have passed the tests









}
/**
 * Gets the information from the grid and uploads the read names and
 * associated metadata from the grid to the server
 * @param {string} sraEmail - The email of the supervisor
 * @param {integer} releasePeriod - The time in months which need to elapse before the assembly can be downloaded
 * @returns {object} A dictionary conatining two items <ul>
 * <li>local_files - a list of file objects that were specified in the grid</li>
 * <li>read_info - a list of dictionaries of the read information e.g
 * <code>+{strain:name,read_1:name,read_1_status:status} <code> . the dictionary
 * can be used to load {@link ReadsGrid}</li><ul>
 */
UploadReadsValidationGrid.prototype.updateReadInfo= function(sraEmail,releasePeroid){
	var meta_data_list = [];
	var self=this;
	var local_files=[];
	var read_info=[];
	var read_files=[];
	/*
	Fix issue of having duplicated read files in database (having more one record with same file name for the same database for the same user)
	The reason of it was that the user submit strain/s metadata with the same read files at the same session
	i.e. submit strain's metadata to the server and then back to "Add metadata panel" and add additional strain/s metadata
	as the front end did not add the submitted read files to the already submitted read files
	the read_files array will contain the read files which will be submitted and these files will be added to self.serverReadStatu
	so if the user tried to use the same read files which have been submitted, it will prevent them from submitting them to the server and inform them
	 K.M. 06/01/2021
	*/
	for(var i=0;i<this.getRowCount();i++){
		var rowID  = this.getRowId(i);
		read_dict={};
		var ids = this.comp_values.getRowIDs("Accession",rowID);
		var count =1;
		var seq_platform ="";
		for (id in ids){
			row = this.comp_values.getRow("Accession",rowID,ids[id]);
			read_dict["read_"+count]=row[0];
			read_files.push(row[0]);
			seq_platform =row[1];
			if (this.files[row[0]]){
			 	read_dict["read_"+count+"_status"]="Local Upload";
				local_files.push(this.files[row[0]])
			}
			else{
				read_dict["read_"+count+"_status"]="Awaiting Upload";
			}
			count++;
		}
		var meta_data = this.getValuesForRecord(rowID,true);
		meta_data['release_period'] = releasePeroid;
		meta_data['sra_email'] = sraEmail;
		read_dict['strain'] = meta_data['strain'];
		read_dict['seq_platform']=seq_platform;
		read_info.push(read_dict);
		meta_data_list.push(meta_data);
	}
	var to_send = {database:this.species,
				   data:meta_data_list

	};
	Enterobase.call_restful_send_json("/store_temp_metadata","POST",to_send,function(msg){
		Enterobase.modalAlert("Could not store metadata: "+msg)
	});
	for (i in read_files)
	{
	 self.serverReadStatus[read_files[i]]="Awaiting Upload";
	}
	return {local_files:local_files,read_info:read_info};

}

UploadReadsValidationGrid.prototype.processMetaData = function(callback){
    var self = this;
	//change the properties for accession
    this.labels_to_names["Read Files"] = "Accession";
    this.labels_to_names['Read File'] = "accession";
    for (var i in this.col_list){
        var col = this.col_list[i];
        if (col['name']==="Accession"){
            col['label']="Read Files";
			col['editable']=true;
			col['max_rows']=2;
        }
    }




	//remove unecassary fields in accesion
	this.comp_values.dict['Accession']['fields'].splice(4,4);


	this.columnWidths['Accession']=250;

    //set for uploading reads
	var read = this.val_params['accession'];
	read['default_value'] = "Click to Add File";
	read['datatype']="custom";
	read['label'] = "Read File";
	this.val_params['accession']['getInput']= function(rowID){
		var input =$("<input>").attr("type","text").css("cursor","pointer").attr({"readonly":"readonly"});
		input.bind("click",function(){
			var number = $(this).attr("id").split("-")[1];
			var extension = ".gz";
			var seq_platform = $("#seq_platform-"+number).val();
			if (seq_platform ==='Complete Genome'){
				extension = ".fasta";
			}
			var fb = new Enterobase.FileBrowser(function(files){
				var name = files[0].name;
				if (self.serverReadStatus[name]){
					Enterobase.modalAlert("The file is already associated with an assembly");
					return;
				}

				//var format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
				var format = /[ !@#$%^&*()+\=\[\]{};':"\\|,<>\/?]/;
				if(format.test(name)){
					Enterobase.modalAlert("The file name should not contain special characters like [ !@#$%^&*()+=[]{};':\"\|,<>/?]");
					return;
				}

				if (!self.files[name]){
					self.validateReadFile(files[0],seq_platform,function(filename,reason){
					if (! reason){
						self.files[name] = files[0];
						input.val(files[0].name);

					}
					else{
						Enterobase.modalAlert("The file is not suitiable:"+reason);
						}

					});
				}
			},extension);
			fb.getFiles();
		}).mousedown(function(e){
			if (e.which ===3){
				if ($(this).val()==="Click to Add File"){
					$(this).val("");
				}
				$(this).removeAttr("readonly");
				}

		}).bind("onblur",function(){
			$(this).attr("readonly","readonly");
		}).contextmenu(function(){
			return false;
		});
		setTimeout(function() {
			$("#seq_platform-1").change(function(e, ui) {
				if($(e.target).val() === 'ILLUMINA') {
					$("#seq_library-1").val('Paired');
					$("#seq_library-1").trigger('change');
					if ($("#comp_dailog_table button").length == 1) {
						$("#edit_popup_but").click();
					}
				} else {
					$("#seq_library-1").val('Single');
					$("#seq_library-1").trigger('change');
					if ($("#comp_dailog_table button").length > 1) {
						$($("#comp_dailog_table button")[1]).click();
					}
				}
			});
		}, 500);
		return input;
	};
	//only Illumina and paired avaialable
	this.val_params['seq_platform']['values'] = ["ILLUMINA", "Complete Genome", "legacy"];
	this.val_params['seq_library']['values'] = ["Paired", "Single"];
	callback("OK");

};

UploadReadsValidationGrid.prototype.validateStrains=function(){
    StrainValidationGrid.prototype.validateStrains.call(this);
    this.checkDuplicates= false;
    if (this.getCompleteRowCount()>0){
         this.duplicateRecord();
    }
    this.checkDuplicates = true;
    this.errorListener(this.errorNumber,this.numberOfRowsWithErrors);
    this.refreshGrid();
}

UploadReadsValidationGrid.prototype.addToContextMenu =  function (rowIndex,colIndex,target){
    var rowID = this.getRowId(rowIndex);
    var colName = this.getColumnName(colIndex);

    var self = this;
    var extramenu =[
        {
        name: 'Help',
        title: 'Help',
        fun: function () {
		var page= "Metadata_Fields-";
		if (colName==='Accession'){
			colName = "reads";
			page='Add_upload_reads-';
		}
           window.open(Enterobase.wiki_url+page+colName,'newwindow','width=700, height=500');
        }
		},
        {
			name: 'Delete Row',
			title: 'Delete Row',
			fun: function () {
				self.removeRow(rowID);
			}
		},
		{
			name: 'Insert Row',
			title: 'Insert Row',
			fun: function () {
				self.insertRowAfter(rowIndex,self.default_values);
				self.refreshGrid();
			}
		}

    ];
    return extramenu;
};






UploadReadsValidationGrid.prototype.showMessage = function(msg){
	 if (msg==='Accession'){
			 window.open(Enterobase.wiki_url+"Add upload_reads",'newwindow','width=1000, height=600');
	 }
	 else{
		window.open(Enterobase.wiki_url+"Metadata_Fields-"+msg,'newwindow','width=1000, height=600');
	}
}

