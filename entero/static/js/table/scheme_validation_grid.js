SchemeGrid.prototype = Object.create(ValidationGrid.prototype);
SchemeGrid.prototype.constructor = SchemeGrid

function SchemeGrid(name,config,species,admin){

   var self = this;
   ValidationGrid.call(this,name,config);
   this.lineColors= ["#CCC","#FFF"]
   this.species=species;
   this.detailsCallback= function(){};
   this.jobDetailsURl = "/species/get_individual_job";
   this.admin=true;
}


SchemeGrid.prototype.setMetaData= function(){ 
                var self = this;
                self.addCheckSelectColumn();
      
        var scheme = {
             name:"scheme_label",
             label:"Scheme name",
             display_order:4
            
        }
        self.addNewColumn(scheme)
        var description = {
             name:"description",
             label:"Description",
             display_order:6
            
        }
        self.addNewColumn(description)
        var label = {
             name:"label",
             label:"Locus tag",
             display_order:5
            
        }
        self.addNewColumn(label)
               
  
               self.columnWidths['id'] = 60;
               self.columnWidths['scheme'] = 250;                
                
               self.columnWidths['label'] = 250;                
               self.columnWidths['description'] = 800;


}

SchemeGrid.prototype.downloadST= function(pipeline){
     if (pipeline=="UoW")
          pipeline="Achtman7GeneMLST";
     else if (pipeline=="wgMLSTv1")
          pipeline="wgMLST";
     else if (pipeline=="cgMLSTv1")
          pipeline="cgMLSTv2";
     is_found=false;
     for  (var i in this.extraRowInfo){
          var info = this.extraRowInfo[i];
          if (info['pipeline']  == pipeline) { 
          if (info['pipeline_species']!= "Salmonella" && pipeline=="cgMLSTv2")
                  pipeline="cgMLSTv1";
                  
              this.downloadFiles('', 'profiles', pipeline,info['pipeline_species']  );          
              is_found=true;
              break;
         }
     }     
     if (!is_found)
         {     

          if (pipeline=="cgMLSTv2")
                   pipeline="cgMLSTv1";
    
          for  (var i in this.extraRowInfo){
              var info = this.extraRowInfo[i];
              if (info['pipeline']  == pipeline) {          
                      
                  this.downloadFiles('', 'profiles', pipeline,info['pipeline_species']  );          
                  break;
             }
         }
     }
        
}

                

SchemeGrid.prototype.downloadFiles= function(Ids,type, pipeline, pipeline_species){
     var downloaded = false;
     var msg = "<ul>";
     var downloaded =0;
     if (type == 'profiles') { 
         // Enterobase.downloadURL('/download_data?species=' + pipeline_species + "&scheme=" + pipeline + "&allele=profiles");                      
          Enterobase.downloadURL('http://enterobase.warwick.ac.uk/schemes/' + pipeline_species + '.' + pipeline + "/profiles.list.gz");                      
          msg+="<ul>"
          msg = "ST profiles have been downloaded<br>"+msg;
          Enterobase.modalAlert(msg);
        
     }
     else{         
          for (var index in Ids){
               var raw_row = this.extraRowInfo[Ids[index]];
               Enterobase.downloadURL('http://enterobase.warwick.ac.uk/schemes/' + raw_row['pipeline_species'] + '.' + raw_row['pipeline'] + "/" + raw_row['name'] + ".fasta.gz");                      
               //Enterobase.downloadURL('/download_data?species=' + raw_row['pipeline_species'] + "&scheme=" + raw_row['pipeline'] + "&allele=" + raw_row['name']  );                      
               downloaded++;
          }         
     }
     if (downloaded > 0) {
          msg+="<ul>"
          msg = downloaded + " File(s) have been downloaded<br>"+msg;
          Enterobase.modalAlert(msg);
     }
     else{
          msg+="<ul>"
          msg = "No Loci have been selected<br>"+msg;
          Enterobase.modalAlert(msg);      
     }  
};