BaseExperimentGrid.prototype = Object.create(ValidationGrid.prototype);
BaseExperimentGrid.prototype.constructor= BaseExperimentGrid;

/**
 * The base grid for all schemes/experiments to be displayed in the right table of the 
 * @constructor
 * @extends ValidationGrid
 * @param {string} name - The name of the grid
 * @param {object} config - Configuration options e.g.
 * <pre><code>{
 *     enableSort: true, 
 *     editmode: "static",
 *     editorzoneid: "edition",
 *     allowSimultaneousEdition: false,
 *     pageSize:25
 *}
* </code></pre>
 * @param {string} species - The name of the database associated with this table
 * @param {string} scheme The name (description) of the scheme/experiment
 * @param {string} scheme_name The label that the scheme uses
 */
function BaseExperimentGrid(name,config,species,scheme,scheme_name){
        this.species=species;
        this.scheme= scheme;
        this.scheme_name =scheme_name;
        ValidationGrid.call(this,name,config);
        this.strainGrid=null;
   
};


/**
 * Shows a dialog box with the given message
 * @param {string} msg The message to display
*/
BaseExperimentGrid.prototype.showError =  function (msg){
        Enterobase.modalAlert(msg);
}


BaseExperimentGrid.prototype.showMessage = function(msg){
     window.open(Enterobase.wiki_url+"Experiment_Fields#"+msg,'newwindow','width=1000, height=600');
};


/**
* Method to obtain functions from the grid to add to the Experiment menu
* on the main search page
* This method should be overwritten in sub classes
* @returns {list} A list of objects which have name,label and method e.g.
* <code><pre>
* var self = this;
* return [{name:"do_stuff",
                label:"Do Stuff",
                method:function(){self.do stuff()}
                }];
* </code></pre>
*/
BaseExperimentGrid.prototype.getMethodData= function(){
       return [];
};



/**
* Puts a space in the first column of very row (if empty). This is
* to ensure that all rows are of equal height
*/
BaseExperimentGrid.prototype.loadNewData= function(data){
      var numCol = this.col_list.filter( c => c.datatype === 'double').map( c=> c.name);
      this.max_value = {};
      for (var c of numCol) {
          this.max_value[c] = -999999.;
      }
      var first_col =this.col_list[0]['name']
      for (var record of data){
            if (record[first_col] === undefined){
               record[first_col]='  ';
           }
           for (var c of numCol) {
              var v = Number(record[c]);
              if ( !isNaN(v) && v > this.max_value[c])
                  this.max_value[c] = v;
           }
       }  
       ValidationGrid.prototype.loadNewData.call(this,data,false,true);
}


/**
* Stores a pointer to the strainGrid
*/
BaseExperimentGrid.prototype.setStrainGrid= function(strainGrid){
        this.strainGrid=strainGrid;
}


/**
* Retreives the name of the strain in the linked strain table associated with the row
* @param {integer} row_index - The row index
* @returns {string} The name of the strain
*/
BaseExperimentGrid.prototype.getStrainName=function(row_index){
        var col_index= this.strainGrid.getColumnIndex('strain');
        return this.strainGrid.getValueAt(row_index,col_index);
}

