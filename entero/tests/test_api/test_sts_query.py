# import unittest
import pytest
#import flask
#from flask import current_app
#from entero import create_app, config
#import os
# from entero.databases.system.models import User

from entero.test_utilities import ebHTTPNotAuth, get_api_headers, enHashInfoQueryResponseData

from urllib2 import HTTPError
import urllib2
import json
import re

@pytest.mark.slow
class TestSTsQueryCase1(object):
    sts_query1 = "/api/v2.0/senterica/MLST_Achtman/sts?scheme=MLST_Achtman&show_alleles=false&limit=5"
  
    @pytest.fixture(scope='class') 
    def sts_query1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.sts_query1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def sts_query1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.sts_query1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def sts_query1_jack_auth_response_data(self, sts_query1_jack_auth_response):
        return json.loads(sts_query1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Known that test fails due to status code 200 when it should get status code 401")
    def test_sts_query1_bad_auth_status(self, sts_query1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert sts_query1_bad_auth_response.status_code == ebHTTPNotAuth
        assert sts_query1_bad_auth_response.status_code == 401

    def test_sts_query1_jack_auth_status(self, sts_query1_jack_auth_response):
        assert sts_query1_jack_auth_response.status_code == 200

    def test_sts_query1_non_empty(self, sts_query1_jack_auth_response_data):
        assert sts_query1_jack_auth_response_data is not None
        assert len(sts_query1_jack_auth_response_data) > 0

    def test_sts_query1_has_sts_key(self, sts_query1_jack_auth_response_data):
        assert "STs" in sts_query1_jack_auth_response_data

    def test_sts_query1_has_sts_content(self, sts_query1_jack_auth_response_data):
        assert len(sts_query1_jack_auth_response_data["STs"]) > 0

    def test_sts_query1_has_links_key(self, sts_query1_jack_auth_response_data):
        assert "links" in sts_query1_jack_auth_response_data

    def test_sts_query1_records_gt_0(self, sts_query1_jack_auth_response_data):
        assert sts_query1_jack_auth_response_data["links"]["records"] > 0
