# import unittest
import pytest
#import flask
#from flask import current_app
#from entero import create_app, config
#import os
# from entero.databases.system.models import User

from entero.test_utilities import ebHTTPNotAuth, get_api_headers, enHashInfoQueryResponseData

from urllib2 import HTTPError
import urllib2
# import base64
import json
import re

# jackAPIKey = None

## According to HTTP standards 403 is the status code for flat out
## forbidden (i.e. authorisation won't help); and
## 401 is the status code for unauthorized.  Current EnteroBase
## Swagger UI documentation says 403 is for unauthorized; so
## treating that as correct for now.
#ebHTTPNotAuth = 403

#@pytest.fixture(scope="module")
## WVN 14/2/18
## app is an argument since this depends on the app fixture.
## This looks weird; but it really does make whether or not
## you get an exception and a stack trace in the function below.
#def eb_jack_api_key(app):
#    # TEMP - later use the recommended approach
#    # jackAPIKey = "eyJhbGciOiJIUzI1NiIsImV4cCI6MTU0OTk5MTExMywiaWF0IjoxNTE4NDU1MTEzfQ.eyJ1c2VybmFtZSI6ImphY2siLCJjaXR5IjoiTWV0cm9wb2xpcyIsImNvbmZpcm1lZCI6MSwiY2hhbmdlX2Fzc2VtYmx5X3N0YXR1cyI6IlRydWUiLCJmaXJzdG5hbWUiOiJKYWNrIiwiY2hhbmdlX3N0cmFpbl9vd25lciI6IlRydWUiLCJjaGFuZ2VfYXNzZW1ibHlfcmVsZWFzZV9kYXRlIjoiVHJ1ZSIsImNvdW50cnkiOiJVbml0ZWQgS2luZ2RvbSIsImlkIjoxNTgsImFkbWluaXN0cmF0b3IiOm51bGwsImFwaV9hY2Nlc3Nfc2VudGVyaWNhIjoiVHJ1ZSIsImVtYWlsIjoibS5qLnNlcmdlYW50MUBnb29nbGVtYWlsLmNvbSIsImRlcGFydG1lbnQiOiJXZWlyZCBDaGVtaWNhbHMgRGVwYXJ0bWVudCIsInZpZXdfc3BlY2llczEiOiJUcnVlIiwibGFzdG5hbWUiOiJTbWl0aCIsImFjdGl2ZSI6bnVsbCwidmlld19zcGVjaWVzIjoiVHJ1ZSIsImVkaXRfc3RyYWluX21ldGFkYXRhIjoiVHJ1ZSIsImRlbGV0ZV9zdHJhaW5zIjoiVHJ1ZSIsImluc3RpdHV0aW9uIjoiQUNNRSBsYWJzIiwidXBsb2FkX3JlYWRzMSI6IlRydWUifQ.p96LyX8s3-bKyhAoQw2fkkTc7jAeWFHuDLacQKLKfzE"
#    # Borrowing code from species/views.py again since I don't
#    # want to refresh the key which happens with the recommended
#    # approach for users.
#    jackUser = User.query.filter_by(username='jack').first()
#    jackAPIKey = jackUser.generate_api_token(expiration=31536000)
#    return jackAPIKey

def test_jack_api_key(eb_jack_api_key):
    assert eb_jack_api_key is not None

#def __create_request(request_str, api_key):
#    request = urllib2.Request(request_str)
#    base64string = base64.encodestring("%s:%s" % (api_key,"")).replace("\n", "")
#    request.add_header("Authorization", "Basic %s" % base64string)
#    return request

#def get_api_headers(api_key):
#    return {
#        'Authorization': 'Basic ' + base64.b64encode(
#            (api_key+ ":").encode('utf-8')).decode('utf-8'),
#        'Accept': 'application/json'
#    }

def test_404(eb_api_client, eb_jack_api_key):
    response = eb_api_client.get("/wrong/url", headers=get_api_headers(eb_jack_api_key))
    assert response.status_code == 404
    # json_response = json.loads(response.get_data(as_text = True))
    # Next line fails with a key error; but I'm not sure it's actually needed to
    # to have behaviour below since the response status code is actually 404.
    # assert json_response["error"] == "not found"

