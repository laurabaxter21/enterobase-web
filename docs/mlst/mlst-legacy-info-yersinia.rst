Protocols used for MLST of *Yersinia pseudotuberculosis*
======================================================
Genes
-----
The *Y. pseudotuberculosis* MLST scheme uses internal fragments of the
following seven house-keeping genes:

* *adk* (adenylate kinase)  
* *argA* (N-acetylglutamate synthase)  
* *aroA* (3-phosphoshikimate-1-carboxylvinyltransferase)  
* *glnA* (glutamine synthase)  
* *thrA* (aspartokinase I/homoserine dehydrogenase I)  
* *tmk* (thymidylate kinase)  
* *trpE* (anthranilate synthase component I)  

PCR Amplification
-----------------
The primer pairs we use for the PCR amplification of internal fragments of
these genes are:

+----------+---------------------------------+---------------+
|          |                                 |Product length:|
+----------+---------------------------------+---------------+
|          |                                 |               |
+----------+---------------------------------+---------------+
|*adk_P1*: | F 5'-ATGCGTATCATTCTGCTGGG-3'    |641 bp         |
+----------+---------------------------------+---------------+
|*adk_P2*: | R 5'-CCGAGAATAGTCGCCAGTTC-3'    |               |
+----------+---------------------------------+---------------+
|          |                                 |               |
+----------+---------------------------------+---------------+
|*argA_P1*:| F 5'-GGATTTCGCCACTCAGTTCC-3'    |615 bp         |
+----------+---------------------------------+---------------+
|*argA_P2*:| R 5'-ATCAGTCACCCCTTGTGATG-3'    |               |
+----------+---------------------------------+---------------+
|          |                                 |               |
+----------+---------------------------------+---------------+
|*aroA_P3*:| F 5'-AGCGGCCAATTGGCCATTTG-3'    |802 bp         |
+----------+---------------------------------+---------------+
|*aroA_P4*:| R 5'-CACATCGCCATACGGTGGTC-3'    |               |
+----------+---------------------------------+---------------+
|          |                                 |               |
+----------+---------------------------------+---------------+
|*glnA_P1*:| F 5'-GCTGACTTCTTCGAAGAAGG-3'    |701 bp         |
+----------+---------------------------------+---------------+
|*glnA_P2*:| R 5'-GACATATGGCAGTGCATACC-3'    |               |
+----------+---------------------------------+---------------+
|          |                                 |               |
+----------+---------------------------------+---------------+
|*thrA_P3*:| F 5'-CGTCTTTGCGGTGATGTCG-3'     |823 bp         |
+----------+---------------------------------+---------------+
|*thrA_P2*:| R 5'-GTTGGTGTCATACAAGAATTTACG-3'|               |
+----------+---------------------------------+---------------+
|          |                                 |               |
+----------+---------------------------------+---------------+
|*tmk_P3*: | F 5'-TATTGAAGGGCTTGAAGGGG-3'    |606 bp         |
+----------+---------------------------------+---------------+
|*tmk_P4*: | R 5'-TGATTGGTCAGCCACTGAGC-3'    |               |
+----------+---------------------------------+---------------+
|          |                                 |               |
+----------+---------------------------------+---------------+
|*trpE_P3*:| F 5'-CACCAGTTACAACAAGCGCC-3'    |743 bp         |
+----------+---------------------------------+---------------+
|*trpE_P4*:| R 5'-GTATCCAAATCACCGTGAGC-3'    |               |
+----------+---------------------------------+---------------+
    
An annealing temperature of 55° C is fine for all genes.

Sequencing
----------
*adk_S1*: F 5'-TGGAGAAATACGGCATTCCG-3'  
*adk_S2*: R 5'-ACATTACGGGTTCCGTCCAG-3'  

*argA_S3*: F 5'-CAAGACATTTGTTGTCATGC-3'  
*argA_S4*: R 5'-ATGGCTAACTGAGTTGCAAC-3'  

*aroA_S3*: F 5'-TGCACAGATCGATTATCTGG-3'  
*aroA_S2*: R 5'-ATGGTCATTGCAGCATCAGG-3'  

*glnA_S1*: F 5'-TTTGATGGCTCCTCGATTGGTG-3'  
*glnA_S2*: R 5'-TTGGTCATGGTATTGAAGCG-3'  

*thrA_S1*: F 5'-GATGTGATGGAACATCTGGC-3'  
*thrA_S2*: R 5'-GTCACCACATGGAAGCCATC-3'  

*tmk_S5*: F 5'-GGCCCAAGGGATTAACGATAT-3'  
*tmk_S2*: R 5'-AATGGGTTGGGAGGCATCAAT-3'  

*trpE_S1*: F 5'-CCAGAGATGGCGTTACAGTG-3'  
*trpE_S4*: R 5'-TAGCCCACTGCACCGCCGTA-3'

Allele template
---------------
Allelic profile of *Y. pseudotuberculosis* strain SP93422.

*adk* (389 bp):  

.. code-block:: bash 

    AAAGCAGGTTCTGAGTTAGGTCTGAAAGCAAAAGAAATTATGGATGCGGGCAAGTTGGTGACTGATGAGTTAGTTATCGCAT  
    TAGTCAAAGAACGTATCACACAGGAAGATTGCCGCGATGGTTTTCTGTTAGACGGGTTCCCGCGTACCATTCCTCAGGCAGAT  
    GCCATGAAAGAAGCCGGTATCAAAGTTGATTATGTGCTGGAGTTTGATGTTCCAGACGAGCTGATTGTTGAGCGCATTGTCGG  
    CCGTCGGGTACATGCTGCTTCAGGCCGTGTTTATCACGTTAAATTCAACCCACCTAAAGTTGAAGATAAAGATGATGTTACCG  
    GTGAAGAGCTGACTATTCGTAAAGATGATCAGGAAGCGACTGTCCGTAAGCGTCTTAT  
    

*argA* (361 bp):  

.. code-block:: bash 

    CTAGTATCGTCAACGATATAGGCTTATTACATAGCCTTGGGATCAGATTAGTCGTGGTATACGGTGCCCGACCACAGATTGAC  
    AGTAATCTGGCAGATCATAACTACGAACCTATTTATCATAAACATACCCGAGTGACTGACGCCCGGACTCTGGAGATGGTCA  
    AGCAAGCGGCCGGTTTATTACAACTGGATATCACGGCCAGGCTTTCAATGAGTTTGAACAACACCCCACTGCAAGGTGCTCAT  
    ATCAACGTAGTTAGCGGTAACTTTATCATTGCTCAACCGCTAGGTGTGGATGATGGTGTTGATTATTGCCATAGCGGGCGTATC  
    CGGCGCATTGATGAAGAAGCTATTCATCG  
    

*aroA* (357 bp):  

.. code-block:: bash 

    TTGATGGGCGTGTCTCTAGCCAGTTCCTGACTGCTTTATTGATGACCGCCCCGCTGGCGGAGCAAGATACGACTATTCGGATT  
    TGGGTGATCTGGTTTCCAAACCTTATATCGATATTACTCTGCACTTGATGAAAGCATTTGGTATTGACGTGGGGCATGAGAAC  
    TACCAAATTTTCCACATCAAAGGGGGTCAGACCTACCGCTCACCAGGGACTTATTTGGTTGAGGGCGATGCCTCGTCGGCTTC  
    CTACTTCTTAGCGGCTGCGGCTATTAAGGGGGGAACAGTGCGTGTCACTGGTATTGGCAAGAAAAGTGTACAGGGCGACACT  
    AAATTTGCCGATGTGTTGGAAAAAA  
    

*glnA* (338 bp):  

.. code-block:: bash 

    CACTGATTATCCGTTGTGACATTCTTGAGCCAGGCACCATGCAGGGCTATGACCGCGATCCACGTTCTATCTCCAAACGTG  
    TGAAGACTTCCTGAAATCTTCAGGTATCGCTGATACCGTGTTGTTCGGGCCAGAACCAGAATTCTTCCTGTTTGACGATATC   
    CGTTTTGGTAGCAGCATCAATGGGTCACACGTATCCATCGATGACGTCGAAGGCGCATGGAACTCCAGCACCAAATACGA  
    AGGCGGCAACAAAGGCCACCGTCCTGCAGTAAAAGGCGGTTACTTCCCTGTTCCTCCGGTCGACTCTGCACAAGACCTCC  
    GTTCTGCCATGTGT  
    

*thrA* (342 bp):  

.. code-block:: bash 

    TCAATATTATTGCTATCGCTCAAGGCTCGTCTGAGCGCTCTATTTCTGTGGTTGTCAATAATGATGCGGTCACTACCGGGGTGC  
    AGTTTGCCACCAGATGCTGTTTAACACGGATCAAGTCATTGAGGTGTTTGTCATTGGCGTGGGTGGTGTGGGGGGCGCATTAA  
    TCGAACAAATCTATCGCCAGCAACCGTGGTTGAAACAGCGTCATATCGATTTACGTGTTTGTGGTATCGCGAATTCCAAAGCC  
    ATGTTGACCAACGTGCATGGCATTGCATTAGATAACTGGCGTCAGGAACTGGCTGAAGTTCAGGAGCCGTTTAACCTGAGCC  
    GCTTGATCC  
    

*tmk* (375 bp):  

.. code-block:: bash 

    AAAACTGCGTGATTTGATTAAGCAAGGTATTGACGGTGAGGTCCTGACGGATAAAGCCGAGGTATTAATGCTGTATGCCG  
    CCAGGGTGCAATTAGTAGAAAATGTCATCAAGCCAGCACTGGCGCGTGGCAGTTGGGTTGTGGGTGACCGCCATGATTTG  
    TCATCACAGGCATATCAGGGCGGGGGGCGAGGGATTGACAGCCAACTGATGGCCTCGTTACGTGACACGGTGTTAGGCG  
    AATTCCGGCCAGACCTGACCTTATATTTGGATCTGCCACCGGCTGTGGGCTTGGCGCGCGCGCGCGCGCGCGGTGAATTGG  
    ACCGTATCGAACAAGAGTCACTGGCGTTTTTTGAGCGAACCCGCGCACGTTACCT  
    

*trpE* (465 bp):  

.. code-block:: bash 

    CCATCCCGCCGTTTTACGCTGCCCTGCCCATCACCGCTGGCGGCCTATCAGACACTGAAAGACCATAATCCCAGCCCCTAC  
    ATGTTTTTCATGCAAGACAATGATTTTTCTCTGTTTGGCGCATCACCTGAAAGCGCACTGAAATACGATGCCAGCAACCGT  
    CAAATTGAGATTTACCCGATTGCGGGTACTCGTCCACGCGGTCGTCGTCCTAATGGAGAACTCGATCGTGATTTAGACAG  
    CCGTATCGAGTTGGAAATGCGTACTGACCATAAAGAGATGGCAGAACATTTAATGTTGGTGGATCTGGCTCGTAACGATC  
    TGGCACGTATTTGCGAACCCGGTAGCCGCTATGTTGCAGATTTAACCAAAGTTGACCGTTACTCTTTTGTCATGCATCTGG  
    TGTCCCGCGTGATTGGCACTCTGCGTCAAGATTTAGATGTGCTGCATGCTTATCAAGCGTGT