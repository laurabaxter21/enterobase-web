Protocols used for MLST of *Moraxella catarrhalis*
================================================
Genes
-----
The *M. catarrhalis* MLST scheme uses internal fragments of the following eight house-keeping genes: 

* *glyRS* (glycyl-tRNA synthetase beta subunit)  
* *ppa* (Pyrophosphate phospho-hydrolase)  
* *efp* (elongation factor P)  
* *fumC* (fumarate hydratase)  
* *trpE* (anthranilate synthase component I)   
* *mutY* (adenine glycosylase)  
* *adk* (Adenylate kinase)  
* *abcZ* (ATP-binding protein)  

PCR Amplification
-----------------
The primer pairs we use for the PCR amplification of internal fragments of
these genes are:

+----------+---------------------------------+---------------+
|          |                                 |Product length:|
+----------+---------------------------------+---------------+
|          |                                 |               |
+----------+---------------------------------+---------------+
|*glyRS*:  | F 5'-GCACCGAAGAGTTGCCACCA-3'    |762 bp         |
+----------+---------------------------------+---------------+
|*glyRS*:  | R 5'-ACGCAACGGGCAAATCCACC-3'    |               |
+----------+---------------------------------+---------------+
|          |                                 |               |
+----------+---------------------------------+---------------+
|*ppa*:    | F 5'-AATAAAATTCTAGATGCTGGC-3'   |523 bp         |
+----------+---------------------------------+---------------+
|*ppa*:    | R 5'-ACTTATTGCTCTGTCCAGCG-3'    |               |
+----------+---------------------------------+---------------+
|          |                                 |               |
+----------+---------------------------------+---------------+
|*efp*:    | F 5'-CTCTGATTGACAACTGGCAGG-3'   |582 bp         |
+----------+---------------------------------+---------------+
|*efp*:    | R 5'-GATATTCGCCAGTACGCG-3'      |               |
+----------+---------------------------------+---------------+
|          |                                 |               |
+----------+---------------------------------+---------------+
|*fumC*:   | F 5'-GGGCGGTACAGCAGTCGGCAC-3'   |675 bp         |
+----------+---------------------------------+---------------+
|*fumC*:   | R 5'-CTCATCAAATTCAGCTTCAG-3'    |               |
+----------+---------------------------------+---------------+
|          |                                 |               |
+----------+---------------------------------+---------------+
|*trpE*:   | F 5'-TTATCCCGCATCGAAAATGG-3'    |545 bp         |
+----------+---------------------------------+---------------+
|*trpE*:   | R 5'-GGTTTCATCCCATTCAGCC-3'     |               |
+----------+---------------------------------+---------------+
|          |                                 |               |
+----------+---------------------------------+---------------+
|*mutY*:   | F 5'-GGCAATACCATCATCAGCCG-3'    |609 bp         |
+----------+---------------------------------+---------------+
|*mutY*:   | R 5'-GGTAACTGACTTTGAACGCC-3'    |               |
+----------+---------------------------------+---------------+
|          |                                 |               |
+----------+---------------------------------+---------------+
|*adk*:    | F 5'-GGCATTCCTCAAATCTCAAC-3'    |631 bp         |
+----------+---------------------------------+---------------+
|*adk*:    | R 5'-GATGGGCTTTATTGTCAAATG-3'   |               |
+----------+---------------------------------+---------------+
|          |                                 |               |
+----------+---------------------------------+---------------+
|*abcZ*:   | F 5'-ACATGCTGATGATGGTGAG-3'     |610 bp         |
+----------+---------------------------------+---------------+
|*abcZ*:   | R 5'-CACTGGCAAGTTCAAGCGC-3'     |               |
+----------+---------------------------------+---------------+

An annealing temperature of 52° C is fine for all genes, except *glyRS*
(58° C) and *adk* (54° C).

Sequencing
----------
We use the amplification primer pairs for the sequencing step except for the
following genes.

*glyRS*: F 5'-GCACCGAAGAGTTGCCACCA-3'  
*glyRS*: R 5'-ATATCGGCTTGACGCTGATC-3'  

*fumC*: F 5'-GCTGTCAAAGTCGCTAAAG-3'  
*fumC*: R 5'-CTCATCAAATTCAGCTTCAG-  

*mutY*: F 5'-TATGCTGTGTGGGTATCTG-3'  
*mutY*: R 5'-GGTAACTGACTTTGAACGCC-3'  

Allele template
---------------

Allelic profile of *M. catarrhalis* MC216

*glyRS* (537 bp):  

.. code-block:: bash

    GTTGGCACTTTACTAAATGATCAAAATATCAGCTTTGATGATATTCATGCCTTTGCCGCACCACGCCGTTTGGCACTTACTATTAC  
    AGGTGTGGGCGAGTTTCAGCCTGACCGTATCGAAACTAAAAAAGGCCCAAGTGTTAAGGCAGCGTTTGATGACGCAGGTAATTTAA  
    CGCGTGCAGGGCAAGGATTTTTGCAAGGTTTAAATGCCACTGGGCGAAATCTTAGTGTCAAAGATTTGGGGCGTATTGCTGATAAA  
    AAAGGCGAGTACATCGCTTATGATTTGACCATTAAAGGCGAAAAAATCACCGATTTAATGCCAAATATCTTACAAAAAGCGCTTGA  
    TGATTTGCCAATTGCTAAGCGTATGCGTTCTGGCATTGATCGCCATGAGTTCATACGGCCTGTTCATTGGGTGGTTTTGATGTCAG  
    ATGATGTTGTAATTGAGGCGACCATTCAAGGCCATAAAACTGGCAACCAAAGCCGTGGTCATCGCTATCATTCACCTGATTTTTTT  
    GCCATTGATCATGCCGATCAT  
    

*ppa* (393 bp):  

.. code-block:: bash

    TCAAATCATAAAATTGAGTGGAATCGTCATTTGGCATGCTTTGAGCTTGATCGTGTTGAGCCAATTGCCTTTGCCAAACCATGCAA  
    CTATGGCTTTATTCCCCAAACCTTAGATGAAGATGGTGATGAATTAGACGCCTTAATCATCACAGAACAGCCTTTGACGACGGGTA  
    TTTTTCTTAAAGCCAAAGTCATCGGGGTAATGAAATTTGTGGATGATGGCGAGGTCGACGATAAAGTCATCGTTGTTCCTGCCGAT  
    GATCGTAATAATGGCAACGCCTATAATAGTCTTGACGACCTACCACCACAGTTAATAAAGCAACTTGAATTTCATTTTAGTCATTA  
    TAAAGACTTAAAAAAACCAGGCTCAACTGTGGTTGAGGGCTTTTTTGAT  
    

*efp* (414 bp):  

.. code-block:: bash

    AATGAATATGTGAAACCAGGTAAAGGTCAAGCTTTTAACCGTGTTAAGTTGCGTAATCTACGCACAGGCAAAGTGCTTGAGCAGAC  
    TTTTAAGTCAGGCGATTCACTTGAAGGTGCTGATGTGGTGGATGTGGAAATGAACTATCTATACAACGATGGCGAATTTTGGCATT  
    TTATGCACCCTGAGACTTTTGAGCAATTGCAAGCAGATAAAACCGCAATGGGTGATGCTGCACAGTGGCTAAAAGAAAATTCAAAT  
    GCGTTATGCACTTTGACGCTATTTAATGGTGCGCCGCTGTCGGTCACACCACCAAACTTTGTAGAGCTTGAAATCGTTGAAACTGA  
    CCCAGGTGTTCGCGGTGATACCTCAGGTGGTGGCGGTAAGCCTGCACGCCTTGAAACAGGTGCAACCGTG  
    

*fumC* (465 bp):  

.. code-block:: bash

    GAGGCTTTGGCCGCACGAGATGCCGAAGTATTTGCTTCAGGTGCACTAAAAACACTGGCAGCAAGCCTAAATAAAATCGCCAATGA  
    TGTGCGTTGGCTGGCTTCAGGCCCACGCTGCGGTCTAGGTGAGCTTAGCATTCCTGAAAATGAGCCTGGTTCATCCATCATGCCTG  
    GTAAAGTCAATCCAACTCAGTGTGAGGCGATGACCATGGTGGTCACCCAAGTCTTTGGTAATGATACGACCATTTCTATGGCAGGG  
    GCTTCTGGTAATTTTGAATTAAATGTTTATATGCCTGTGATTGGATATAACTTATTGCAGTCGATTCGTCTGCTAACGGATGCGAT  
    CAATAGCTTTAATGAGCATTGTGCGGTAGGGATCGAACCAATTTACGAAAAAATTGACCATTATTTACATCATTCTTTGATGTTGG  
    TCACCGCACTCAATCGCCATATTGGCTATGAAAAT  
    

*trpE* (372 bp):  

.. code-block:: bash

    TTAAATGACCAAAAAGAGATCGCAGAACATCTGATGCTCATTGATCTTGGGCGTAATGATATTGGGCGTGTCTGTGAGATTGGTTC  
    GGTTAAAGTGACAGATCAGATGTTCATTGAACGCTACTCGCAAGTGATGCATATCGCTAGTAATGTTGAGGGGCGTATACGCCAAG  
    ATGTCGATGCTTTAGATGTGTTTTGTGCGACCTTTCCAGCGGGTACTTTGTCTGGTGCGCCAAAAATCCGTGCCATGCAAATTATT  
    GATGAAGTTGAGCCAGTGCGTCGCACCGTTTTTGGCGGGGCGATCGGATATCTTGGCTGGCATGGCAATATGGATACTGCCATCGC  
    AATACGCACAGCGGTCATGAAAGATGGT  
    

*mutY* (426 bp):  

.. code-block:: bash

    TTTTTTGAGCCGTTTTTGGCACGATTTGCAACCGTACAAGAGCTTGCTGTAGCAGATTGGCAAGAAGTAGCGTCTTTTTGGGCAGG  
    GCTTGGGTATTATGCTCGGGCAAGAAATTTACACGCAGGCGCACAGCAAGTGGCTGATTTTATTGATACTCATGGCAGGTTTCCTG  
    AAACGGTCAATGAGTGGCAAGCAGTCAAAGGGGTGGGGCGATCGACAGCTGGCGCAATCGTTGCGATGGGCGTTAAGAAGTTTGGC  
    GTCATCTGCGATGGCAATGTCAAGCGTGTACTTGCTCGCCATCGCGCGGTTTTTGGTGATGTGACAAAAAGCGCAACTGACAAAAG  
    GCTTTGGGAAATTGCCACCGCACTAACCCCGAAAGAGTATAGTGGACACTATGCCCAAGCGATGATGGATTTGGGTGCAACA  
    

*adk* (471 bp):  

.. code-block:: bash

    CAAGCAAAAAGTGTCATGGATGCAGGTCAATTGGTTTCTGATGAGCTGATTATTAATTTGGTTAAAGAACGCTTAAAACAGCCAGA  
    TTGTGACAATGGTTGTATTTTTGATGGATTCCCACGCACAATCCCACAAGCCAAAGCACTGGCAGATGCTGGCATTGCCATTGACC  
    ATGTGATTGAAATCAGCGCCCCTGATGATGAAATCGTAAGCCGTATGTCGGGTCGCCGTGCACATTTGGCATCAGGTCGTACTTAT  
    CATGTGATCTATAATCCCCCAAAAACTGACGGTATTGATGATGTCACAGGCGAGGCACTGGTGCAGCGTGATGATGATAAAGAAGA  
    AGTGGTGCGTGATCGCTTATCGGTATATCACGCCCAAACCGCTACATTGATTAGTCATTATCAAGAAGTTGCCAAATCGGGCGAAA  
    ATGCCCCTGAATATCATGACTTTGATGGAACAAAATCCATC  
    

*abcZ* (429 bp):  

.. code-block:: bash

    CTTGATGTGGTGATGGAGGGTGATGACAAAGTTGCCCAAGCCTTAAAAAACTATCATCAAGCCAGTGCAAAATGCGCCATGGGCGA  
    CAATGAAGCTTGTGATGCCATGAGCGGTCTTCAGCAAACACTCGATGAGTTAAATGGTTGGGACTTGGAGCGGCGTGCGCGTGCAT  
    TGATTGACAGCATGGGGTTTGACGCTGATGCTTGCTTATCAACATTATCAGGGGGTCGTAAACGCCGTGTTCTGTTAGCACAGGCT  
    TTGGTAACAAATCCTGATGTGCTACTTTTAGACGAGCCAACCAACCATTTAGATATTGAAAGCATCACTTGGCTTGAAAAATTTTT  
    ATTAAATGAACGATTAACCGTTCTTTTCATCTCACATGATCGGAAGTTTGTTGATACACTTGCCACTTATATCATCGAGCTTGAC