Details of assembly methods and in silico genotyping for *Clostridioides*
=========================================================================

* **All MLST-like typing methods in EnteroBase are derived from a genome assembly of sequenced reads. For an explanation of this method**, see :doc:`here </pipelines/backend-pipeline-qassembly>`
* **For a general description of the in silico typing method**, see :doc:`here </pipelines/backend-pipeline-nomenclature>`


+-----------------+----------------+---------------+-----------------+
| MLST – Classic  | Ribosomal MLST | Core Genome   | Whole Genome    |
|                 | (Jolley, 2012) | MLST          | MLST            |
+=================+================+===============+=================+
| 7 Loci          | 53 Loci        | 2,556 Loci    | 11,490 Loci     |
+-----------------+----------------+---------------+-----------------+
| Conserved       | Ribosomal      | Core genes    | Any coding      |
| Housekeeping    | proteins       |               | sequence        |
| genes           |                |               |                 |
+-----------------+----------------+---------------+-----------------+
| Highly          | Highly         | Variable;     | Highly          |
| conserved; Low  | conserved;     | High          | variable;       |
| resolution      | Medium         | resolution    | Extreme         |
|                 | resolution     |               | resolution      |
+-----------------+----------------+---------------+-----------------+
| Different       | Single scheme  | Different     | Different       |
| scheme for each | across tree of | scheme for    | scheme for each |
| species/genus   | life           | each          | species/genus   |
|                 |                | species/genus |                 |
+-----------------+----------------+---------------+-----------------+

7 Gene MLST
-----------

The classic MLST scheme for Clostridioides is described in `Griffiths et
al (2010) J. Clin. Microbiol. 48, 770-778`_.

Genes included in 7 gene MLST (together with the length of sequence used
for MLST taken from table 1 in the above cited paper):

==== ======
Gene Length
==== ======
adk  501
atpA 555
dxr  411
glyA 516
recA 564
sodA 450
tpi  504
==== ======

Ribosomal MLST (rMLST)
----------------------

-  **rMLST** is Copyright © 2010-2016, University of Oxford. rMLST is
   described in `Jolley et al. 2012 Microbiology 158:1005-15`_.

.. _please click here: EnteroBase%20Backend%20Pipeline%3A%20nomenclature
.. _Griffiths et al (2010) J. Clin. Microbiol. 48, 770-778: http://dx.doi.org/10.1128%2FJCM.01796-09
.. _`Jolley et al. 2012 Microbiology 158:1005-15`: http://www.ncbi.nlm.nih.gov/pubmed/22282518