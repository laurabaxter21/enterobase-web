Editing strain metadata
=======================

Access control
--------------
Firstly, All the genome records downloaded from public databases, i.e. GenBank/SRA, can be viewed by all the users. 
However, there are strict access controls in EnteroBase for genome records `uploaded by our users <add-upload-reads.html>`_. 
By default, only the owner (the one who uploaded the data) and curators/administrators of the database have access to the genomic assemblies, metadata and genotypes prior to the release date. Owners can explicitly assign editing permissions to other users. See more about `permissions <https://enterobase.readthedocs.io/en/latest/about.html#fair-usage>`_ here.  

Furthermore, the user defined fields are private to their creator and curators/admin. Again, the creator can grant access to other users and the owner can chose to publicly publish their user defined fields, such that all other users can see and download the data. 

The specific permissions granted to different users are listed in the table:

+----------------------------------------------------------+---------------------------+------------------------------------------------------------+--------------------------------------+
|                                                          | Public                    | |            User uploads                                  | User defined fields                  |
|                                                          +---------------------------+---------------------------------+--------------------------+--------------------+-----------------+
|                                                          | Released                  | Pre-release                     | Post-release             | Private            |    Public       |
+==========================================================+===========================+=================================+==========================+====================+=================+
| | Curators &                                             | | Edit &                  | | Edit &                        | | Edit &                 | | Edit &           | | Edit &        |
| | Admins                                                 | | Download                | | Download                      | | Download               | | Download         | | Download      |
+----------------------------------------------------------+---------------------------+---------------------------------+--------------------------+--------------------+-----------------+
| Owner                                                    | | Edit &                  | | Edit &                        | | Edit &                 | | Edit &           | *Download*      |
|                                                          | | Download                | | Download                      | | Download               | | Download         |                 |
+----------------------------------------------------------+---------------------------+---------------------------------+--------------------------+--------------------+-----------------+
| | Buddies with                                           | | Edit &                  | | Edit &                        | | Edit &                 | | **Only visible** | *Download*      |
| | edit rights                                            | | Download                | | Download                      | | Download               | | **when shared**  |                 |
+----------------------------------------------------------+---------------------------+---------------------------------+--------------------------+--------------------+-----------------+
| | Buddies w/o                                            | *Download*                | | *Download* in                 | *Download*               | | **Only visible** | *Download*      |
| | edit rights                                            |                           | | shared workspace              |                          | | **when shared**  |                 |
+---------+------------------------------------------------+---------------------------+---------------------------------+--------------------------+--------------------+-----------------+
| Other users                                              | *Download*                | **View only**                   | *Download*               | **Not visible**    | *Download*      |
+----------------------------------------------------------+---------------------------+---------------------------------+--------------------------+--------------------+-----------------+


Edit permissions
----------------


EnteroBase allows users with sufficent permissions to edit metadata.
Users can correct mistakes in uploaded metadata or metadata associated with
records downloaded from sequenced read archives (SRAs). Metadata maybe
absent, or inaccurate, even after the automated pre-processing by EnteroBase.
Editing permissions are automatically granted to:

-   Administrators and curators
-   the owner of a strain, or their :doc:`buddies </features/buddies>`, if:

1.  their EnteroBase email address matches the submitter's email address in the SRA
    metadata
2.  they have uploaded short reads to EnteroBase
3.  ownership rights that were explicitly granted by
    administrators/curators after receiving an email request

To enter Edit Mode, login, and search strains using "Search Strains" and
click on the Edit Mode checkbox. A popup window informs you that you are
in Edit Mode and you need to click on OK. You have the rights to edit
any row of metadata that contains a pencil icon in the second column
from the left. If you need to edit metadata with no pencil, contact the
administrators/curators to obtain permissions for individual Strain
designations.

Editing Mode 
------------
In Edit Mode, for Strains where you have editing permission, a dark
green pencil indicates adequate metadata while a red pencil indicates
metadata which is lacking required information. The fields with errors
are coloured red.

Required information include:

-   Year of isolation
-   Country, and continent of isolation
-   Laboratory contact details

If country and/or continent are inconsistent, you will need to resolved
this in the sub-window, which is opened by clicking the field
Location.

Metadata can be changed by typing into the text boxes, or all metadata
for a Strain can be edited after clicking the pencil icon. Editing
changes are updated in the Edit Mode browser after clicking outside the
text box, but can be reversed with Ctl+Z or a mouse right click on the
cell with changed data. Yellow cells indicate data which has been
edited. **Saving edited data is not automatic!** To save edited data,
click on Save Edits icon or ```Edit -> Save Edits```. To save only one row of
edited data, right click and choose Save Edited Row.

Multiple replaces can be performed using ```Edit -> Search and Replace```.

Downloading Metadata
--------------------
``Metadata -> Download Metadata`` will generate a tab delimited
text file (can be imported into Excel) containing the metadata and an
overview of the experimental data from the right part of the screen. If
some entries are selected, only selected entries will be downloaded. If
no entries are selected, you will need to specify how many are to be
downloaded, starting from the top entry on the screen. Warning:
downloading all entries can result in a file containing 10,000s of rows.
```Metadata -> Download editable Metadata``` will also generate a tab
delimited text file, with the suffix ".txt".

You are not able to download metadata of a private EnteroBase record 
unless you have been granted download permissions from the owner of the record. 