Managing your buddies
=====================
Editing rights to your data and access to your workspaces can be controlled through the buddies system. Buddies permission control for the currently opened database can be accessed through the left hand sidebar: Tasks > My Buddies.

Add a buddy
-----------
To add a buddy, fill their user name in into the Name text box, it will try to suggest possible users with a drop down. Select the correct User and click ‘Add Buddy’, this will add them to the table below. 

  .. image:: https://bitbucket.org/enterobase/enterobase-web/raw/4cb061ed3c4ce6fc2b9b8273f481eb5aeac62538/docs/images/add-buddy.PNG

Grant global editing/download rights
------------------------------------
Checking the ‘edit metadata’ box will give this user global editing rights for the current database to all metadata for entries that you own, and download access to all your metadata and genomes. They will receive an email informing them of this change.

Share workspaces/analyses/folders
---------------------------
Clicking on the Add button in the Shared Analysis column will allow you to choose one of your existing resources to share with that buddy. The resource could be a `Folder`_, `Workspace`_, `Custom View`_ or other analysis type. Select the resource from the Workspaces window and press the Share button. The buddy will receive an email informing them of this addition.
Share a folder to you buddies will give them access to all the workspaces and analyses in the folder.

  .. image:: https://bitbucket.org/enterobase/enterobase-web/raw/4cb061ed3c4ce6fc2b9b8273f481eb5aeac62538/docs/images/share-workspace.PNG

Stop sharing
------------
In order to stop sharing a resource, click on the ‘X’. (If you are no longer sharing a resource with another user then that user will be removed from the Buddy list.)  
The ability to edit metadata can also be removed from another user by unticking the box in that column.

  .. image:: https://bitbucket.org/repo/Xyayxn/images/3015178011-buddies5.PNG

.. _`Folder`: using-workspaces.html#folder-system
.. _`Workspace`: using-workspaces.html
.. _`Custom View`: user-defined-content.html
