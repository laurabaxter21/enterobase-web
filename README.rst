**A Powerful, User-Friendly Online Resource for Analyzing and Visualizing Genomic 
Variation within Enteric Bacteria**

EnteroBase aims to establish a world-class, one-stop, user-friendly, 
backwards-compatible but forward-looking genome database, Enterobase --
together with a set of web-based tools, EnteroBase Backend Pipeline --
to enable bacteriologists to identify, analyse, quantify and visualize 
genomic variation principally within the genera: 

* `Escherichia <http://enterobase.warwick.ac.uk/species/index/ecoli>`_
* `Salmonella <http://enterobase.warwick.ac.uk/species/index/senterica>`_
* `Yersinia <http://enterobase.warwick.ac.uk/species/index/yersinia>`_
* `Moraxella <http://enterobase.warwick.ac.uk/species/index/mcatarrhalis>`_

EnteroBase is populated with over 200,000 of genomic assemblies derived from 
publicly available complete genomes, sequence read archives and user uploads.

Click here to `Post an issue or bug about EnteroBase <https://bitbucket.org/enterobase/enterobase-web/issues?status=new&status=open>`_

Funded by BBSRC research grant `BB/L020319/1 <http://gtr.rcuk.ac.uk/projects?ref=BB%2FL020319%2F1>`_.

For full documentation see http://enterobase.readthedocs.io/en/latest/ 