#For more information on configuration, see:

#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user  nginx;
worker_processes  1;


pid        /run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    include blockips.conf;
    default_type  application/octet-stream;

    log_format upstreamlog '[$time_local] $remote_addr - $remote_user - $server_name to: $upstream_addr: $request upstream_response_time $upstream_response_time msec  request_time $request_time';

    access_log  /home/u1470946/access.log  upstreamlog;
    error_log  /home/u1470946/error.log;
    proxy_read_timeout 600s;
    proxy_connect_timeout 600s;
    limit_req_zone $binary_remote_addr zone=apilimit:10m rate=2r/s;
   
    sendfile        on;
    sendfile_max_chunk 1m;
    tcp_nopush     on;
    client_max_body_size 3000M;
    keepalive_timeout  65;

    gzip  on;
    gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;
    gunzip on;

    index   index.html index.htm;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;
  upstream backend_hosts {
#       server hydra.warwick.ac.uk max_fails=0 fail_timeout=30s;
        server hercules.warwick.ac.uk:8000 max_fails=10 fail_timeout=30s;
#        server tomva.lnx.warwick.ac.uk:8000 max_fails=10 fail_timeout=30s;
#        server olkep.lnx.warwick.ac.uk:8000 max_fails=10 fail_timeout=30s;
  }


 server{
	server_name mlst.warwick.ac.uk;
   	location /mlst/dbs/Ecoli {
		return 301 http://enterobase.warwick.ac.uk/species/ecoli/allele_st_search;
   	}
	location /mlst/dbs/Senterica{
		return 301 http://enterobase.warwick.ac.uk/species/senterica/allele_st_search;
	}
	location /mlst/dbs/Ypseudotuberculosis {
		return 301 http://enterobase.warwick.ac.uk/species/yersinia/allele_st_search;
	}
   	location /mlst/dbs/Mcatarrhalis {
                return 301 http://enterobase.warwick.ac.uk/species/mcatarrhalis/allele_st_search;
        }
  	location / {
		return 301 http://enterobase.warwick.ac.uk/warwick_mlst_legacy;
    }
  }


  server {
        listen       80;
        listen       443 http2 ssl;
        server_name  enterobase.warwick.ac.uk;
        root         /usr/share/nginx/html;
        ssl_certificate /etc/nginx/ssl/enterobase.warwick.ac.uk.crt;
        ssl_certificate_key /etc/nginx/ssl/enterobase.warwick.ac.uk.key;
        
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers on;
        ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
        ssl_dhparam /etc/nginx/ssl/dhparams.pem;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
             proxy_pass http://backend_hosts; 

            proxy_set_header X-Real-IP  $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto https;
            proxy_set_header X-Forwarded-Port 443;
            proxy_set_header Host $host;
            rewrite http://137.205.66.6/ /warwick_mlst_legacy;
            add_header X-Proxy-Upstream $upstream_addr;                  
            add_header X-Proxy-Tag $cookie_session;                  
        }

        location /view_jbrowse {
             rewrite ^/(view_jbrowse.*) /$1 break;
	     proxy_pass http://hercules.warwick.ac.uk:8000;
             proxy_set_header Host $host;
             add_header X-Proxy-Upstream $upstream_addr;
             proxy_set_header X-Real-IP  $remote_addr;
             proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

      

        location /upload {
             rewrite ^/(upload.*) /$1 break;
             proxy_pass http://backend_hosts;
#	     proxy_pass http://hercules.warwick.ac.uk:8000;
             proxy_set_header Host $host;
             add_header X-Proxy-Upstream $upstream_addr;                  
             proxy_set_header X-Real-IP  $remote_addr;
             proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
   	}
        location /api/v2.0/ {
             rewrite ^/(api.*) /$1 break;
             proxy_pass http://backend_hosts;
#	     proxy_pass http://hercules.warwick.ac.uk:8000;
             proxy_set_header Host $host;
             add_header X-Proxy-Upstream $upstream_addr;                  
             proxy_set_header X-Real-IP  $remote_addr;
             proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	     limit_req zone=apilimit;
   	}

	location ~ hierBG.*{
             deny all;
             return 403;
        }

	location ~ /schemes/(hierBG.*|.*rMLST|Listeria.*|Bacillus.*|Helicobacter.*|Mycobacterium.*|Neirsseria.*|Photorhabdus.*|x.*|Vibrio.*|klebsiella.*|daily.*){
             deny all;
             return 403;
        }
        location /as {
            deny all;
            alias /share_space/interact/outputs/;
            location ~ (.fasta|.fastq|.gbk.gz|.gff.gz){
                allow all;
            }
        }
        location /old_as {
            deny all;
            alias /share_space/assembly/;
            location ~ (.fasta|.fastq){
                allow all;
            }
        }
	location /schemes {
	    autoindex on;
            alias /data/NServ_dump/;
	}
        location ~ /(ipfs.*|ipns.*) {
             rewrite ^/(ipfs.*|ipns.*) /$1 break;
	     proxy_pass http://hydra.warwick.ac.uk:8020;
             add_header X-Proxy-Upstream $upstream_addr;                  
	}
	location /sparse {
	     alias /share_space/metagenome/database/SPARSE/; 
	     autoindex on;
	} 
       location /static {
	     root /var/www/entero/entero;
           add_header X-Proxy-Cache $upstream_cache_status;                  

        }
        location /anvi_public {
            rewrite ^/anvi_public/(.*)$ https://enterobase.warwick.ac.uk/anvio/public/zhemin/$1  permanent;
            add_header X-Proxy-Upstream $upstream_addr;
        }
        location /anvio {
                rewrite ^/(anvio.*) /$1 break;
                # proxy_pass http://137.205.123.127;
	        proxy_pass http://hercules.warwick.ac.uk:8000;
                add_header X-Proxy-Upstream $upstream_addr;                  
	}

       location /entero_jbrowse {
                rewrite ^/(entero_jbrowse.*) /$1 break;
                # proxy_pass http://137.205.123.127;
	        proxy_pass http://hercules.warwick.ac.uk:8000;
                 proxy_set_header Host $host;
                add_header X-Proxy-Upstream $upstream_addr;
                proxy_set_header X-Real-IP  $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        }
        location ~(search_strains|upload_reads|register|login) {
             proxy_pass http://backend_hosts; 
        proxy_set_header Host             $host;
        proxy_set_header X-Real-IP        $remote_addr;
        proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;
        proxy_set_header X-Accel-Internal /internal-nginx-static-location;       
          if ($scheme = https) {
            return 301 http://$server_name$request_uri;
         }

 
         }
        	
    }
}
